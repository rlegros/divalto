<?xml version="1.0" encoding="UTF-8"?>
<md:node xmlns:md="http://www.stambia.com/md" defType="com.stambia.rdbms.server" id="_N_b74AcLEeu--f7ccO6NIw" name="DivaltoMSSQLServer" md:ref="platform:/plugin/com.indy.environment/technology/rdbms/mssql/mssql.rdbms.md#UUID_MD_RDBMS_MSSQL?fileId=UUID_MD_RDBMS_MSSQL$type=md$name=Microsoft%20SQL%20Server?">
  <attribute defType="com.stambia.rdbms.server.url" id="_4-rpcAcLEeu--f7ccO6NIw" value="jdbc:sqlserver://172.16.128.191;databaseName=erpdivaltotest"/>
  <attribute defType="com.stambia.rdbms.server.driver" id="_4-y-MAcLEeu--f7ccO6NIw" value="com.microsoft.sqlserver.jdbc.SQLServerDriver"/>
  <attribute defType="com.stambia.rdbms.server.user" id="_4-0MUAcLEeu--f7ccO6NIw" value="dsi_etudes"/>
  <attribute defType="com.stambia.rdbms.server.password" id="_4-0MUQcLEeu--f7ccO6NIw" value="25FC3DFA90B1557E3938DF9446D31D379F56FC671615A6C08640143EF56D2DF2"/>
  <node defType="com.stambia.rdbms.schema" id="_OGfmIAcLEeu--f7ccO6NIw" name="ERPDIVALTOTEST.dbo">
    <attribute defType="com.stambia.rdbms.schema.catalog.name" id="_OHcoYAcLEeu--f7ccO6NIw" value="ERPDIVALTOTEST"/>
    <attribute defType="com.stambia.rdbms.schema.name" id="_OHcoYQcLEeu--f7ccO6NIw" value="dbo"/>
    <attribute defType="com.stambia.rdbms.schema.rejectMask" id="_OHcoYgcLEeu--f7ccO6NIw" value="R_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.loadMask" id="_OHcoYwcLEeu--f7ccO6NIw" value="L[number]_[targetName]"/>
    <attribute defType="com.stambia.rdbms.schema.integrationMask" id="_OHcoZAcLEeu--f7ccO6NIw" value="I_[targetName]"/>
    <node defType="com.stambia.rdbms.datastore" id="_ArdfMQcdEeu--f7ccO6NIw" name="ECF_COBAL_MERCURIALE_CLIENT">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_ArdfMgcdEeu--f7ccO6NIw" value="ECF_COBAL_MERCURIALE_CLIENT"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_ArdfMwcdEeu--f7ccO6NIw" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_ArpscAcdEeu--f7ccO6NIw" name="NOM_MERCURIALE" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_ArpscQcdEeu--f7ccO6NIw" value="NOM_MERCURIALE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ArpscgcdEeu--f7ccO6NIw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ArpscwcdEeu--f7ccO6NIw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ArpsdAcdEeu--f7ccO6NIw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ArpsdQcdEeu--f7ccO6NIw" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ArpsdgcdEeu--f7ccO6NIw" name="CODE_CLIENT" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_ArpsdwcdEeu--f7ccO6NIw" value="CODE_CLIENT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ArpseAcdEeu--f7ccO6NIw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ArpseQcdEeu--f7ccO6NIw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ArpsegcdEeu--f7ccO6NIw" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ArpsewcdEeu--f7ccO6NIw" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_AsmuvAcdEeu--f7ccO6NIw" name="TOP_ETL" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_AsmuvQcdEeu--f7ccO6NIw" value="TOP_ETL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_AsmuvgcdEeu--f7ccO6NIw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_AsmuvwcdEeu--f7ccO6NIw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_AsmuwAcdEeu--f7ccO6NIw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_AsmuwQcdEeu--f7ccO6NIw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Asv4oAcdEeu--f7ccO6NIw" name="TOP_MAJ" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_Asv4oQcdEeu--f7ccO6NIw" value="TOP_MAJ"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Asv4ogcdEeu--f7ccO6NIw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Asv4owcdEeu--f7ccO6NIw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Asv4pAcdEeu--f7ccO6NIw" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Asv4pQcdEeu--f7ccO6NIw" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Asv4pgcdEeu--f7ccO6NIw" name="DATE_CREATION" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_Asv4pwcdEeu--f7ccO6NIw" value="DATE_CREATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Asv4qAcdEeu--f7ccO6NIw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Asv4qQcdEeu--f7ccO6NIw" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Asv4qgcdEeu--f7ccO6NIw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Asv4qwcdEeu--f7ccO6NIw" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Asv4rAcdEeu--f7ccO6NIw" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Asv4rQcdEeu--f7ccO6NIw" name="DATE_MODIFICATION" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_Asv4rgcdEeu--f7ccO6NIw" value="DATE_MODIFICATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Asv4rwcdEeu--f7ccO6NIw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Asv4sAcdEeu--f7ccO6NIw" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Asv4sQcdEeu--f7ccO6NIw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Asv4sgcdEeu--f7ccO6NIw" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Asv4swcdEeu--f7ccO6NIw" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_As5poAcdEeu--f7ccO6NIw" name="DATE_SUPPRESSION" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_As5poQcdEeu--f7ccO6NIw" value="DATE_SUPPRESSION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_As5pogcdEeu--f7ccO6NIw" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_As5powcdEeu--f7ccO6NIw" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_As5ppAcdEeu--f7ccO6NIw" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_As5ppQcdEeu--f7ccO6NIw" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_As5ppgcdEeu--f7ccO6NIw" value="23"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_uVE60S2JEeuJLcmW5jMZbA" name="ART">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_w28aoC2JEeuJLcmW5jMZbA" value="ART"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_9KFnQC2JEeuJLcmW5jMZbA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_K9wygC2KEeuJLcmW5jMZbA" name="ART_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_K9wygS2KEeuJLcmW5jMZbA" value="ART_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K9wygi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_K9wygy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K9wyhC2KEeuJLcmW5jMZbA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K9wyhS2KEeuJLcmW5jMZbA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K9wyhi2KEeuJLcmW5jMZbA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K9wyhy2KEeuJLcmW5jMZbA" name="CE1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_K9wyiC2KEeuJLcmW5jMZbA" value="CE1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K9wyiS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K9wyii2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K9wyiy2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K9wyjC2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K96jgC2KEeuJLcmW5jMZbA" name="CE2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_K96jgS2KEeuJLcmW5jMZbA" value="CE2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K96jgi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K96jgy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K96jhC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K96jhS2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K98_wC2KEeuJLcmW5jMZbA" name="CE3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_K98_wS2KEeuJLcmW5jMZbA" value="CE3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K98_wi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K98_wy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K98_xC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K98_xS2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K98_xi2KEeuJLcmW5jMZbA" name="CE4" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_K98_xy2KEeuJLcmW5jMZbA" value="CE4"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K98_yC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K98_yS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K98_yi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K98_yy2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K-H-4C2KEeuJLcmW5jMZbA" name="CE5" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_K-H-4S2KEeuJLcmW5jMZbA" value="CE5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K-H-4i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K-H-4y2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K-H-5C2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K-H-5S2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K-H-5i2KEeuJLcmW5jMZbA" name="CE6" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_K-H-5y2KEeuJLcmW5jMZbA" value="CE6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K-H-6C2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K-H-6S2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K-H-6i2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K-H-6y2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K-H-7C2KEeuJLcmW5jMZbA" name="CE7" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_K-H-7S2KEeuJLcmW5jMZbA" value="CE7"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K-H-7i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K-H-7y2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K-H-8C2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K-H-8S2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K-RI0C2KEeuJLcmW5jMZbA" name="CE8" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_K-RI0S2KEeuJLcmW5jMZbA" value="CE8"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K-RI0i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K-RI0y2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K-RI1C2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K-RI1S2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K-RI1i2KEeuJLcmW5jMZbA" name="CE9" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_K-RI1y2KEeuJLcmW5jMZbA" value="CE9"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K-RI2C2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K-RI2S2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K-RI2i2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K-RI2y2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K-a50C2KEeuJLcmW5jMZbA" name="CEA" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_K-a50S2KEeuJLcmW5jMZbA" value="CEA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K-a50i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K-a50y2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K-a51C2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K-a51S2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K-a51i2KEeuJLcmW5jMZbA" name="DOS" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_K-a51y2KEeuJLcmW5jMZbA" value="DOS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K-a52C2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K-a52S2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K-a52i2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K-a52y2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K-a53C2KEeuJLcmW5jMZbA" name="REF" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_K-a53S2KEeuJLcmW5jMZbA" value="REF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K-a53i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K-a53y2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K-a54C2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K-a54S2KEeuJLcmW5jMZbA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K-kDwC2KEeuJLcmW5jMZbA" name="USERCR" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_K-kDwS2KEeuJLcmW5jMZbA" value="USERCR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K-kDwi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K-kDwy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K-kDxC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K-kDxS2KEeuJLcmW5jMZbA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K-kDxi2KEeuJLcmW5jMZbA" name="USERMO" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_K-kDxy2KEeuJLcmW5jMZbA" value="USERMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K-kDyC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K-kDyS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K-kDyi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K-kDyy2KEeuJLcmW5jMZbA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K-t0wC2KEeuJLcmW5jMZbA" name="CONF" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_K-t0wS2KEeuJLcmW5jMZbA" value="CONF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K-t0wi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K-t0wy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K-t0xC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K-t0xS2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K-t0xi2KEeuJLcmW5jMZbA" name="DES" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_K-t0xy2KEeuJLcmW5jMZbA" value="DES"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K-t0yC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K-t0yS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K-t0yi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K-t0yy2KEeuJLcmW5jMZbA" value="80"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K-t0zC2KEeuJLcmW5jMZbA" name="DESABR" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_K-t0zS2KEeuJLcmW5jMZbA" value="DESABR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K-t0zi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K-t0zy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K-t00C2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K-t00S2KEeuJLcmW5jMZbA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K-3lwC2KEeuJLcmW5jMZbA" name="EAN" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_K-3lwS2KEeuJLcmW5jMZbA" value="EAN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K-3lwi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K-3lwy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K-3lxC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K-3lxS2KEeuJLcmW5jMZbA" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K-3lxi2KEeuJLcmW5jMZbA" name="TIERS" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_K-3lxy2KEeuJLcmW5jMZbA" value="TIERS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K-3lyC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K-3lyS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K-3lyi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K-3lyy2KEeuJLcmW5jMZbA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_AvsC2KEeuJLcmW5jMZbA" name="TAREF" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_AvsS2KEeuJLcmW5jMZbA" value="TAREF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_Avsi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_Avsy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_AvtC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_AvtS2KEeuJLcmW5jMZbA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_FBIC2KEeuJLcmW5jMZbA" name="REFRPL" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_FBIS2KEeuJLcmW5jMZbA" value="REFRPL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_FBIi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_FBIy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_FBJC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_FBJS2KEeuJLcmW5jMZbA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_FBJi2KEeuJLcmW5jMZbA" name="TAFAMRX" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_FBJy2KEeuJLcmW5jMZbA" value="TAFAMRX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_FBKC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_FBKS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_FBKi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_FBKy2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_FBLC2KEeuJLcmW5jMZbA" name="TAFAMR" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_FBLS2KEeuJLcmW5jMZbA" value="TAFAMR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_FBLi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_FBLy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_FBMC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_FBMS2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_OyIC2KEeuJLcmW5jMZbA" name="REFAMRX" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_OyIS2KEeuJLcmW5jMZbA" value="REFAMRX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_OyIi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_OyIy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_OyJC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_OyJS2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_OyJi2KEeuJLcmW5jMZbA" name="REFAMR" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_OyJy2KEeuJLcmW5jMZbA" value="REFAMR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_OyKC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_OyKS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_OyKi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_OyKy2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_X8EC2KEeuJLcmW5jMZbA" name="COFAMR" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_X8ES2KEeuJLcmW5jMZbA" value="COFAMR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_X8Ei2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_X8Ey2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_X8FC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_X8FS2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_X8Fi2KEeuJLcmW5jMZbA" name="FAM_0001" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_X8Fy2KEeuJLcmW5jMZbA" value="FAM_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_X8GC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_X8GS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_X8Gi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_X8Gy2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_X8HC2KEeuJLcmW5jMZbA" name="FAM_0002" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_X8HS2KEeuJLcmW5jMZbA" value="FAM_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_X8Hi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_X8Hy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_X8IC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_X8IS2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_htEC2KEeuJLcmW5jMZbA" name="FAM_0003" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_htES2KEeuJLcmW5jMZbA" value="FAM_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_htEi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_htEy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_htFC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_htFS2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_htFi2KEeuJLcmW5jMZbA" name="PRODNAT" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_htFy2KEeuJLcmW5jMZbA" value="PRODNAT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_htGC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_htGS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_htGi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_htGy2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_reEC2KEeuJLcmW5jMZbA" name="REFUN" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_reES2KEeuJLcmW5jMZbA" value="REFUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_reEi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_reEy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_reFC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_reFS2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_reFi2KEeuJLcmW5jMZbA" name="ACHUN" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_reFy2KEeuJLcmW5jMZbA" value="ACHUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_reGC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_reGS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_reGi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_reGy2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_reHC2KEeuJLcmW5jMZbA" name="STUN" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_reHS2KEeuJLcmW5jMZbA" value="STUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_reHi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_reHy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_reIC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_reIS2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_0oAC2KEeuJLcmW5jMZbA" name="VENUN" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_0oAS2KEeuJLcmW5jMZbA" value="VENUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_0oAi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_0oAy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_0oBC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_0oBS2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_0oBi2KEeuJLcmW5jMZbA" name="POIUN" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_0oBy2KEeuJLcmW5jMZbA" value="POIUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_0oCC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_0oCS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_0oCi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_0oCy2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_K_-ZAC2KEeuJLcmW5jMZbA" name="VOLUN" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_K_-ZAS2KEeuJLcmW5jMZbA" value="VOLUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_K_-ZAi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_K_-ZAy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_K_-ZBC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_K_-ZBS2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LACDYC2KEeuJLcmW5jMZbA" name="DIMUN" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_LACDYS2KEeuJLcmW5jMZbA" value="DIMUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LACDYi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LACDYy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LACDZC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LACDZS2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LACDZi2KEeuJLcmW5jMZbA" name="CPTV" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_LACDZy2KEeuJLcmW5jMZbA" value="CPTV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LACDaC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LACDaS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LACDai2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LACDay2KEeuJLcmW5jMZbA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LACDbC2KEeuJLcmW5jMZbA" name="CPTA" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_LACDbS2KEeuJLcmW5jMZbA" value="CPTA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LACDbi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LACDby2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LACDcC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LACDcS2KEeuJLcmW5jMZbA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LAL0YC2KEeuJLcmW5jMZbA" name="CPTS" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_LAL0YS2KEeuJLcmW5jMZbA" value="CPTS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LAL0Yi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LAL0Yy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LAL0ZC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LAL0ZS2KEeuJLcmW5jMZbA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LAL0Zi2KEeuJLcmW5jMZbA" name="TPFR_0001" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_LAL0Zy2KEeuJLcmW5jMZbA" value="TPFR_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LAL0aC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LAL0aS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LAL0ai2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LAL0ay2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LAL0bC2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LAL0bS2KEeuJLcmW5jMZbA" name="TPFR_0002" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_LAL0bi2KEeuJLcmW5jMZbA" value="TPFR_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LAL0by2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LAL0cC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LAL0cS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LAL0ci2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LAL0cy2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LAU-UC2KEeuJLcmW5jMZbA" name="TPFR_0003" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_LAU-US2KEeuJLcmW5jMZbA" value="TPFR_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LAU-Ui2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LAU-Uy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LAU-VC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LAU-VS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LAU-Vi2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LAU-Vy2KEeuJLcmW5jMZbA" name="TVANOM" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_LAU-WC2KEeuJLcmW5jMZbA" value="TVANOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LAU-WS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LAU-Wi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LAU-Wy2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LAU-XC2KEeuJLcmW5jMZbA" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LAevUC2KEeuJLcmW5jMZbA" name="TVAUN" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_LAevUS2KEeuJLcmW5jMZbA" value="TVAUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LAevUi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LAevUy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LAevVC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LAevVS2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LAevVi2KEeuJLcmW5jMZbA" name="TVARGCOD" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_LAevVy2KEeuJLcmW5jMZbA" value="TVARGCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LAevWC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LAevWS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LAevWi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LAevWy2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LAevXC2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LAevXS2KEeuJLcmW5jMZbA" name="EDCOD" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_LAevXi2KEeuJLcmW5jMZbA" value="EDCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LAevXy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LAevYC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LAevYS2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LAevYi2KEeuJLcmW5jMZbA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LAogUC2KEeuJLcmW5jMZbA" name="GRICOD" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_LAogUS2KEeuJLcmW5jMZbA" value="GRICOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LAogUi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LAogUy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LAogVC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LAogVS2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LAogVi2KEeuJLcmW5jMZbA" name="ZONA" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_LAogVy2KEeuJLcmW5jMZbA" value="ZONA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LAogWC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LAogWS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LAogWi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LAogWy2KEeuJLcmW5jMZbA" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LAxqQC2KEeuJLcmW5jMZbA" name="MEDIA" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_LAxqQS2KEeuJLcmW5jMZbA" value="MEDIA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LAxqQi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LAxqQy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LAxqRC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LAxqRS2KEeuJLcmW5jMZbA" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LAxqRi2KEeuJLcmW5jMZbA" name="HTML" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_LAxqRy2KEeuJLcmW5jMZbA" value="HTML"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LAxqSC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LAxqSS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LAxqSi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LAxqSy2KEeuJLcmW5jMZbA" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LAxqTC2KEeuJLcmW5jMZbA" name="AXEMSK" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_LAxqTS2KEeuJLcmW5jMZbA" value="AXEMSK"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LAxqTi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LAxqTy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LAxqUC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LAxqUS2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LA7bQC2KEeuJLcmW5jMZbA" name="AXENO" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_LA7bQS2KEeuJLcmW5jMZbA" value="AXENO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LA7bQi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LA7bQy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LA7bRC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LA7bRS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LA7bRi2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LA_FoC2KEeuJLcmW5jMZbA" name="ABCCOD" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_LA_FoS2KEeuJLcmW5jMZbA" value="ABCCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LA_Foi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LA_Foy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LA_FpC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LA_FpS2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LA_Fpi2KEeuJLcmW5jMZbA" name="QUESTION" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_LA_Fpy2KEeuJLcmW5jMZbA" value="QUESTION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LA_FqC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LA_FqS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LA_Fqi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LA_Fqy2KEeuJLcmW5jMZbA" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LBSnoC2KEeuJLcmW5jMZbA" name="CBNGESCOD" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_LBSnoS2KEeuJLcmW5jMZbA" value="CBNGESCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LBSnoi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LBSnoy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LBSnpC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LBSnpS2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LBSnpi2KEeuJLcmW5jMZbA" name="COMPETCOD" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_LBSnpy2KEeuJLcmW5jMZbA" value="COMPETCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LBSnqC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LBSnqS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LBSnqi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LBSnqy2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LBSnrC2KEeuJLcmW5jMZbA" name="CPTACES" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_LBSnrS2KEeuJLcmW5jMZbA" value="CPTACES"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LBSnri2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LBSnry2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LBSnsC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LBSnsS2KEeuJLcmW5jMZbA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LBbxkC2KEeuJLcmW5jMZbA" name="CPTVCES" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_LBbxkS2KEeuJLcmW5jMZbA" value="CPTVCES"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LBbxki2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LBbxky2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LBbxlC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LBbxlS2KEeuJLcmW5jMZbA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LBbxli2KEeuJLcmW5jMZbA" name="ACHTAFAMRX" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_LBbxly2KEeuJLcmW5jMZbA" value="ACHTAFAMRX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LBbxmC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LBbxmS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LBbxmi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LBbxmy2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LBbxnC2KEeuJLcmW5jMZbA" name="ACHTAFAMR" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_LBbxnS2KEeuJLcmW5jMZbA" value="ACHTAFAMR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LBbxni2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LBbxny2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LBbxoC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LBbxoS2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LBlikC2KEeuJLcmW5jMZbA" name="ACHREFAMRX" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_LBlikS2KEeuJLcmW5jMZbA" value="ACHREFAMRX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LBliki2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LBliky2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LBlilC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LBlilS2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LBlili2KEeuJLcmW5jMZbA" name="ACHREFAMR" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_LBlily2KEeuJLcmW5jMZbA" value="ACHREFAMR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LBlimC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LBlimS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LBlimi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LBlimy2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LBlinC2KEeuJLcmW5jMZbA" name="SURFUN" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_LBlinS2KEeuJLcmW5jMZbA" value="SURFUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LBlini2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LBliny2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LBlioC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LBlioS2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LBvTkC2KEeuJLcmW5jMZbA" name="COEFPTS" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_LBvTkS2KEeuJLcmW5jMZbA" value="COEFPTS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LBvTki2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LBvTky2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LBvTlC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LBvTlS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LBvTli2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LBvTly2KEeuJLcmW5jMZbA" name="CONFIGURATEURFORMULAIRE" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_LBvTmC2KEeuJLcmW5jMZbA" value="CONFIGURATEURFORMULAIRE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LBvTmS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LBvTmi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LBvTmy2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LBvTnC2KEeuJLcmW5jMZbA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LB4dgC2KEeuJLcmW5jMZbA" name="CONFIGURATEURCHEMINCOD" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_LB4dgS2KEeuJLcmW5jMZbA" value="CONFIGURATEURCHEMINCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LB4dgi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LB4dgy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LB4dhC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LB4dhS2KEeuJLcmW5jMZbA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LB8u8C2KEeuJLcmW5jMZbA" name="SMCFL" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_LB8u8S2KEeuJLcmW5jMZbA" value="SMCFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LB8u8i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LB8u8y2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LB8u9C2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LB8u9S2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LB8u9i2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LB8u9y2KEeuJLcmW5jMZbA" name="SMCVISUFL" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_LB8u-C2KEeuJLcmW5jMZbA" value="SMCVISUFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LB8u-S2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LB8u-i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LB8u-y2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LB8u_C2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LB8u_S2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LB8u_i2KEeuJLcmW5jMZbA" name="USERCRDH" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_LB8u_y2KEeuJLcmW5jMZbA" value="USERCRDH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LB8vAC2KEeuJLcmW5jMZbA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LB8vAS2KEeuJLcmW5jMZbA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LB8vAi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LB8vAy2KEeuJLcmW5jMZbA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LB8vBC2KEeuJLcmW5jMZbA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LCF44C2KEeuJLcmW5jMZbA" name="USERMODH" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_LCF44S2KEeuJLcmW5jMZbA" value="USERMODH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LCF44i2KEeuJLcmW5jMZbA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LCF44y2KEeuJLcmW5jMZbA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LCF45C2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LCF45S2KEeuJLcmW5jMZbA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LCF45i2KEeuJLcmW5jMZbA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LCF45y2KEeuJLcmW5jMZbA" name="HSDT" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_LCF46C2KEeuJLcmW5jMZbA" value="HSDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LCF46S2KEeuJLcmW5jMZbA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LCF46i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LCF46y2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LCF47C2KEeuJLcmW5jMZbA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LCF47S2KEeuJLcmW5jMZbA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LCPp4C2KEeuJLcmW5jMZbA" name="DOPDH" position="74">
        <attribute defType="com.stambia.rdbms.column.name" id="_LCPp4S2KEeuJLcmW5jMZbA" value="DOPDH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LCPp4i2KEeuJLcmW5jMZbA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LCPp4y2KEeuJLcmW5jMZbA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LCPp5C2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LCPp5S2KEeuJLcmW5jMZbA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LCPp5i2KEeuJLcmW5jMZbA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LCPp5y2KEeuJLcmW5jMZbA" name="CENOTE" position="75">
        <attribute defType="com.stambia.rdbms.column.name" id="_LCPp6C2KEeuJLcmW5jMZbA" value="CENOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LCPp6S2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LCPp6i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LCPp6y2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LCPp7C2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LCPp7S2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LCPp7i2KEeuJLcmW5jMZbA" name="NOTE" position="76">
        <attribute defType="com.stambia.rdbms.column.name" id="_LCPp7y2KEeuJLcmW5jMZbA" value="NOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LCPp8C2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LCPp8S2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LCPp8i2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LCPp8y2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LCPp9C2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LCZa4C2KEeuJLcmW5jMZbA" name="POIN" position="77">
        <attribute defType="com.stambia.rdbms.column.name" id="_LCZa4S2KEeuJLcmW5jMZbA" value="POIN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LCZa4i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LCZa4y2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LCZa5C2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LCZa5S2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LCZa5i2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LCZa5y2KEeuJLcmW5jMZbA" name="POIB" position="78">
        <attribute defType="com.stambia.rdbms.column.name" id="_LCZa6C2KEeuJLcmW5jMZbA" value="POIB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LCZa6S2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LCZa6i2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LCZa6y2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LCZa7C2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LCZa7S2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LCZa7i2KEeuJLcmW5jMZbA" name="GRISAIS" position="79">
        <attribute defType="com.stambia.rdbms.column.name" id="_LCZa7y2KEeuJLcmW5jMZbA" value="GRISAIS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LCZa8C2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LCZa8S2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LCZa8i2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LCZa8y2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LCZa9C2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LCik0C2KEeuJLcmW5jMZbA" name="RESTOTQTE" position="80">
        <attribute defType="com.stambia.rdbms.column.name" id="_LCik0S2KEeuJLcmW5jMZbA" value="RESTOTQTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LCik0i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LCik0y2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LCik1C2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LCik1S2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LCik1i2KEeuJLcmW5jMZbA" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LCik1y2KEeuJLcmW5jMZbA" name="CDECLQTE" position="81">
        <attribute defType="com.stambia.rdbms.column.name" id="_LCik2C2KEeuJLcmW5jMZbA" value="CDECLQTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LCik2S2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LCik2i2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LCik2y2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LCik3C2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LCik3S2KEeuJLcmW5jMZbA" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LCsV0C2KEeuJLcmW5jMZbA" name="CDEFOQTE" position="82">
        <attribute defType="com.stambia.rdbms.column.name" id="_LCsV0S2KEeuJLcmW5jMZbA" value="CDEFOQTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LCsV0i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LCsV0y2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LCsV1C2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LCsV1S2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LCsV1i2KEeuJLcmW5jMZbA" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LCsV1y2KEeuJLcmW5jMZbA" name="STTOTQTE" position="83">
        <attribute defType="com.stambia.rdbms.column.name" id="_LCsV2C2KEeuJLcmW5jMZbA" value="STTOTQTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LCsV2S2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LCsV2i2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LCsV2y2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LCsV3C2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LCsV3S2KEeuJLcmW5jMZbA" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LCsV3i2KEeuJLcmW5jMZbA" name="INVDT" position="84">
        <attribute defType="com.stambia.rdbms.column.name" id="_LCsV3y2KEeuJLcmW5jMZbA" value="INVDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LCsV4C2KEeuJLcmW5jMZbA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LCsV4S2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LCsV4i2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LCsV4y2KEeuJLcmW5jMZbA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LCsV5C2KEeuJLcmW5jMZbA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LC1fwC2KEeuJLcmW5jMZbA" name="GARJRNB" position="85">
        <attribute defType="com.stambia.rdbms.column.name" id="_LC1fwS2KEeuJLcmW5jMZbA" value="GARJRNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LC1fwi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LC1fwy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LC1fxC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LC1fxS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LC1fxi2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LC5xMC2KEeuJLcmW5jMZbA" name="STCOD" position="86">
        <attribute defType="com.stambia.rdbms.column.name" id="_LC5xMS2KEeuJLcmW5jMZbA" value="STCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LC5xMi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LC5xMy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LC5xNC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LC5xNS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LC5xNi2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LC5xNy2KEeuJLcmW5jMZbA" name="GICOD" position="87">
        <attribute defType="com.stambia.rdbms.column.name" id="_LC5xOC2KEeuJLcmW5jMZbA" value="GICOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LC5xOS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LC5xOi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LC5xOy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LC5xPC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LC5xPS2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LC5xPi2KEeuJLcmW5jMZbA" name="VOL" position="88">
        <attribute defType="com.stambia.rdbms.column.name" id="_LC5xPy2KEeuJLcmW5jMZbA" value="VOL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LC5xQC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LC5xQS2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LC5xQi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LC5xQy2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LC5xRC2KEeuJLcmW5jMZbA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LDC7IC2KEeuJLcmW5jMZbA" name="DIM_0001" position="89">
        <attribute defType="com.stambia.rdbms.column.name" id="_LDC7IS2KEeuJLcmW5jMZbA" value="DIM_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LDC7Ii2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LDC7Iy2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LDC7JC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LDC7JS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LDC7Ji2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LDC7Jy2KEeuJLcmW5jMZbA" name="DIM_0002" position="90">
        <attribute defType="com.stambia.rdbms.column.name" id="_LDC7KC2KEeuJLcmW5jMZbA" value="DIM_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LDC7KS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LDC7Ki2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LDC7Ky2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LDC7LC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LDC7LS2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LDC7Li2KEeuJLcmW5jMZbA" name="DIM_0003" position="91">
        <attribute defType="com.stambia.rdbms.column.name" id="_LDC7Ly2KEeuJLcmW5jMZbA" value="DIM_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LDC7MC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LDC7MS2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LDC7Mi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LDC7My2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LDC7NC2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LDMsIC2KEeuJLcmW5jMZbA" name="SREFCOD" position="92">
        <attribute defType="com.stambia.rdbms.column.name" id="_LDMsIS2KEeuJLcmW5jMZbA" value="SREFCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LDMsIi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LDMsIy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LDMsJC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LDMsJS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LDMsJi2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LDMsJy2KEeuJLcmW5jMZbA" name="OPSAIS_0001" position="93">
        <attribute defType="com.stambia.rdbms.column.name" id="_LDMsKC2KEeuJLcmW5jMZbA" value="OPSAIS_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LDMsKS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LDMsKi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LDMsKy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LDMsLC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LDMsLS2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LDWdIC2KEeuJLcmW5jMZbA" name="OPSAIS_0002" position="94">
        <attribute defType="com.stambia.rdbms.column.name" id="_LDWdIS2KEeuJLcmW5jMZbA" value="OPSAIS_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LDWdIi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LDWdIy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LDWdJC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LDWdJS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LDWdJi2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LDWdJy2KEeuJLcmW5jMZbA" name="OPSAIS_0003" position="95">
        <attribute defType="com.stambia.rdbms.column.name" id="_LDWdKC2KEeuJLcmW5jMZbA" value="OPSAIS_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LDWdKS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LDWdKi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LDWdKy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LDWdLC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LDWdLS2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LDWdLi2KEeuJLcmW5jMZbA" name="ZONN" position="96">
        <attribute defType="com.stambia.rdbms.column.name" id="_LDWdLy2KEeuJLcmW5jMZbA" value="ZONN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LDWdMC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LDWdMS2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LDWdMi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LDWdMy2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LDWdNC2KEeuJLcmW5jMZbA" value="16"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LDfnEC2KEeuJLcmW5jMZbA" name="MGTX" position="97">
        <attribute defType="com.stambia.rdbms.column.name" id="_LDfnES2KEeuJLcmW5jMZbA" value="MGTX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LDfnEi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LDfnEy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LDfnFC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LDfnFS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LDfnFi2KEeuJLcmW5jMZbA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LDfnFy2KEeuJLcmW5jMZbA" name="PERJRNB" position="98">
        <attribute defType="com.stambia.rdbms.column.name" id="_LDfnGC2KEeuJLcmW5jMZbA" value="PERJRNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LDfnGS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LDfnGi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LDfnGy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LDfnHC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LDfnHS2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LDfnHi2KEeuJLcmW5jMZbA" name="STVALCOD" position="99">
        <attribute defType="com.stambia.rdbms.column.name" id="_LDfnHy2KEeuJLcmW5jMZbA" value="STVALCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LDfnIC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LDfnIS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LDfnIi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LDfnIy2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LDfnJC2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LDpYEC2KEeuJLcmW5jMZbA" name="STSORCOD" position="100">
        <attribute defType="com.stambia.rdbms.column.name" id="_LDpYES2KEeuJLcmW5jMZbA" value="STSORCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LDpYEi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LDpYEy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LDpYFC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LDpYFS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LDpYFi2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LDpYFy2KEeuJLcmW5jMZbA" name="WEBCDECOD" position="101">
        <attribute defType="com.stambia.rdbms.column.name" id="_LDpYGC2KEeuJLcmW5jMZbA" value="WEBCDECOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LDpYGS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LDpYGi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LDpYGy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LDpYHC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LDpYHS2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LDpYHi2KEeuJLcmW5jMZbA" name="PVCOD" position="102">
        <attribute defType="com.stambia.rdbms.column.name" id="_LDpYHy2KEeuJLcmW5jMZbA" value="PVCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LDpYIC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LDpYIS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LDpYIi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LDpYIy2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LDpYJC2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LDyiAC2KEeuJLcmW5jMZbA" name="LGTYP" position="103">
        <attribute defType="com.stambia.rdbms.column.name" id="_LDyiAS2KEeuJLcmW5jMZbA" value="LGTYP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LDyiAi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LDyiAy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LDyiBC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LDyiBS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LDyiBi2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LD2zcC2KEeuJLcmW5jMZbA" name="STRES" position="104">
        <attribute defType="com.stambia.rdbms.column.name" id="_LD2zcS2KEeuJLcmW5jMZbA" value="STRES"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LD2zci2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LD2zcy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LD2zdC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LD2zdS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LD2zdi2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LD2zdy2KEeuJLcmW5jMZbA" name="CDEINQTE" position="105">
        <attribute defType="com.stambia.rdbms.column.name" id="_LD2zeC2KEeuJLcmW5jMZbA" value="CDEINQTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LD2zeS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LD2zei2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LD2zey2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LD2zfC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LD2zfS2KEeuJLcmW5jMZbA" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LEAkcC2KEeuJLcmW5jMZbA" name="CEJOINT" position="106">
        <attribute defType="com.stambia.rdbms.column.name" id="_LEAkcS2KEeuJLcmW5jMZbA" value="CEJOINT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LEAkci2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LEAkcy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LEAkdC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LEAkdS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LEAkdi2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LEAkdy2KEeuJLcmW5jMZbA" name="JOINT" position="107">
        <attribute defType="com.stambia.rdbms.column.name" id="_LEAkeC2KEeuJLcmW5jMZbA" value="JOINT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LEAkeS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LEAkei2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LEAkey2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LEAkfC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LEAkfS2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LEAkfi2KEeuJLcmW5jMZbA" name="REJALOF" position="108">
        <attribute defType="com.stambia.rdbms.column.name" id="_LEAkfy2KEeuJLcmW5jMZbA" value="REJALOF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LEAkgC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LEAkgS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LEAkgi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LEAkgy2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LEAkhC2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LEJuYC2KEeuJLcmW5jMZbA" name="REJALCDE" position="109">
        <attribute defType="com.stambia.rdbms.column.name" id="_LEJuYS2KEeuJLcmW5jMZbA" value="REJALCDE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LEJuYi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LEJuYy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LEJuZC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LEJuZS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LEJuZi2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LEJuZy2KEeuJLcmW5jMZbA" name="REJALOFJRNB" position="110">
        <attribute defType="com.stambia.rdbms.column.name" id="_LEJuaC2KEeuJLcmW5jMZbA" value="REJALOFJRNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LEJuaS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LEJuai2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LEJuay2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LEJubC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LEJubS2KEeuJLcmW5jMZbA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LETfYC2KEeuJLcmW5jMZbA" name="REJALCDEJRNB" position="111">
        <attribute defType="com.stambia.rdbms.column.name" id="_LETfYS2KEeuJLcmW5jMZbA" value="REJALCDEJRNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LETfYi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LETfYy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LETfZC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LETfZS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LETfZi2KEeuJLcmW5jMZbA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LETfZy2KEeuJLcmW5jMZbA" name="TOLERANCETX" position="112">
        <attribute defType="com.stambia.rdbms.column.name" id="_LETfaC2KEeuJLcmW5jMZbA" value="TOLERANCETX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LETfaS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LETfai2KEeuJLcmW5jMZbA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LETfay2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LETfbC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LETfbS2KEeuJLcmW5jMZbA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LETfbi2KEeuJLcmW5jMZbA" name="COMSAIS" position="113">
        <attribute defType="com.stambia.rdbms.column.name" id="_LETfby2KEeuJLcmW5jMZbA" value="COMSAIS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LETfcC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LETfcS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LETfci2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LETfcy2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LETfdC2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LEcpUC2KEeuJLcmW5jMZbA" name="SURF" position="114">
        <attribute defType="com.stambia.rdbms.column.name" id="_LEcpUS2KEeuJLcmW5jMZbA" value="SURF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LEcpUi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LEcpUy2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LEcpVC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LEcpVS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LEcpVi2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LEcpVy2KEeuJLcmW5jMZbA" name="COEFOIVOL" position="115">
        <attribute defType="com.stambia.rdbms.column.name" id="_LEcpWC2KEeuJLcmW5jMZbA" value="COEFOIVOL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LEcpWS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LEcpWi2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LEcpWy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LEcpXC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LEcpXS2KEeuJLcmW5jMZbA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LEmaUC2KEeuJLcmW5jMZbA" name="MANUN" position="116">
        <attribute defType="com.stambia.rdbms.column.name" id="_LEmaUS2KEeuJLcmW5jMZbA" value="MANUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LEmaUi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LEmaUy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LEmaVC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LEmaVS2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LEmaVi2KEeuJLcmW5jMZbA" name="STLGTABCCOD" position="117">
        <attribute defType="com.stambia.rdbms.column.name" id="_LEmaVy2KEeuJLcmW5jMZbA" value="STLGTABCCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LEmaWC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LEmaWS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LEmaWi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LEmaWy2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LEwLUC2KEeuJLcmW5jMZbA" name="STLGTHGCOD" position="118">
        <attribute defType="com.stambia.rdbms.column.name" id="_LEwLUS2KEeuJLcmW5jMZbA" value="STLGTHGCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LEwLUi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LEwLUy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LEwLVC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LEwLVS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LEwLVi2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LEz1sC2KEeuJLcmW5jMZbA" name="STLGTHGCOLINB" position="119">
        <attribute defType="com.stambia.rdbms.column.name" id="_LEz1sS2KEeuJLcmW5jMZbA" value="STLGTHGCOLINB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LEz1si2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LEz1sy2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LEz1tC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LEz1tS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LEz1ti2KEeuJLcmW5jMZbA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LEz1ty2KEeuJLcmW5jMZbA" name="RANGABC" position="120">
        <attribute defType="com.stambia.rdbms.column.name" id="_LEz1uC2KEeuJLcmW5jMZbA" value="RANGABC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LEz1uS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LEz1ui2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LEz1uy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LEz1vC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LEz1vS2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LEz1vi2KEeuJLcmW5jMZbA" name="PDP" position="121">
        <attribute defType="com.stambia.rdbms.column.name" id="_LEz1vy2KEeuJLcmW5jMZbA" value="PDP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LEz1wC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LEz1wS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LEz1wi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LEz1wy2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LEz1xC2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LE9msC2KEeuJLcmW5jMZbA" name="PDPPERCOD" position="122">
        <attribute defType="com.stambia.rdbms.column.name" id="_LE9msS2KEeuJLcmW5jMZbA" value="PDPPERCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LE9msi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LE9msy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LE9mtC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LE9mtS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LE9mti2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LE9mty2KEeuJLcmW5jMZbA" name="PDPDELOBT" position="123">
        <attribute defType="com.stambia.rdbms.column.name" id="_LE9muC2KEeuJLcmW5jMZbA" value="PDPDELOBT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LE9muS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LE9mui2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LE9muy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LE9mvC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LE9mvS2KEeuJLcmW5jMZbA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LE9mvi2KEeuJLcmW5jMZbA" name="PDPFERMEPERNB" position="124">
        <attribute defType="com.stambia.rdbms.column.name" id="_LE9mvy2KEeuJLcmW5jMZbA" value="PDPFERMEPERNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LE9mwC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LE9mwS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LE9mwi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LE9mwy2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LE9mxC2KEeuJLcmW5jMZbA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LFHXsC2KEeuJLcmW5jMZbA" name="PDPNEGOPERNB" position="125">
        <attribute defType="com.stambia.rdbms.column.name" id="_LFHXsS2KEeuJLcmW5jMZbA" value="PDPNEGOPERNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LFHXsi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LFHXsy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LFHXtC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LFHXtS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LFHXti2KEeuJLcmW5jMZbA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LFHXty2KEeuJLcmW5jMZbA" name="PDPLOTQTE" position="126">
        <attribute defType="com.stambia.rdbms.column.name" id="_LFHXuC2KEeuJLcmW5jMZbA" value="PDPLOTQTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LFHXuS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LFHXui2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LFHXuy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LFHXvC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LFHXvS2KEeuJLcmW5jMZbA" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LFHXvi2KEeuJLcmW5jMZbA" name="PDPREGPERNB" position="127">
        <attribute defType="com.stambia.rdbms.column.name" id="_LFHXvy2KEeuJLcmW5jMZbA" value="PDPREGPERNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LFHXwC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LFHXwS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LFHXwi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LFHXwy2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LFHXxC2KEeuJLcmW5jMZbA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LFQhoC2KEeuJLcmW5jMZbA" name="PDPECARTMAX" position="128">
        <attribute defType="com.stambia.rdbms.column.name" id="_LFQhoS2KEeuJLcmW5jMZbA" value="PDPECARTMAX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LFQhoi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LFQhoy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LFQhpC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LFQhpS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LFQhpi2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LFQhpy2KEeuJLcmW5jMZbA" name="TVAART" position="129">
        <attribute defType="com.stambia.rdbms.column.name" id="_LFQhqC2KEeuJLcmW5jMZbA" value="TVAART"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LFQhqS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LFQhqi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LFQhqy2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LFQhrC2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LFQhrS2KEeuJLcmW5jMZbA" name="TVAARTA" position="130">
        <attribute defType="com.stambia.rdbms.column.name" id="_LFQhri2KEeuJLcmW5jMZbA" value="TVAARTA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LFQhry2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LFQhsC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LFQhsS2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LFQhsi2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LFaSoC2KEeuJLcmW5jMZbA" name="WMQTEIMP" position="131">
        <attribute defType="com.stambia.rdbms.column.name" id="_LFaSoS2KEeuJLcmW5jMZbA" value="WMQTEIMP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LFaSoi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LFaSoy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LFaSpC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LFaSpS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LFaSpi2KEeuJLcmW5jMZbA" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LFaSpy2KEeuJLcmW5jMZbA" name="WMREAPFROIDFL" position="132">
        <attribute defType="com.stambia.rdbms.column.name" id="_LFaSqC2KEeuJLcmW5jMZbA" value="WMREAPFROIDFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LFaSqS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LFaSqi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LFaSqy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LFaSrC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LFaSrS2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LFjckC2KEeuJLcmW5jMZbA" name="SREF1" position="133">
        <attribute defType="com.stambia.rdbms.column.name" id="_LFjckS2KEeuJLcmW5jMZbA" value="SREF1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LFjcki2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LFjcky2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LFjclC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LFjclS2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LFjcli2KEeuJLcmW5jMZbA" name="SREF2" position="134">
        <attribute defType="com.stambia.rdbms.column.name" id="_LFjcly2KEeuJLcmW5jMZbA" value="SREF2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LFjcmC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LFjcmS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LFjcmi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LFjcmy2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LFjcnC2KEeuJLcmW5jMZbA" name="PREFDVNO" position="135">
        <attribute defType="com.stambia.rdbms.column.name" id="_LFjcnS2KEeuJLcmW5jMZbA" value="PREFDVNO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LFjcni2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LFjcny2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LFjcoC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LFjcoS2KEeuJLcmW5jMZbA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LFtNkC2KEeuJLcmW5jMZbA" name="DVNO" position="136">
        <attribute defType="com.stambia.rdbms.column.name" id="_LFtNkS2KEeuJLcmW5jMZbA" value="DVNO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LFtNki2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LFtNky2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LFtNlC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LFtNlS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LFtNli2KEeuJLcmW5jMZbA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LFw38C2KEeuJLcmW5jMZbA" name="DTIND" position="137">
        <attribute defType="com.stambia.rdbms.column.name" id="_LFw38S2KEeuJLcmW5jMZbA" value="DTIND"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LFw38i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LFw38y2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LFw39C2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LFw39S2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LFw39i2KEeuJLcmW5jMZbA" name="TIERSCLI" position="138">
        <attribute defType="com.stambia.rdbms.column.name" id="_LFw39y2KEeuJLcmW5jMZbA" value="TIERSCLI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LFw3-C2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LFw3-S2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LFw3-i2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LFw3-y2KEeuJLcmW5jMZbA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LFw3_C2KEeuJLcmW5jMZbA" name="REVUART" position="139">
        <attribute defType="com.stambia.rdbms.column.name" id="_LFw3_S2KEeuJLcmW5jMZbA" value="REVUART"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LFw3_i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LFw3_y2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LFw4AC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LFw4AS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LFw4Ai2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LF6o8C2KEeuJLcmW5jMZbA" name="ICPFL" position="140">
        <attribute defType="com.stambia.rdbms.column.name" id="_LF6o8S2KEeuJLcmW5jMZbA" value="ICPFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LF6o8i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LF6o8y2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LF6o9C2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LF6o9S2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LF6o9i2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LF6o9y2KEeuJLcmW5jMZbA" name="BUDGETCOD" position="141">
        <attribute defType="com.stambia.rdbms.column.name" id="_LF6o-C2KEeuJLcmW5jMZbA" value="BUDGETCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LF6o-S2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LF6o-i2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LF6o-y2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LF6o_C2KEeuJLcmW5jMZbA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LGDy4C2KEeuJLcmW5jMZbA" name="TRAVUN" position="142">
        <attribute defType="com.stambia.rdbms.column.name" id="_LGDy4S2KEeuJLcmW5jMZbA" value="TRAVUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LGDy4i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LGDy4y2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LGDy5C2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LGDy5S2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LGDy5i2KEeuJLcmW5jMZbA" name="PRIXMOY" position="143">
        <attribute defType="com.stambia.rdbms.column.name" id="_LGDy5y2KEeuJLcmW5jMZbA" value="PRIXMOY"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LGDy6C2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LGDy6S2KEeuJLcmW5jMZbA" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LGDy6i2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LGDy6y2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LGDy7C2KEeuJLcmW5jMZbA" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LGDy7S2KEeuJLcmW5jMZbA" name="CONTRATCOD" position="144">
        <attribute defType="com.stambia.rdbms.column.name" id="_LGDy7i2KEeuJLcmW5jMZbA" value="CONTRATCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LGDy7y2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LGDy8C2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LGDy8S2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LGDy8i2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LGNj4C2KEeuJLcmW5jMZbA" name="CONTRATFAM" position="145">
        <attribute defType="com.stambia.rdbms.column.name" id="_LGNj4S2KEeuJLcmW5jMZbA" value="CONTRATFAM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LGNj4i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LGNj4y2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LGNj5C2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LGNj5S2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LGNj5i2KEeuJLcmW5jMZbA" name="EANAUTOFL" position="146">
        <attribute defType="com.stambia.rdbms.column.name" id="_LGNj5y2KEeuJLcmW5jMZbA" value="EANAUTOFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LGNj6C2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LGNj6S2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LGNj6i2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LGNj6y2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LGNj7C2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LGWt0C2KEeuJLcmW5jMZbA" name="TYPEARTCOD" position="147">
        <attribute defType="com.stambia.rdbms.column.name" id="_LGWt0S2KEeuJLcmW5jMZbA" value="TYPEARTCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LGWt0i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LGWt0y2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LGWt1C2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LGWt1S2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LGWt1i2KEeuJLcmW5jMZbA" name="RESJRNB" position="148">
        <attribute defType="com.stambia.rdbms.column.name" id="_LGWt1y2KEeuJLcmW5jMZbA" value="RESJRNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LGWt2C2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LGWt2S2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LGWt2i2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LGWt2y2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LGWt3C2KEeuJLcmW5jMZbA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LGge0C2KEeuJLcmW5jMZbA" name="MARCHE" position="149">
        <attribute defType="com.stambia.rdbms.column.name" id="_LGge0S2KEeuJLcmW5jMZbA" value="MARCHE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LGge0i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LGge0y2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LGge1C2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LGge1S2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LGge1i2KEeuJLcmW5jMZbA" name="QTE" position="150">
        <attribute defType="com.stambia.rdbms.column.name" id="_LGge1y2KEeuJLcmW5jMZbA" value="QTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LGge2C2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LGge2S2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LGge2i2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LGge2y2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LGge3C2KEeuJLcmW5jMZbA" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LGge3S2KEeuJLcmW5jMZbA" name="TIERSREF" position="151">
        <attribute defType="com.stambia.rdbms.column.name" id="_LGge3i2KEeuJLcmW5jMZbA" value="TIERSREF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LGge3y2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LGge4C2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LGge4S2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LGge4i2KEeuJLcmW5jMZbA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LGq24C2KEeuJLcmW5jMZbA" name="EPHEMEREFL" position="152">
        <attribute defType="com.stambia.rdbms.column.name" id="_LGq24S2KEeuJLcmW5jMZbA" value="EPHEMEREFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LGq24i2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LGq24y2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LGq25C2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LGq25S2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LGq25i2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LGt6MC2KEeuJLcmW5jMZbA" name="ACHUNREF" position="153">
        <attribute defType="com.stambia.rdbms.column.name" id="_LGt6MS2KEeuJLcmW5jMZbA" value="ACHUNREF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LGt6Mi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LGt6My2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LGt6NC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LGt6NS2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LGt6Ni2KEeuJLcmW5jMZbA" name="QTEMIN" position="154">
        <attribute defType="com.stambia.rdbms.column.name" id="_LGt6Ny2KEeuJLcmW5jMZbA" value="QTEMIN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LGt6OC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LGt6OS2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LGt6Oi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LGt6Oy2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LGt6PC2KEeuJLcmW5jMZbA" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LG3rMC2KEeuJLcmW5jMZbA" name="QTEPAR" position="155">
        <attribute defType="com.stambia.rdbms.column.name" id="_LG3rMS2KEeuJLcmW5jMZbA" value="QTEPAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LG3rMi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LG3rMy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LG3rNC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LG3rNS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LG3rNi2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LG3rNy2KEeuJLcmW5jMZbA" name="CPTI" position="156">
        <attribute defType="com.stambia.rdbms.column.name" id="_LG3rOC2KEeuJLcmW5jMZbA" value="CPTI"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LG3rOS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LG3rOi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LG3rOy2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LG3rPC2KEeuJLcmW5jMZbA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LG3rPS2KEeuJLcmW5jMZbA" name="VISA_0001" position="157">
        <attribute defType="com.stambia.rdbms.column.name" id="_LG3rPi2KEeuJLcmW5jMZbA" value="VISA_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LG3rPy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LG3rQC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LG3rQS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LG3rQi2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LG3rQy2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LHBcMC2KEeuJLcmW5jMZbA" name="VISA_0002" position="158">
        <attribute defType="com.stambia.rdbms.column.name" id="_LHBcMS2KEeuJLcmW5jMZbA" value="VISA_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LHBcMi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LHBcMy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LHBcNC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LHBcNS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LHBcNi2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LHBcNy2KEeuJLcmW5jMZbA" name="VISA_0003" position="159">
        <attribute defType="com.stambia.rdbms.column.name" id="_LHBcOC2KEeuJLcmW5jMZbA" value="VISA_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LHBcOS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LHBcOi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LHBcOy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LHBcPC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LHBcPS2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LHBcPi2KEeuJLcmW5jMZbA" name="VISA_0004" position="160">
        <attribute defType="com.stambia.rdbms.column.name" id="_LHBcPy2KEeuJLcmW5jMZbA" value="VISA_0004"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LHBcQC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LHBcQS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LHBcQi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LHBcQy2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LHBcRC2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LHKmIC2KEeuJLcmW5jMZbA" name="VISA_0005" position="161">
        <attribute defType="com.stambia.rdbms.column.name" id="_LHKmIS2KEeuJLcmW5jMZbA" value="VISA_0005"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LHKmIi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LHKmIy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LHKmJC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LHKmJS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LHKmJi2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LHKmJy2KEeuJLcmW5jMZbA" name="FRAISAPPTYPCOD" position="162">
        <attribute defType="com.stambia.rdbms.column.name" id="_LHKmKC2KEeuJLcmW5jMZbA" value="FRAISAPPTYPCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LHKmKS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LHKmKi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LHKmKy2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LHKmLC2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LHKmLS2KEeuJLcmW5jMZbA" name="TRANSITFL" position="163">
        <attribute defType="com.stambia.rdbms.column.name" id="_LHKmLi2KEeuJLcmW5jMZbA" value="TRANSITFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LHKmLy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LHKmMC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LHKmMS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LHKmMi2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LHKmMy2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LHUXIC2KEeuJLcmW5jMZbA" name="REFMO" position="164">
        <attribute defType="com.stambia.rdbms.column.name" id="_LHUXIS2KEeuJLcmW5jMZbA" value="REFMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LHUXIi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LHUXIy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LHUXJC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LHUXJS2KEeuJLcmW5jMZbA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LHUXJi2KEeuJLcmW5jMZbA" name="MOPREPQTE" position="165">
        <attribute defType="com.stambia.rdbms.column.name" id="_LHUXJy2KEeuJLcmW5jMZbA" value="MOPREPQTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LHUXKC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LHUXKS2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LHUXKi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LHUXKy2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LHUXLC2KEeuJLcmW5jMZbA" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LHeIIC2KEeuJLcmW5jMZbA" name="MOPREPQTEPAR" position="166">
        <attribute defType="com.stambia.rdbms.column.name" id="_LHeIIS2KEeuJLcmW5jMZbA" value="MOPREPQTEPAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LHeIIi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LHeIIy2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LHeIJC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LHeIJS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LHeIJi2KEeuJLcmW5jMZbA" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LHeIJy2KEeuJLcmW5jMZbA" name="MOPREPQTETYP" position="167">
        <attribute defType="com.stambia.rdbms.column.name" id="_LHeIKC2KEeuJLcmW5jMZbA" value="MOPREPQTETYP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LHeIKS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LHeIKi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LHeIKy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LHeILC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LHeILS2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LHeILi2KEeuJLcmW5jMZbA" name="MOPOSEQTE" position="168">
        <attribute defType="com.stambia.rdbms.column.name" id="_LHeILy2KEeuJLcmW5jMZbA" value="MOPOSEQTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LHeIMC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LHeIMS2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LHeIMi2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LHeIMy2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LHeINC2KEeuJLcmW5jMZbA" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LHnSEC2KEeuJLcmW5jMZbA" name="MOPOSEQTEPAR" position="169">
        <attribute defType="com.stambia.rdbms.column.name" id="_LHnSES2KEeuJLcmW5jMZbA" value="MOPOSEQTEPAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LHnSEi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LHnSEy2KEeuJLcmW5jMZbA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LHnSFC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LHnSFS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LHnSFi2KEeuJLcmW5jMZbA" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LHrjgC2KEeuJLcmW5jMZbA" name="MOPOSEQTETYP" position="170">
        <attribute defType="com.stambia.rdbms.column.name" id="_LHrjgS2KEeuJLcmW5jMZbA" value="MOPOSEQTETYP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LHrjgi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LHrjgy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LHrjhC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LHrjhS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LHrjhi2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LHrjhy2KEeuJLcmW5jMZbA" name="CREDITIMPOTP" position="171">
        <attribute defType="com.stambia.rdbms.column.name" id="_LHrjiC2KEeuJLcmW5jMZbA" value="CREDITIMPOTP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LHrjiS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LHrjii2KEeuJLcmW5jMZbA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LHrjiy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LHrjjC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LHrjjS2KEeuJLcmW5jMZbA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LH1UgC2KEeuJLcmW5jMZbA" name="RISTOURNFL" position="172">
        <attribute defType="com.stambia.rdbms.column.name" id="_LH1UgS2KEeuJLcmW5jMZbA" value="RISTOURNFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LH1Ugi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LH1Ugy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LH1UhC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LH1UhS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LH1Uhi2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LH1Uhy2KEeuJLcmW5jMZbA" name="FRAISDOUANEFL" position="173">
        <attribute defType="com.stambia.rdbms.column.name" id="_LH1UiC2KEeuJLcmW5jMZbA" value="FRAISDOUANEFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LH1UiS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LH1Uii2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LH1Uiy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LH1UjC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LH1UjS2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LH-ecC2KEeuJLcmW5jMZbA" name="TAGID" position="174">
        <attribute defType="com.stambia.rdbms.column.name" id="_LH-ecS2KEeuJLcmW5jMZbA" value="TAGID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LH-eci2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LH-ecy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LH-edC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LH-edS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LH-edi2KEeuJLcmW5jMZbA" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LH-edy2KEeuJLcmW5jMZbA" name="TVANASSUJETTIEFL" position="175">
        <attribute defType="com.stambia.rdbms.column.name" id="_LH-eeC2KEeuJLcmW5jMZbA" value="TVANASSUJETTIEFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LH-eeS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LH-eei2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LH-eey2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LH-efC2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LH-efS2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LH-efi2KEeuJLcmW5jMZbA" name="PG_REFCONSTRUCTEUR" position="176">
        <attribute defType="com.stambia.rdbms.column.name" id="_LH-efy2KEeuJLcmW5jMZbA" value="PG_REFCONSTRUCTEUR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LH-egC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LH-egS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LH-egi2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LH-egy2KEeuJLcmW5jMZbA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LIIPcC2KEeuJLcmW5jMZbA" name="PG_REFCOMMERCIALE" position="177">
        <attribute defType="com.stambia.rdbms.column.name" id="_LIIPcS2KEeuJLcmW5jMZbA" value="PG_REFCOMMERCIALE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LIIPci2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LIIPcy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LIIPdC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LIIPdS2KEeuJLcmW5jMZbA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LIIPdi2KEeuJLcmW5jMZbA" name="PG_MARQUE" position="178">
        <attribute defType="com.stambia.rdbms.column.name" id="_LIIPdy2KEeuJLcmW5jMZbA" value="PG_MARQUE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LIIPeC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LIIPeS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LIIPei2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LIIPey2KEeuJLcmW5jMZbA" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LIRZYC2KEeuJLcmW5jMZbA" name="PG_CLASSEENERGETIQUE" position="179">
        <attribute defType="com.stambia.rdbms.column.name" id="_LIRZYS2KEeuJLcmW5jMZbA" value="PG_CLASSEENERGETIQUE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LIRZYi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LIRZYy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LIRZZC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LIRZZS2KEeuJLcmW5jMZbA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LIRZZi2KEeuJLcmW5jMZbA" name="PG_ARTPUBLIQUE" position="180">
        <attribute defType="com.stambia.rdbms.column.name" id="_LIRZZy2KEeuJLcmW5jMZbA" value="PG_ARTPUBLIQUE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LIRZaC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LIRZaS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LIRZai2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LIRZay2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LIRZbC2KEeuJLcmW5jMZbA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LIRZbS2KEeuJLcmW5jMZbA" name="PG_ORDRE" position="181">
        <attribute defType="com.stambia.rdbms.column.name" id="_LIRZbi2KEeuJLcmW5jMZbA" value="PG_ORDRE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LIRZby2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LIRZcC2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LIRZcS2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LIRZci2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LIRZcy2KEeuJLcmW5jMZbA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LIbKYC2KEeuJLcmW5jMZbA" name="PG_PAGECATALOGUE" position="182">
        <attribute defType="com.stambia.rdbms.column.name" id="_LIbKYS2KEeuJLcmW5jMZbA" value="PG_PAGECATALOGUE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LIbKYi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_LIbKYy2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LIbKZC2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LIbKZS2KEeuJLcmW5jMZbA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LIbKZi2KEeuJLcmW5jMZbA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LIbKZy2KEeuJLcmW5jMZbA" name="PG_ARTCOBAL" position="183">
        <attribute defType="com.stambia.rdbms.column.name" id="_LIbKaC2KEeuJLcmW5jMZbA" value="PG_ARTCOBAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LIbKaS2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LIbKai2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LIbKay2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LIbKbC2KEeuJLcmW5jMZbA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LIk7YC2KEeuJLcmW5jMZbA" name="PG_ARTLANTIN" position="184">
        <attribute defType="com.stambia.rdbms.column.name" id="_LIk7YS2KEeuJLcmW5jMZbA" value="PG_ARTLANTIN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LIk7Yi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LIk7Yy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LIk7ZC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LIk7ZS2KEeuJLcmW5jMZbA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_LIolwC2KEeuJLcmW5jMZbA" name="PG_ARTCHOMETTE" position="185">
        <attribute defType="com.stambia.rdbms.column.name" id="_LIolwS2KEeuJLcmW5jMZbA" value="PG_ARTCHOMETTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_LIolwi2KEeuJLcmW5jMZbA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_LIolwy2KEeuJLcmW5jMZbA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_LIolxC2KEeuJLcmW5jMZbA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_LIolxS2KEeuJLcmW5jMZbA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_LJloAC2KEeuJLcmW5jMZbA" name="PK__ART__FCD631073243EF98">
        <node defType="com.stambia.rdbms.colref" id="_LJloAS2KEeuJLcmW5jMZbA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_LJloAi2KEeuJLcmW5jMZbA" ref="#_K9wygC2KEeuJLcmW5jMZbA?fileId=_N_b74AcLEeu--f7ccO6NIw$type=md$name=ART_ID?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_i076YS43EeuCkMC4uUU-mA" name="TAR">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_i076Yi43EeuCkMC4uUU-mA" value="TAR"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_i076Yy43EeuCkMC4uUU-mA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_i1cQsC43EeuCkMC4uUU-mA" name="TAR_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_i1cQsS43EeuCkMC4uUU-mA" value="TAR_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i1cQsi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i1cQsy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i1cQtC43EeuCkMC4uUU-mA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i1cQtS43EeuCkMC4uUU-mA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i1cQti43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i1cQty43EeuCkMC4uUU-mA" name="CE1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_i1cQuC43EeuCkMC4uUU-mA" value="CE1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i1cQuS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i1cQui43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i1cQuy43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i1cQvC43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i1mBsC43EeuCkMC4uUU-mA" name="CE2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_i1mBsS43EeuCkMC4uUU-mA" value="CE2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i1mBsi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i1mBsy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i1mBtC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i1mBtS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i1vLoC43EeuCkMC4uUU-mA" name="CE3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_i1vLoS43EeuCkMC4uUU-mA" value="CE3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i1vLoi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i1vLoy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i1vLpC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i1vLpS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i1vLpi43EeuCkMC4uUU-mA" name="CE4" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_i1vLpy43EeuCkMC4uUU-mA" value="CE4"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i1vLqC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i1vLqS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i1vLqi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i1vLqy43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i148oC43EeuCkMC4uUU-mA" name="CE5" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_i148oS43EeuCkMC4uUU-mA" value="CE5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i148oi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i148oy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i148pC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i148pS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i2CtoC43EeuCkMC4uUU-mA" name="CE6" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_i2CtoS43EeuCkMC4uUU-mA" value="CE6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i2Ctoi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i2Ctoy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i2CtpC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i2CtpS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i2GYAC43EeuCkMC4uUU-mA" name="CE7" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_i2GYAS43EeuCkMC4uUU-mA" value="CE7"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i2GYAi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i2GYAy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i2GYBC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i2GYBS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i2Ph8C43EeuCkMC4uUU-mA" name="CE8" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_i2Ph8S43EeuCkMC4uUU-mA" value="CE8"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i2Ph8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i2Ph8y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i2Ph9C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i2Ph9S43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i2Ph9i43EeuCkMC4uUU-mA" name="CE9" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_i2Ph9y43EeuCkMC4uUU-mA" value="CE9"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i2Ph-C43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i2Ph-S43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i2Ph-i43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i2Ph-y43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i2ZS8C43EeuCkMC4uUU-mA" name="CEA" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_i2ZS8S43EeuCkMC4uUU-mA" value="CEA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i2ZS8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i2ZS8y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i2ZS9C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i2ZS9S43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i2jD8C43EeuCkMC4uUU-mA" name="DOS" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_i2jD8S43EeuCkMC4uUU-mA" value="DOS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i2jD8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i2jD8y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i2jD9C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i2jD9S43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i2jD9i43EeuCkMC4uUU-mA" name="REF" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_i2jD9y43EeuCkMC4uUU-mA" value="REF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i2jD-C43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i2jD-S43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i2jD-i43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i2jD-y43EeuCkMC4uUU-mA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i3MkMC43EeuCkMC4uUU-mA" name="SREF1" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_i3MkMS43EeuCkMC4uUU-mA" value="SREF1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i3MkMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i3MkMy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i3MkNC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i3MkNS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i3MkNi43EeuCkMC4uUU-mA" name="SREF2" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_i3MkNy43EeuCkMC4uUU-mA" value="SREF2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i3MkOC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i3MkOS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i3MkOi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i3MkOy43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i3WVMC43EeuCkMC4uUU-mA" name="TIERS" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_i3WVMS43EeuCkMC4uUU-mA" value="TIERS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i3WVMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i3WVMy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i3WVNC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i3WVNS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i3gGMC43EeuCkMC4uUU-mA" name="MARCHE" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_i3gGMS43EeuCkMC4uUU-mA" value="MARCHE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i3gGMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i3gGMy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i3gGNC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i3gGNS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i3pQIC43EeuCkMC4uUU-mA" name="TACOD" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_i3pQIS43EeuCkMC4uUU-mA" value="TACOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i3pQIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i3pQIy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i3pQJC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i3pQJS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i3zBIC43EeuCkMC4uUU-mA" name="VENUN" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_i3zBIS43EeuCkMC4uUU-mA" value="VENUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i3zBIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i3zBIy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i3zBJC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i3zBJS43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i38yIC43EeuCkMC4uUU-mA" name="DEV" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_i38yIS43EeuCkMC4uUU-mA" value="DEV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i38yIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i38yIy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i38yJC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i38yJS43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i4AcgC43EeuCkMC4uUU-mA" name="DEPO" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_i4AcgS43EeuCkMC4uUU-mA" value="DEPO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i4Acgi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i4Acgy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i4AchC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i4AchS43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i4JmcC43EeuCkMC4uUU-mA" name="TADT" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_i4JmcS43EeuCkMC4uUU-mA" value="TADT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i4Jmci43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i4Jmcy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i4JmdC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i4JmdS43EeuCkMC4uUU-mA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i4Jmdi43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i4TXcC43EeuCkMC4uUU-mA" name="QTE" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_i4TXcS43EeuCkMC4uUU-mA" value="QTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i4TXci43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i4TXcy43EeuCkMC4uUU-mA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i4TXdC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i4TXdS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i4TXdi43EeuCkMC4uUU-mA" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i4TXdy43EeuCkMC4uUU-mA" name="CONF" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_i4TXeC43EeuCkMC4uUU-mA" value="CONF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i4TXeS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i4TXei43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i4TXey43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i4TXfC43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i4dvgC43EeuCkMC4uUU-mA" name="USERCR" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_i4dvgS43EeuCkMC4uUU-mA" value="USERCR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i4dvgi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i4dvgy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i4dvhC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i4dvhS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i4nggC43EeuCkMC4uUU-mA" name="USERMO" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_i4nggS43EeuCkMC4uUU-mA" value="USERMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i4nggi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i4nggy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i4nghC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i4nghS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i4nghi43EeuCkMC4uUU-mA" name="USERCRDH" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_i4nghy43EeuCkMC4uUU-mA" value="USERCRDH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i4ngiC43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i4ngiS43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i4ngii43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i4ngiy43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i4ngjC43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i4wqcC43EeuCkMC4uUU-mA" name="USERMODH" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_i4wqcS43EeuCkMC4uUU-mA" value="USERMODH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i4wqci43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i4wqcy43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i4wqdC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i4wqdS43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i4wqdi43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i46bcC43EeuCkMC4uUU-mA" name="CENOTE" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_i46bcS43EeuCkMC4uUU-mA" value="CENOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i46bci43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i46bcy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i46bdC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i46bdS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i46bdi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i49ewC43EeuCkMC4uUU-mA" name="NOTE" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_i49ewS43EeuCkMC4uUU-mA" value="NOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i49ewi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i49ewy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i49exC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i49exS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i49exi43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i5GosC43EeuCkMC4uUU-mA" name="PUB" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_i5GosS43EeuCkMC4uUU-mA" value="PUB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i5Gosi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i5Gosy43EeuCkMC4uUU-mA" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i5GotC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i5GotS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i5Goti43EeuCkMC4uUU-mA" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i5Goty43EeuCkMC4uUU-mA" name="PPAR" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_i5GouC43EeuCkMC4uUU-mA" value="PPAR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i5GouS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i5Goui43EeuCkMC4uUU-mA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i5Gouy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i5GovC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i5GovS43EeuCkMC4uUU-mA" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i5QZsC43EeuCkMC4uUU-mA" name="COE" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_i5QZsS43EeuCkMC4uUU-mA" value="COE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i5QZsi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i5QZsy43EeuCkMC4uUU-mA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i5QZtC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i5QZtS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i5QZti43EeuCkMC4uUU-mA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i5ZjoC43EeuCkMC4uUU-mA" name="PUBUN" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_i5ZjoS43EeuCkMC4uUU-mA" value="PUBUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i5Zjoi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i5Zjoy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i5ZjpC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i5ZjpS43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i5jUoC43EeuCkMC4uUU-mA" name="HTCOD" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_i5jUoS43EeuCkMC4uUU-mA" value="HTCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i5jUoi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i5jUoy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i5jUpC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i5jUpS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i5jUpi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i5jUpy43EeuCkMC4uUU-mA" name="PUBTYP" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_i5jUqC43EeuCkMC4uUU-mA" value="PUBTYP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i5jUqS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i5jUqi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i5jUqy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i5jUrC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i5jUrS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i5tFoC43EeuCkMC4uUU-mA" name="MARQTE" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_i5tFoS43EeuCkMC4uUU-mA" value="MARQTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i5tFoi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i5tFoy43EeuCkMC4uUU-mA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i5tFpC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i5tFpS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i5tFpi43EeuCkMC4uUU-mA" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i57IEC43EeuCkMC4uUU-mA" name="PAFORF" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_i57IES43EeuCkMC4uUU-mA" value="PAFORF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i57IEi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i57IEy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i57IFC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i57IFS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i57IFi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i57IFy43EeuCkMC4uUU-mA" name="HSDT" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_i57IGC43EeuCkMC4uUU-mA" value="HSDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i57IGS43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i57IGi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i57IGy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i57IHC43EeuCkMC4uUU-mA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i57IHS43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i6E5EC43EeuCkMC4uUU-mA" name="ICPFL" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_i6E5ES43EeuCkMC4uUU-mA" value="ICPFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i6E5Ei43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i6E5Ey43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i6E5FC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i6E5FS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i6E5Fi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i6YbEC43EeuCkMC4uUU-mA" name="ETB" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_i6YbES43EeuCkMC4uUU-mA" value="ETB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i6YbEi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i6YbEy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i6YbFC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i6YbFS43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i6hlAC43EeuCkMC4uUU-mA" name="REGLENO" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_i6hlAS43EeuCkMC4uUU-mA" value="REGLENO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i6hlAi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i6hlAy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i6hlBC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i6hlBS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i6hlBi43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_i7B7UC43EeuCkMC4uUU-mA" name="PK__TAR__83E262BF68824C0B">
        <node defType="com.stambia.rdbms.colref" id="_i7B7US43EeuCkMC4uUU-mA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_i7B7Ui43EeuCkMC4uUU-mA" ref="#_i1cQsC43EeuCkMC4uUU-mA?fileId=_N_b74AcLEeu--f7ccO6NIw$type=md$name=TAR_ID?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_jMrE8S43EeuCkMC4uUU-mA" name="MNOTE">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_jMrE8i43EeuCkMC4uUU-mA" value="MNOTE"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_jMrE8y43EeuCkMC4uUU-mA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_jNBqQC43EeuCkMC4uUU-mA" name="MNOTE_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_jNBqQS43EeuCkMC4uUU-mA" value="MNOTE_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jNBqQi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jNBqQy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jNBqRC43EeuCkMC4uUU-mA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jNBqRS43EeuCkMC4uUU-mA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jNBqRi43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jNLbQC43EeuCkMC4uUU-mA" name="APPLIC" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_jNLbQS43EeuCkMC4uUU-mA" value="APPLIC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jNLbQi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jNLbQy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jNLbRC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jNLbRS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jNVMQC43EeuCkMC4uUU-mA" name="NOTE" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_jNVMQS43EeuCkMC4uUU-mA" value="NOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jNVMQi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jNVMQy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jNVMRC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jNVMRS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jNVMRi43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jNVMRy43EeuCkMC4uUU-mA" name="NOTETAILBLOB" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_jNVMSC43EeuCkMC4uUU-mA" value="NOTETAILBLOB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jNVMSS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jNVMSi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jNVMSy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jNVMTC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jNVMTS43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jNeWMC43EeuCkMC4uUU-mA" name="NOTEOBJ" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_jNeWMS43EeuCkMC4uUU-mA" value="NOTEOBJ"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jNeWMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jNeWMy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jNeWNC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jNeWNS43EeuCkMC4uUU-mA" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jNoHMC43EeuCkMC4uUU-mA" name="TEXST" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_jNoHMS43EeuCkMC4uUU-mA" value="TEXST"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jNoHMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jNoHMy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jNoHNC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jNoHNS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jNoHNi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jNoHNy43EeuCkMC4uUU-mA" name="NOTEBLOB" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_jNoHOC43EeuCkMC4uUU-mA" value="NOTEBLOB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jNoHOS43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jNoHOi43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jNoHOy43EeuCkMC4uUU-mA" value="varbinary"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jNoHPC43EeuCkMC4uUU-mA" value="max"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_jOIdgC43EeuCkMC4uUU-mA" name="PK__MNOTE__DB2E7CA91ED04268">
        <node defType="com.stambia.rdbms.colref" id="_jOIdgS43EeuCkMC4uUU-mA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_jOIdgi43EeuCkMC4uUU-mA" ref="#_jNBqQC43EeuCkMC4uUU-mA?fileId=_N_b74AcLEeu--f7ccO6NIw$type=md$name=MNOTE_ID?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_jOlJcS43EeuCkMC4uUU-mA" name="T085">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_jOlJci43EeuCkMC4uUU-mA" value="T085"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_jOlJcy43EeuCkMC4uUU-mA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_jO7uwC43EeuCkMC4uUU-mA" name="T085_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_jO7uwS43EeuCkMC4uUU-mA" value="T085_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jO7uwi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jO7uwy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jO7uxC43EeuCkMC4uUU-mA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jO7uxS43EeuCkMC4uUU-mA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jO7uxi43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jPFfwC43EeuCkMC4uUU-mA" name="CEBIN" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_jPFfwS43EeuCkMC4uUU-mA" value="CEBIN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jPFfwi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jPFfwy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jPFfxC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jPFfxS43EeuCkMC4uUU-mA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jPFfxi43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jPFfxy43EeuCkMC4uUU-mA" name="DOS" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_jPFfyC43EeuCkMC4uUU-mA" value="DOS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jPFfyS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jPFfyi43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jPFfyy43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jPFfzC43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jPPQwC43EeuCkMC4uUU-mA" name="TABNO" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_jPPQwS43EeuCkMC4uUU-mA" value="TABNO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jPPQwi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jPPQwy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jPPQxC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jPPQxS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jPPQxi43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jPYasC43EeuCkMC4uUU-mA" name="TVAART" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_jPYasS43EeuCkMC4uUU-mA" value="TVAART"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jPYasi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jPYasy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jPYatC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jPYatS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jPYati43EeuCkMC4uUU-mA" name="TVATIE" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_jPYaty43EeuCkMC4uUU-mA" value="TVATIE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jPYauC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jPYauS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jPYaui43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jPYauy43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jPiLsC43EeuCkMC4uUU-mA" name="EFFETDT" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_jPiLsS43EeuCkMC4uUU-mA" value="EFFETDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jPiLsi43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jPiLsy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jPiLtC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jPiLtS43EeuCkMC4uUU-mA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jPiLti43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jPr8sC43EeuCkMC4uUU-mA" name="USERCR" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_jPr8sS43EeuCkMC4uUU-mA" value="USERCR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jPr8si43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jPr8sy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jPr8tC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jPr8tS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jPr8ti43EeuCkMC4uUU-mA" name="USERMO" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_jPr8ty43EeuCkMC4uUU-mA" value="USERMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jPr8uC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jPr8uS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jPr8ui43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jPr8uy43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jP4xAC43EeuCkMC4uUU-mA" name="USERCRDH" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_jP4xAS43EeuCkMC4uUU-mA" value="USERCRDH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jP4xAi43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jP4xAy43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jP4xBC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jP4xBS43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jP4xBi43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jP4xBy43EeuCkMC4uUU-mA" name="USERMODH" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_jP4xCC43EeuCkMC4uUU-mA" value="USERMODH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jP4xCS43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jP4xCi43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jP4xCy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jP4xDC43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jP4xDS43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jQCiAC43EeuCkMC4uUU-mA" name="CENOTE" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_jQCiAS43EeuCkMC4uUU-mA" value="CENOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jQCiAi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jQCiAy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jQCiBC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jQCiBS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jQCiBi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jQCiBy43EeuCkMC4uUU-mA" name="NOTE" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_jQCiCC43EeuCkMC4uUU-mA" value="NOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jQCiCS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jQCiCi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jQCiCy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jQCiDC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jQCiDS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jQMTAC43EeuCkMC4uUU-mA" name="TVAP" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_jQMTAS43EeuCkMC4uUU-mA" value="TVAP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jQMTAi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jQMTAy43EeuCkMC4uUU-mA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jQMTBC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jQMTBS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jQMTBi43EeuCkMC4uUU-mA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jQVc8C43EeuCkMC4uUU-mA" name="TVACPT_0001" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_jQVc8S43EeuCkMC4uUU-mA" value="TVACPT_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jQVc8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jQVc8y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jQVc9C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jQVc9S43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jQVc9i43EeuCkMC4uUU-mA" name="TVACPT_0002" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_jQVc9y43EeuCkMC4uUU-mA" value="TVACPT_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jQVc-C43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jQVc-S43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jQVc-i43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jQVc-y43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jQfN8C43EeuCkMC4uUU-mA" name="TVAALP" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_jQfN8S43EeuCkMC4uUU-mA" value="TVAALP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jQfN8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jQfN8y43EeuCkMC4uUU-mA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jQfN9C43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jQfN9S43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jQfN9i43EeuCkMC4uUU-mA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jQo-8C43EeuCkMC4uUU-mA" name="TVAALCPT_0001" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_jQo-8S43EeuCkMC4uUU-mA" value="TVAALCPT_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jQo-8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jQo-8y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jQo-9C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jQo-9S43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jQo-9i43EeuCkMC4uUU-mA" name="TVAALCPT_0002" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_jQo-9y43EeuCkMC4uUU-mA" value="TVAALCPT_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jQo--C43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jQo--S43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jQo--i43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jQo--y43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jQ2aUC43EeuCkMC4uUU-mA" name="TVATYP" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_jQ2aUS43EeuCkMC4uUU-mA" value="TVATYP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jQ2aUi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jQ2aUy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jQ2aVC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jQ2aVS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jQ2aVi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jQ2aVy43EeuCkMC4uUU-mA" name="ESCACCCPT" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_jQ2aWC43EeuCkMC4uUU-mA" value="ESCACCCPT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jQ2aWS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jQ2aWi43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jQ2aWy43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jQ2aXC43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jQ_kQC43EeuCkMC4uUU-mA" name="ESCOBTCPT" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_jQ_kQS43EeuCkMC4uUU-mA" value="ESCOBTCPT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jQ_kQi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jQ_kQy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jQ_kRC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jQ_kRS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jQ_kRi43EeuCkMC4uUU-mA" name="TVAIMMOCPT" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_jQ_kRy43EeuCkMC4uUU-mA" value="TVAIMMOCPT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jQ_kSC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jQ_kSS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jQ_kSi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jQ_kSy43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jRJVQC43EeuCkMC4uUU-mA" name="TVAVENCPT" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_jRJVQS43EeuCkMC4uUU-mA" value="TVAVENCPT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jRJVQi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jRJVQy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jRJVRC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jRJVRS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jRTGQC43EeuCkMC4uUU-mA" name="CHRGNNDEDCPT" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_jRTGQS43EeuCkMC4uUU-mA" value="CHRGNNDEDCPT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jRTGQi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jRTGQy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jRTGRC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jRTGRS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jRTGRi43EeuCkMC4uUU-mA" name="COEADM" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_jRTGRy43EeuCkMC4uUU-mA" value="COEADM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jRTGSC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jRTGSS43EeuCkMC4uUU-mA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jRTGSi43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jRTGSy43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jRTGTC43EeuCkMC4uUU-mA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jRcQMC43EeuCkMC4uUU-mA" name="COEASS" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_jRcQMS43EeuCkMC4uUU-mA" value="COEASS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jRcQMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jRcQMy43EeuCkMC4uUU-mA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jRcQNC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jRcQNS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jRcQNi43EeuCkMC4uUU-mA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jRmBMC43EeuCkMC4uUU-mA" name="COETAX" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_jRmBMS43EeuCkMC4uUU-mA" value="COETAX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jRmBMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jRmBMy43EeuCkMC4uUU-mA" value="5"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jRmBNC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jRmBNS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jRmBNi43EeuCkMC4uUU-mA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jRmBNy43EeuCkMC4uUU-mA" name="LIB" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_jRmBOC43EeuCkMC4uUU-mA" value="LIB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jRmBOS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jRmBOi43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jRmBOy43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jRmBPC43EeuCkMC4uUU-mA" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jRvyMC43EeuCkMC4uUU-mA" name="MODELEAXECPT" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_jRvyMS43EeuCkMC4uUU-mA" value="MODELEAXECPT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jRvyMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jRvyMy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jRvyNC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jRvyNS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_jSGXgC43EeuCkMC4uUU-mA" name="PK__T085__358C9EDD2B187FC1">
        <node defType="com.stambia.rdbms.colref" id="_jSGXgS43EeuCkMC4uUU-mA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_jSGXgi43EeuCkMC4uUU-mA" ref="#_jO7uwC43EeuCkMC4uUU-mA?fileId=_N_b74AcLEeu--f7ccO6NIw$type=md$name=T085_ID?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_jJ9IIS43EeuCkMC4uUU-mA" name="ARTTAX">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_jJ9IIi43EeuCkMC4uUU-mA" value="ARTTAX"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_jJ9IIy43EeuCkMC4uUU-mA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_jKKjgC43EeuCkMC4uUU-mA" name="ARTTAX_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_jKKjgS43EeuCkMC4uUU-mA" value="ARTTAX_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jKKjgi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jKKjgy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jKKjhC43EeuCkMC4uUU-mA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jKKjhS43EeuCkMC4uUU-mA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jKKjhi43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jKUUgC43EeuCkMC4uUU-mA" name="CE1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_jKUUgS43EeuCkMC4uUU-mA" value="CE1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jKUUgi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jKUUgy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jKUUhC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jKUUhS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jKUUhi43EeuCkMC4uUU-mA" name="CE2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_jKUUhy43EeuCkMC4uUU-mA" value="CE2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jKUUiC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jKUUiS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jKUUii43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jKUUiy43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jKdecC43EeuCkMC4uUU-mA" name="CE3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_jKdecS43EeuCkMC4uUU-mA" value="CE3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jKdeci43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jKdecy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jKdedC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jKdedS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jKnPcC43EeuCkMC4uUU-mA" name="CE4" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_jKnPcS43EeuCkMC4uUU-mA" value="CE4"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jKnPci43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jKnPcy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jKnPdC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jKnPdS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jKxAcC43EeuCkMC4uUU-mA" name="CE5" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_jKxAcS43EeuCkMC4uUU-mA" value="CE5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jKxAci43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jKxAcy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jKxAdC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jKxAdS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jKxAdi43EeuCkMC4uUU-mA" name="CE6" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_jKxAdy43EeuCkMC4uUU-mA" value="CE6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jKxAeC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jKxAeS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jKxAei43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jKxAey43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jLD7YC43EeuCkMC4uUU-mA" name="CE7" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_jLD7YS43EeuCkMC4uUU-mA" value="CE7"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jLD7Yi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jLD7Yy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jLD7ZC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jLD7ZS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jLHlwC43EeuCkMC4uUU-mA" name="CE8" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_jLHlwS43EeuCkMC4uUU-mA" value="CE8"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jLHlwi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jLHlwy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jLHlxC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jLHlxS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jLRWwC43EeuCkMC4uUU-mA" name="CE9" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_jLRWwS43EeuCkMC4uUU-mA" value="CE9"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jLRWwi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jLRWwy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jLRWxC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jLRWxS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jLRWxi43EeuCkMC4uUU-mA" name="CEA" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_jLRWxy43EeuCkMC4uUU-mA" value="CEA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jLRWyC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jLRWyS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jLRWyi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jLRWyy43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jLagsC43EeuCkMC4uUU-mA" name="DOS" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_jLagsS43EeuCkMC4uUU-mA" value="DOS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jLagsi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jLagsy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jLagtC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jLagtS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jLkRsC43EeuCkMC4uUU-mA" name="REF" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_jLkRsS43EeuCkMC4uUU-mA" value="REF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jLkRsi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jLkRsy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jLkRtC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jLkRtS43EeuCkMC4uUU-mA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jLkRti43EeuCkMC4uUU-mA" name="TAXCOD" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_jLkRty43EeuCkMC4uUU-mA" value="TAXCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jLkRuC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jLkRuS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jLkRui43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jLkRuy43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jLuCsC43EeuCkMC4uUU-mA" name="ICPFL" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_jLuCsS43EeuCkMC4uUU-mA" value="ICPFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jLuCsi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jLuCsy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jLuCtC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jLuCtS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jLuCti43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jL3MoC43EeuCkMC4uUU-mA" name="USERCR" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_jL3MoS43EeuCkMC4uUU-mA" value="USERCR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jL3Moi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jL3Moy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jL3MpC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jL3MpS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jL3Mpi43EeuCkMC4uUU-mA" name="USERMO" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_jL3Mpy43EeuCkMC4uUU-mA" value="USERMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jL3MqC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jL3MqS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jL3Mqi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jL3Mqy43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jMEoAC43EeuCkMC4uUU-mA" name="USERCRDH" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_jMEoAS43EeuCkMC4uUU-mA" value="USERCRDH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jMEoAi43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jMEoAy43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jMEoBC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jMEoBS43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jMEoBi43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jMEoBy43EeuCkMC4uUU-mA" name="USERMODH" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_jMEoCC43EeuCkMC4uUU-mA" value="USERMODH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jMEoCS43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jMEoCi43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jMEoCy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jMEoDC43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jMEoDS43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_jMhT8C43EeuCkMC4uUU-mA" name="PK__ARTTAX__EDF8EB7F3037C9FA">
        <node defType="com.stambia.rdbms.colref" id="_jMhT8S43EeuCkMC4uUU-mA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_jMhT8i43EeuCkMC4uUU-mA" ref="#_jKKjgC43EeuCkMC4uUU-mA?fileId=_N_b74AcLEeu--f7ccO6NIw$type=md$name=ARTTAX_ID?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_jBNb0S43EeuCkMC4uUU-mA" name="T033">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_jBNb0i43EeuCkMC4uUU-mA" value="T033"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_jBNb0y43EeuCkMC4uUU-mA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_jBgWwC43EeuCkMC4uUU-mA" name="T033_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_jBgWwS43EeuCkMC4uUU-mA" value="T033_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jBgWwi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jBgWwy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jBgWxC43EeuCkMC4uUU-mA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jBgWxS43EeuCkMC4uUU-mA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jBgWxi43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jBkoMC43EeuCkMC4uUU-mA" name="CEBIN" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_jBkoMS43EeuCkMC4uUU-mA" value="CEBIN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jBkoMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jBkoMy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jBkoNC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jBkoNS43EeuCkMC4uUU-mA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jBkoNi43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jBtyIC43EeuCkMC4uUU-mA" name="DOS" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_jBtyIS43EeuCkMC4uUU-mA" value="DOS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jBtyIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jBtyIy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jBtyJC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jBtyJS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jBtyJi43EeuCkMC4uUU-mA" name="TABNO" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_jBtyJy43EeuCkMC4uUU-mA" value="TABNO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jBtyKC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jBtyKS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jBtyKi43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jBtyKy43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jBtyLC43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jB3jIC43EeuCkMC4uUU-mA" name="REF" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_jB3jIS43EeuCkMC4uUU-mA" value="REF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jB3jIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jB3jIy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jB3jJC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jB3jJS43EeuCkMC4uUU-mA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jCBUIC43EeuCkMC4uUU-mA" name="SREF1" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_jCBUIS43EeuCkMC4uUU-mA" value="SREF1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jCBUIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jCBUIy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jCBUJC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jCBUJS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jCBUJi43EeuCkMC4uUU-mA" name="SREF2" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_jCBUJy43EeuCkMC4uUU-mA" value="SREF2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jCBUKC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jCBUKS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jCBUKi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jCBUKy43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jCKeEC43EeuCkMC4uUU-mA" name="VENUN" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_jCKeES43EeuCkMC4uUU-mA" value="VENUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jCKeEi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jCKeEy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jCKeFC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jCKeFS43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jCUPEC43EeuCkMC4uUU-mA" name="EMBQTE" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_jCUPES43EeuCkMC4uUU-mA" value="EMBQTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jCUPEi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jCUPEy43EeuCkMC4uUU-mA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jCUPFC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jCUPFS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jCUPFi43EeuCkMC4uUU-mA" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jCeAEC43EeuCkMC4uUU-mA" name="USERCR" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_jCeAES43EeuCkMC4uUU-mA" value="USERCR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jCeAEi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jCeAEy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jCeAFC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jCeAFS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jChqcC43EeuCkMC4uUU-mA" name="USERMO" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_jChqcS43EeuCkMC4uUU-mA" value="USERMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jChqci43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jChqcy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jChqdC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jChqdS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jCrbcC43EeuCkMC4uUU-mA" name="USERCRDH" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_jCrbcS43EeuCkMC4uUU-mA" value="USERCRDH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jCrbci43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jCrbcy43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jCrbdC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jCrbdS43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jCrbdi43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jCrbdy43EeuCkMC4uUU-mA" name="USERMODH" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_jCrbeC43EeuCkMC4uUU-mA" value="USERMODH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jCrbeS43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jCrbei43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jCrbey43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jCrbfC43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jCrbfS43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jC0lYC43EeuCkMC4uUU-mA" name="CENOTE" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_jC0lYS43EeuCkMC4uUU-mA" value="CENOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jC0lYi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jC0lYy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jC0lZC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jC0lZS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jC0lZi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jC-WYC43EeuCkMC4uUU-mA" name="NOTE" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_jC-WYS43EeuCkMC4uUU-mA" value="NOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jC-WYi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jC-WYy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jC-WZC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jC-WZS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jC-WZi43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jDHgUC43EeuCkMC4uUU-mA" name="EMBUN" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_jDHgUS43EeuCkMC4uUU-mA" value="EMBUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jDHgUi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jDHgUy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jDHgVC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jDHgVS43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jDRRUC43EeuCkMC4uUU-mA" name="POIN" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_jDRRUS43EeuCkMC4uUU-mA" value="POIN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jDRRUi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jDRRUy43EeuCkMC4uUU-mA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jDRRVC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jDRRVS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jDRRVi43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jDbCUC43EeuCkMC4uUU-mA" name="POIUN" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_jDbCUS43EeuCkMC4uUU-mA" value="POIUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jDbCUi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jDbCUy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jDbCVC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jDbCVS43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jDessC43EeuCkMC4uUU-mA" name="PG_QTETYP" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_jDessS43EeuCkMC4uUU-mA" value="PG_QTETYP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jDessi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jDessy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jDestC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jDestS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jDesti43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_jEEikC43EeuCkMC4uUU-mA" name="PK__T033__832A4C2D702CC41D">
        <node defType="com.stambia.rdbms.colref" id="_jEEikS43EeuCkMC4uUU-mA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_jEEiki43EeuCkMC4uUU-mA" ref="#_jBgWwC43EeuCkMC4uUU-mA?fileId=_N_b74AcLEeu--f7ccO6NIw$type=md$name=T033_ID?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_ibffoS43EeuCkMC4uUU-mA" name="CLI">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_ibffoi43EeuCkMC4uUU-mA" value="CLI"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_ibffoy43EeuCkMC4uUU-mA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_idjVIC43EeuCkMC4uUU-mA" name="CLI_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_idjVIS43EeuCkMC4uUU-mA" value="CLI_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_idjVIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_idjVIy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_idjVJC43EeuCkMC4uUU-mA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_idjVJS43EeuCkMC4uUU-mA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_idjVJi43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_id2QEC43EeuCkMC4uUU-mA" name="CE1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_id2QES43EeuCkMC4uUU-mA" value="CE1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_id2QEi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_id2QEy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_id2QFC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_id2QFS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_id2QFi43EeuCkMC4uUU-mA" name="CE2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_id2QFy43EeuCkMC4uUU-mA" value="CE2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_id2QGC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_id2QGS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_id2QGi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_id2QGy43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_id_aAC43EeuCkMC4uUU-mA" name="CE3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_id_aAS43EeuCkMC4uUU-mA" value="CE3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_id_aAi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_id_aAy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_id_aBC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_id_aBS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ieNccC43EeuCkMC4uUU-mA" name="CE4" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_ieNccS43EeuCkMC4uUU-mA" value="CE4"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ieNcci43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ieNccy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ieNcdC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ieNcdS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ieNcdi43EeuCkMC4uUU-mA" name="CE5" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_ieNcdy43EeuCkMC4uUU-mA" value="CE5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ieNceC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ieNceS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ieNcei43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ieNcey43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ieWmYC43EeuCkMC4uUU-mA" name="CE6" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_ieWmYS43EeuCkMC4uUU-mA" value="CE6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ieWmYi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ieWmYy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ieWmZC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ieWmZS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iegXYC43EeuCkMC4uUU-mA" name="CE7" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_iegXYS43EeuCkMC4uUU-mA" value="CE7"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iegXYi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iegXYy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iegXZC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iegXZS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iegXZi43EeuCkMC4uUU-mA" name="CE8" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_iegXZy43EeuCkMC4uUU-mA" value="CE8"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iegXaC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iegXaS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iegXai43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iegXay43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ieqIYC43EeuCkMC4uUU-mA" name="CE9" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_ieqIYS43EeuCkMC4uUU-mA" value="CE9"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ieqIYi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ieqIYy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ieqIZC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ieqIZS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iezSUC43EeuCkMC4uUU-mA" name="CEA" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_iezSUS43EeuCkMC4uUU-mA" value="CEA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iezSUi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iezSUy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iezSVC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iezSVS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iezSVi43EeuCkMC4uUU-mA" name="DOS" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_iezSVy43EeuCkMC4uUU-mA" value="DOS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iezSWC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iezSWS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iezSWi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iezSWy43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ie9DUC43EeuCkMC4uUU-mA" name="TIERS" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_ie9DUS43EeuCkMC4uUU-mA" value="TIERS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ie9DUi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ie9DUy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ie9DVC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ie9DVS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ifG0UC43EeuCkMC4uUU-mA" name="USERCR" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_ifG0US43EeuCkMC4uUU-mA" value="USERCR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ifG0Ui43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ifG0Uy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ifG0VC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ifG0VS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ifKesC43EeuCkMC4uUU-mA" name="USERMO" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_ifKesS43EeuCkMC4uUU-mA" value="USERMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ifKesi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ifKesy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ifKetC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ifKetS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ifTooC43EeuCkMC4uUU-mA" name="CONF" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_ifTooS43EeuCkMC4uUU-mA" value="CONF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ifTooi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ifTooy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ifTopC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ifTopS43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ifTopi43EeuCkMC4uUU-mA" name="VISA" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_ifTopy43EeuCkMC4uUU-mA" value="VISA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ifToqC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ifToqS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ifToqi43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ifToqy43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ifTorC43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ifdZoC43EeuCkMC4uUU-mA" name="NOMABR" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_ifdZoS43EeuCkMC4uUU-mA" value="NOMABR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ifdZoi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ifdZoy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ifdZpC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ifdZpS43EeuCkMC4uUU-mA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ifwUkC43EeuCkMC4uUU-mA" name="NOM" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_ifwUkS43EeuCkMC4uUU-mA" value="NOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ifwUki43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ifwUky43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ifwUlC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ifwUlS43EeuCkMC4uUU-mA" value="80"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_if6FkC43EeuCkMC4uUU-mA" name="ADRCPL1" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_if6FkS43EeuCkMC4uUU-mA" value="ADRCPL1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_if6Fki43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_if6Fky43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_if6FlC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_if6FlS43EeuCkMC4uUU-mA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_if6Fli43EeuCkMC4uUU-mA" name="ADRCPL2" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_if6Fly43EeuCkMC4uUU-mA" value="ADRCPL2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_if6FmC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_if6FmS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_if6Fmi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_if6Fmy43EeuCkMC4uUU-mA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_igHg8C43EeuCkMC4uUU-mA" name="RUE" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_igHg8S43EeuCkMC4uUU-mA" value="RUE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_igHg8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_igHg8y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_igHg9C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_igHg9S43EeuCkMC4uUU-mA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_igRR8C43EeuCkMC4uUU-mA" name="LOC" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_igRR8S43EeuCkMC4uUU-mA" value="LOC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_igRR8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_igRR8y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_igRR9C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_igRR9S43EeuCkMC4uUU-mA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_igbC8C43EeuCkMC4uUU-mA" name="VIL" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_igbC8S43EeuCkMC4uUU-mA" value="VIL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_igbC8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_igbC8y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_igbC9C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_igbC9S43EeuCkMC4uUU-mA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_igdfMC43EeuCkMC4uUU-mA" name="PAY" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_igdfMS43EeuCkMC4uUU-mA" value="PAY"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_igdfMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_igdfMy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_igdfNC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_igdfNS43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ignQMC43EeuCkMC4uUU-mA" name="CPOSTAL" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_ignQMS43EeuCkMC4uUU-mA" value="CPOSTAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ignQMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ignQMy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ignQNC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ignQNS43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_igwaIC43EeuCkMC4uUU-mA" name="ZIPCOD" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_igwaIS43EeuCkMC4uUU-mA" value="ZIPCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_igwaIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_igwaIy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_igwaJC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_igwaJS43EeuCkMC4uUU-mA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_igwaJi43EeuCkMC4uUU-mA" name="REGIONCOD" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_igwaJy43EeuCkMC4uUU-mA" value="REGIONCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_igwaKC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_igwaKS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_igwaKi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_igwaKy43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ig6LIC43EeuCkMC4uUU-mA" name="INSEECOD" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_ig6LIS43EeuCkMC4uUU-mA" value="INSEECOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ig6LIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ig6LIy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ig6LJC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ig6LJS43EeuCkMC4uUU-mA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ihEjMC43EeuCkMC4uUU-mA" name="TEL" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_ihEjMS43EeuCkMC4uUU-mA" value="TEL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ihEjMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ihEjMy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ihEjNC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ihEjNS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ihEjNi43EeuCkMC4uUU-mA" name="FAX" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_ihEjNy43EeuCkMC4uUU-mA" value="FAX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ihEjOC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ihEjOS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ihEjOi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ihEjOy43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ihOUMC43EeuCkMC4uUU-mA" name="WEB" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_ihOUMS43EeuCkMC4uUU-mA" value="WEB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ihOUMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ihOUMy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ihOUNC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ihOUNS43EeuCkMC4uUU-mA" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ihhPIC43EeuCkMC4uUU-mA" name="EMAIL" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_ihhPIS43EeuCkMC4uUU-mA" value="EMAIL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ihhPIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ihhPIy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ihhPJC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ihhPJS43EeuCkMC4uUU-mA" value="80"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ihhPJi43EeuCkMC4uUU-mA" name="NAF" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_ihhPJy43EeuCkMC4uUU-mA" value="NAF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ihhPKC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ihhPKS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ihhPKi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ihhPKy43EeuCkMC4uUU-mA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ihrAIC43EeuCkMC4uUU-mA" name="TIT" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_ihrAIS43EeuCkMC4uUU-mA" value="TIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ihrAIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ihrAIy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ihrAJC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ihrAJS43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ih97EC43EeuCkMC4uUU-mA" name="REGL" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_ih97ES43EeuCkMC4uUU-mA" value="REGL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ih97Ei43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ih97Ey43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ih97FC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ih97FS43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iiCMgC43EeuCkMC4uUU-mA" name="DEV" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_iiCMgS43EeuCkMC4uUU-mA" value="DEV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iiCMgi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iiCMgy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iiCMhC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iiCMhS43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iiLWcC43EeuCkMC4uUU-mA" name="LANG" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_iiLWcS43EeuCkMC4uUU-mA" value="LANG"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iiLWci43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iiLWcy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iiLWdC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iiLWdS43EeuCkMC4uUU-mA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iiLWdi43EeuCkMC4uUU-mA" name="CPT" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_iiLWdy43EeuCkMC4uUU-mA" value="CPT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iiLWeC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iiLWeS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iiLWei43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iiLWey43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iiVHcC43EeuCkMC4uUU-mA" name="CPTMSK" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_iiVHcS43EeuCkMC4uUU-mA" value="CPTMSK"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iiVHci43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iiVHcy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iiVHdC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iiVHdS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iieRYC43EeuCkMC4uUU-mA" name="SELCOD" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_iieRYS43EeuCkMC4uUU-mA" value="SELCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iieRYi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iieRYy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iieRZC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iieRZS43EeuCkMC4uUU-mA" value="80"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iioCYC43EeuCkMC4uUU-mA" name="SIRET" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_iioCYS43EeuCkMC4uUU-mA" value="SIRET"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iioCYi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iioCYy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iioCZC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iioCZS43EeuCkMC4uUU-mA" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iixzYC43EeuCkMC4uUU-mA" name="ETB" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_iixzYS43EeuCkMC4uUU-mA" value="ETB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iixzYi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iixzYy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iixzZC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iixzZS43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ii69UC43EeuCkMC4uUU-mA" name="USERCRDH" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_ii69US43EeuCkMC4uUU-mA" value="USERCRDH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ii69Ui43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ii69Uy43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ii69VC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ii69VS43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ii69Vi43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ii_OwC43EeuCkMC4uUU-mA" name="USERMODH" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_ii_OwS43EeuCkMC4uUU-mA" value="USERMODH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ii_Owi43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ii_Owy43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ii_OxC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ii_OxS43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ii_Oxi43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ii_Oxy43EeuCkMC4uUU-mA" name="HSDT" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_ii_OyC43EeuCkMC4uUU-mA" value="HSDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ii_OyS43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ii_Oyi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ii_Oyy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ii_OzC43EeuCkMC4uUU-mA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ii_OzS43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ijIYsC43EeuCkMC4uUU-mA" name="NOTE" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_ijIYsS43EeuCkMC4uUU-mA" value="NOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ijIYsi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ijIYsy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ijIYtC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ijIYtS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ijIYti43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ijSJsC43EeuCkMC4uUU-mA" name="CENOTE" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_ijSJsS43EeuCkMC4uUU-mA" value="CENOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ijSJsi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ijSJsy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ijSJtC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ijSJtS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ijSJti43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ijSJty43EeuCkMC4uUU-mA" name="JOINT" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_ijSJuC43EeuCkMC4uUU-mA" value="JOINT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ijSJuS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ijSJui43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ijSJuy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ijSJvC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ijSJvS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ijb6sC43EeuCkMC4uUU-mA" name="CEJOINT" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_ijb6sS43EeuCkMC4uUU-mA" value="CEJOINT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ijb6si43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ijb6sy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ijb6tC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ijb6tS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ijb6ti43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ijlrsC43EeuCkMC4uUU-mA" name="IDCONNECT" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_ijlrsS43EeuCkMC4uUU-mA" value="IDCONNECT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ijlrsi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ijlrsy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ijlrtC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ijlrtS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ijlrti43EeuCkMC4uUU-mA" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ijlrty43EeuCkMC4uUU-mA" name="CLDCOD" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_ijlruC43EeuCkMC4uUU-mA" value="CLDCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ijlruS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ijlrui43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ijlruy43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ijlrvC43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iju1oC43EeuCkMC4uUU-mA" name="GLN" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_iju1oS43EeuCkMC4uUU-mA" value="GLN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iju1oi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iju1oy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iju1pC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iju1pS43EeuCkMC4uUU-mA" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ij4moC43EeuCkMC4uUU-mA" name="TELCLE" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_ij4moS43EeuCkMC4uUU-mA" value="TELCLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ij4moi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ij4moy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ij4mpC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ij4mpS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ij8RAC43EeuCkMC4uUU-mA" name="ICPFL" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_ij8RAS43EeuCkMC4uUU-mA" value="ICPFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ij8RAi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ij8RAy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ij8RBC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ij8RBS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ij8RBi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ikGCAC43EeuCkMC4uUU-mA" name="TACOD" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_ikGCAS43EeuCkMC4uUU-mA" value="TACOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ikGCAi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ikGCAy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ikGCBC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ikGCBS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ikGCBi43EeuCkMC4uUU-mA" name="REMCOD" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_ikGCBy43EeuCkMC4uUU-mA" value="REMCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ikGCCC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ikGCCS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ikGCCi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ikGCCy43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ikPL8C43EeuCkMC4uUU-mA" name="PROMOTACOD" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_ikPL8S43EeuCkMC4uUU-mA" value="PROMOTACOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ikPL8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ikPL8y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ikPL9C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ikPL9S43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ikY88C43EeuCkMC4uUU-mA" name="PROMOREMCOD" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_ikY88S43EeuCkMC4uUU-mA" value="PROMOREMCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ikY88i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ikY88y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ikY89C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ikY89S43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ikY89i43EeuCkMC4uUU-mA" name="TAFAM" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_ikY89y43EeuCkMC4uUU-mA" value="TAFAM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ikY8-C43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ikY8-S43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ikY8-i43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ikY8-y43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ikiG4C43EeuCkMC4uUU-mA" name="TAFAMX" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_ikiG4S43EeuCkMC4uUU-mA" value="TAFAMX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ikiG4i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ikiG4y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ikiG5C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ikiG5S43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ikr34C43EeuCkMC4uUU-mA" name="REFAM" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_ikr34S43EeuCkMC4uUU-mA" value="REFAM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ikr34i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ikr34y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ikr35C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ikr35S43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ik1o4C43EeuCkMC4uUU-mA" name="REFAMX" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_ik1o4S43EeuCkMC4uUU-mA" value="REFAMX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ik1o4i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ik1o4y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ik1o5C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ik1o5S43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ik5TQC43EeuCkMC4uUU-mA" name="REM_0001" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_ik5TQS43EeuCkMC4uUU-mA" value="REM_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ik5TQi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ik5TQy43EeuCkMC4uUU-mA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ik5TRC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ik5TRS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ik5TRi43EeuCkMC4uUU-mA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ilDEQC43EeuCkMC4uUU-mA" name="REM_0002" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_ilDEQS43EeuCkMC4uUU-mA" value="REM_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ilDEQi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ilDEQy43EeuCkMC4uUU-mA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ilDERC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ilDERS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ilDERi43EeuCkMC4uUU-mA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ilDERy43EeuCkMC4uUU-mA" name="REM_0003" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_ilDESC43EeuCkMC4uUU-mA" value="REM_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ilDESS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ilDESi43EeuCkMC4uUU-mA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ilDESy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ilDETC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ilDETS43EeuCkMC4uUU-mA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ilM1QC43EeuCkMC4uUU-mA" name="REMTYP_0001" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_ilM1QS43EeuCkMC4uUU-mA" value="REMTYP_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ilM1Qi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ilM1Qy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ilM1RC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ilM1RS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ilM1Ri43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ilV_MC43EeuCkMC4uUU-mA" name="REMTYP_0002" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_ilV_MS43EeuCkMC4uUU-mA" value="REMTYP_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ilV_Mi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ilV_My43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ilV_NC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ilV_NS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ilV_Ni43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ilV_Ny43EeuCkMC4uUU-mA" name="REMTYP_0003" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_ilV_OC43EeuCkMC4uUU-mA" value="REMTYP_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ilV_OS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ilV_Oi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ilV_Oy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ilV_PC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ilV_PS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ilfwMC43EeuCkMC4uUU-mA" name="DOPDH" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_ilfwMS43EeuCkMC4uUU-mA" value="DOPDH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ilfwMi43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ilfwMy43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ilfwNC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ilfwNS43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ilfwNi43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ilo6IC43EeuCkMC4uUU-mA" name="NBEX_0001" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_ilo6IS43EeuCkMC4uUU-mA" value="NBEX_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ilo6Ii43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ilo6Iy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ilo6JC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ilo6JS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ilo6Ji43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ilo6Jy43EeuCkMC4uUU-mA" name="NBEX_0002" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_ilo6KC43EeuCkMC4uUU-mA" value="NBEX_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ilo6KS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ilo6Ki43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ilo6Ky43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ilo6LC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ilo6LS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_il2VgC43EeuCkMC4uUU-mA" name="NBEX_0003" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_il2VgS43EeuCkMC4uUU-mA" value="NBEX_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_il2Vgi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_il2Vgy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_il2VhC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_il2VhS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_il2Vhi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_imAGgC43EeuCkMC4uUU-mA" name="NBEX_0004" position="74">
        <attribute defType="com.stambia.rdbms.column.name" id="_imAGgS43EeuCkMC4uUU-mA" value="NBEX_0004"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_imAGgi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_imAGgy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_imAGhC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_imAGhS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_imAGhi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_imAGhy43EeuCkMC4uUU-mA" name="EDITTYP_0001" position="75">
        <attribute defType="com.stambia.rdbms.column.name" id="_imAGiC43EeuCkMC4uUU-mA" value="EDITTYP_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_imAGiS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_imAGii43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_imAGiy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_imAGjC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_imAGjS43EeuCkMC4uUU-mA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_imTBcC43EeuCkMC4uUU-mA" name="EDITTYP_0002" position="76">
        <attribute defType="com.stambia.rdbms.column.name" id="_imTBcS43EeuCkMC4uUU-mA" value="EDITTYP_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_imTBci43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_imTBcy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_imTBdC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_imTBdS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_imTBdi43EeuCkMC4uUU-mA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_imcycC43EeuCkMC4uUU-mA" name="EDITTYP_0003" position="77">
        <attribute defType="com.stambia.rdbms.column.name" id="_imcycS43EeuCkMC4uUU-mA" value="EDITTYP_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_imcyci43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_imcycy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_imcydC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_imcydS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_imcydi43EeuCkMC4uUU-mA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_immjcC43EeuCkMC4uUU-mA" name="EDITTYP_0004" position="78">
        <attribute defType="com.stambia.rdbms.column.name" id="_immjcS43EeuCkMC4uUU-mA" value="EDITTYP_0004"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_immjci43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_immjcy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_immjdC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_immjdS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_immjdi43EeuCkMC4uUU-mA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_imvtYC43EeuCkMC4uUU-mA" name="ETANO_0001" position="79">
        <attribute defType="com.stambia.rdbms.column.name" id="_imvtYS43EeuCkMC4uUU-mA" value="ETANO_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_imvtYi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_imvtYy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_imvtZC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_imvtZS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_imz-0C43EeuCkMC4uUU-mA" name="ETANO_0002" position="80">
        <attribute defType="com.stambia.rdbms.column.name" id="_imz-0S43EeuCkMC4uUU-mA" value="ETANO_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_imz-0i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_imz-0y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_imz-1C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_imz-1S43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_imz-1i43EeuCkMC4uUU-mA" name="ETANO_0003" position="81">
        <attribute defType="com.stambia.rdbms.column.name" id="_imz-1y43EeuCkMC4uUU-mA" value="ETANO_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_imz-2C43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_imz-2S43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_imz-2i43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_imz-2y43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_im9IwC43EeuCkMC4uUU-mA" name="ETANO_0004" position="82">
        <attribute defType="com.stambia.rdbms.column.name" id="_im9IwS43EeuCkMC4uUU-mA" value="ETANO_0004"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_im9Iwi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_im9Iwy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_im9IxC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_im9IxS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_inG5wC43EeuCkMC4uUU-mA" name="AXEMSK" position="83">
        <attribute defType="com.stambia.rdbms.column.name" id="_inG5wS43EeuCkMC4uUU-mA" value="AXEMSK"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_inG5wi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_inG5wy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_inG5xC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_inG5xS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_inQqwC43EeuCkMC4uUU-mA" name="AXENO" position="84">
        <attribute defType="com.stambia.rdbms.column.name" id="_inQqwS43EeuCkMC4uUU-mA" value="AXENO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_inQqwi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_inQqwy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_inQqxC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_inQqxS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_inQqxi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_inQqxy43EeuCkMC4uUU-mA" name="STLGTGAMCOD" position="85">
        <attribute defType="com.stambia.rdbms.column.name" id="_inQqyC43EeuCkMC4uUU-mA" value="STLGTGAMCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_inQqyS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_inQqyi43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_inQqyy43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_inQqzC43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_inZ0sC43EeuCkMC4uUU-mA" name="WMAUDITFL" position="86">
        <attribute defType="com.stambia.rdbms.column.name" id="_inZ0sS43EeuCkMC4uUU-mA" value="WMAUDITFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_inZ0si43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_inZ0sy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_inZ0tC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_inZ0tS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_inZ0ti43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_injlsC43EeuCkMC4uUU-mA" name="WMDOCEMP" position="87">
        <attribute defType="com.stambia.rdbms.column.name" id="_injlsS43EeuCkMC4uUU-mA" value="WMDOCEMP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_injlsi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_injlsy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_injltC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_injltS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_injlti43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_injlty43EeuCkMC4uUU-mA" name="WMRESAIMPFL" position="88">
        <attribute defType="com.stambia.rdbms.column.name" id="_injluC43EeuCkMC4uUU-mA" value="WMRESAIMPFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_injluS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_injlui43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_injluy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_injlvC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_injlvS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_inxBEC43EeuCkMC4uUU-mA" name="TRANSPLANCOD" position="89">
        <attribute defType="com.stambia.rdbms.column.name" id="_inxBES43EeuCkMC4uUU-mA" value="TRANSPLANCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_inxBEi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_inxBEy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_inxBFC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_inxBFS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_inxBFi43EeuCkMC4uUU-mA" name="TEXCOD_0001" position="90">
        <attribute defType="com.stambia.rdbms.column.name" id="_inxBFy43EeuCkMC4uUU-mA" value="TEXCOD_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_inxBGC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_inxBGS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_inxBGi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_inxBGy43EeuCkMC4uUU-mA" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_in6yEC43EeuCkMC4uUU-mA" name="TEXCOD_0002" position="91">
        <attribute defType="com.stambia.rdbms.column.name" id="_in6yES43EeuCkMC4uUU-mA" value="TEXCOD_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_in6yEi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_in6yEy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_in6yFC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_in6yFS43EeuCkMC4uUU-mA" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ioEjEC43EeuCkMC4uUU-mA" name="TEXCOD_0003" position="92">
        <attribute defType="com.stambia.rdbms.column.name" id="_ioEjES43EeuCkMC4uUU-mA" value="TEXCOD_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ioEjEi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ioEjEy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ioEjFC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ioEjFS43EeuCkMC4uUU-mA" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ioEjFi43EeuCkMC4uUU-mA" name="TEXCOD_0004" position="93">
        <attribute defType="com.stambia.rdbms.column.name" id="_ioEjFy43EeuCkMC4uUU-mA" value="TEXCOD_0004"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ioEjGC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ioEjGS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ioEjGi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ioEjGy43EeuCkMC4uUU-mA" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ioNtAC43EeuCkMC4uUU-mA" name="TEXCOD_0005" position="94">
        <attribute defType="com.stambia.rdbms.column.name" id="_ioNtAS43EeuCkMC4uUU-mA" value="TEXCOD_0005"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ioNtAi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ioNtAy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ioNtBC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ioNtBS43EeuCkMC4uUU-mA" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ioXeAC43EeuCkMC4uUU-mA" name="TEXCOD_0006" position="95">
        <attribute defType="com.stambia.rdbms.column.name" id="_ioXeAS43EeuCkMC4uUU-mA" value="TEXCOD_0006"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ioXeAi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ioXeAy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ioXeBC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ioXeBS43EeuCkMC4uUU-mA" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ioXeBi43EeuCkMC4uUU-mA" name="TEXCOD_0007" position="96">
        <attribute defType="com.stambia.rdbms.column.name" id="_ioXeBy43EeuCkMC4uUU-mA" value="TEXCOD_0007"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ioXeCC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ioXeCS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ioXeCi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ioXeCy43EeuCkMC4uUU-mA" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iogn8C43EeuCkMC4uUU-mA" name="TEXCOD_0008" position="97">
        <attribute defType="com.stambia.rdbms.column.name" id="_iogn8S43EeuCkMC4uUU-mA" value="TEXCOD_0008"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iogn8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iogn8y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iogn9C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iogn9S43EeuCkMC4uUU-mA" value="11"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ioqY8C43EeuCkMC4uUU-mA" name="AFFCLDCOD" position="98">
        <attribute defType="com.stambia.rdbms.column.name" id="_ioqY8S43EeuCkMC4uUU-mA" value="AFFCLDCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ioqY8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ioqY8y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ioqY9C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ioqY9S43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iouDUC43EeuCkMC4uUU-mA" name="IDENTITEEXT" position="99">
        <attribute defType="com.stambia.rdbms.column.name" id="_iouDUS43EeuCkMC4uUU-mA" value="IDENTITEEXT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iouDUi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iouDUy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iouDVC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iouDVS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iouDVi43EeuCkMC4uUU-mA" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_io30UC43EeuCkMC4uUU-mA" name="STAT_0001" position="100">
        <attribute defType="com.stambia.rdbms.column.name" id="_io30US43EeuCkMC4uUU-mA" value="STAT_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_io30Ui43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_io30Uy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_io30VC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_io30VS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_io30Vi43EeuCkMC4uUU-mA" name="STAT_0002" position="101">
        <attribute defType="com.stambia.rdbms.column.name" id="_io30Vy43EeuCkMC4uUU-mA" value="STAT_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_io30WC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_io30WS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_io30Wi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_io30Wy43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ipA-QC43EeuCkMC4uUU-mA" name="STAT_0003" position="102">
        <attribute defType="com.stambia.rdbms.column.name" id="_ipA-QS43EeuCkMC4uUU-mA" value="STAT_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ipA-Qi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ipA-Qy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ipA-RC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ipA-RS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ipKvQC43EeuCkMC4uUU-mA" name="TPFT" position="103">
        <attribute defType="com.stambia.rdbms.column.name" id="_ipKvQS43EeuCkMC4uUU-mA" value="TPFT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ipKvQi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ipKvQy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ipKvRC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ipKvRS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ipKvRi43EeuCkMC4uUU-mA" name="TIERSGRP" position="104">
        <attribute defType="com.stambia.rdbms.column.name" id="_ipKvRy43EeuCkMC4uUU-mA" value="TIERSGRP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ipKvSC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ipKvSS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ipKvSi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ipKvSy43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ipUgQC43EeuCkMC4uUU-mA" name="ZONA" position="105">
        <attribute defType="com.stambia.rdbms.column.name" id="_ipUgQS43EeuCkMC4uUU-mA" value="ZONA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ipUgQi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ipUgQy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ipUgRC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ipUgRS43EeuCkMC4uUU-mA" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ipdqMC43EeuCkMC4uUU-mA" name="CAMT" position="106">
        <attribute defType="com.stambia.rdbms.column.name" id="_ipdqMS43EeuCkMC4uUU-mA" value="CAMT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ipdqMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ipdqMy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ipdqNC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ipdqNS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ipdqNi43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ipdqNy43EeuCkMC4uUU-mA" name="SALNB" position="107">
        <attribute defType="com.stambia.rdbms.column.name" id="_ipdqOC43EeuCkMC4uUU-mA" value="SALNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ipdqOS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ipdqOi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ipdqOy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ipdqPC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ipdqPS43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ipnbMC43EeuCkMC4uUU-mA" name="ENMAX_0001" position="108">
        <attribute defType="com.stambia.rdbms.column.name" id="_ipnbMS43EeuCkMC4uUU-mA" value="ENMAX_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ipnbMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ipnbMy43EeuCkMC4uUU-mA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ipnbNC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ipnbNS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ipnbNi43EeuCkMC4uUU-mA" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iprFkC43EeuCkMC4uUU-mA" name="ENMAX_0002" position="109">
        <attribute defType="com.stambia.rdbms.column.name" id="_iprFkS43EeuCkMC4uUU-mA" value="ENMAX_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iprFki43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iprFky43EeuCkMC4uUU-mA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iprFlC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iprFlS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iprFli43EeuCkMC4uUU-mA" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ip02kC43EeuCkMC4uUU-mA" name="ESCP" position="110">
        <attribute defType="com.stambia.rdbms.column.name" id="_ip02kS43EeuCkMC4uUU-mA" value="ESCP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ip02ki43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ip02ky43EeuCkMC4uUU-mA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ip02lC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ip02lS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ip02li43EeuCkMC4uUU-mA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ip-AgC43EeuCkMC4uUU-mA" name="ACP" position="111">
        <attribute defType="com.stambia.rdbms.column.name" id="_ip-AgS43EeuCkMC4uUU-mA" value="ACP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ip-Agi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ip-Agy43EeuCkMC4uUU-mA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ip-AhC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ip-AhS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ip-Ahi43EeuCkMC4uUU-mA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ip-Ahy43EeuCkMC4uUU-mA" name="ZONN" position="112">
        <attribute defType="com.stambia.rdbms.column.name" id="_ip-AiC43EeuCkMC4uUU-mA" value="ZONN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ip-AiS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ip-Aii43EeuCkMC4uUU-mA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ip-Aiy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ip-AjC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ip-AjS43EeuCkMC4uUU-mA" value="16"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iqHxgC43EeuCkMC4uUU-mA" name="TVATIE" position="113">
        <attribute defType="com.stambia.rdbms.column.name" id="_iqHxgS43EeuCkMC4uUU-mA" value="TVATIE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iqHxgi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iqHxgy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iqHxhC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iqHxhS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iqRigC43EeuCkMC4uUU-mA" name="TARCOD" position="114">
        <attribute defType="com.stambia.rdbms.column.name" id="_iqRigS43EeuCkMC4uUU-mA" value="TARCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iqRigi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iqRigy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iqRihC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iqRihS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iqRihi43EeuCkMC4uUU-mA" name="COFAM" position="115">
        <attribute defType="com.stambia.rdbms.column.name" id="_iqRihy43EeuCkMC4uUU-mA" value="COFAM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iqRiiC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iqRiiS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iqRiii43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iqRiiy43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iqascC43EeuCkMC4uUU-mA" name="TIERSNAT" position="116">
        <attribute defType="com.stambia.rdbms.column.name" id="_iqascS43EeuCkMC4uUU-mA" value="TIERSNAT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iqasci43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iqascy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iqasdC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iqasdS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iqasdi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iqkdcC43EeuCkMC4uUU-mA" name="ORIGCOD" position="117">
        <attribute defType="com.stambia.rdbms.column.name" id="_iqkdcS43EeuCkMC4uUU-mA" value="ORIGCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iqkdci43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iqkdcy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iqkddC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iqkddS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iqoH0C43EeuCkMC4uUU-mA" name="REPR_0001" position="118">
        <attribute defType="com.stambia.rdbms.column.name" id="_iqoH0S43EeuCkMC4uUU-mA" value="REPR_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iqoH0i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iqoH0y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iqoH1C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iqoH1S43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iqx40C43EeuCkMC4uUU-mA" name="REPR_0002" position="119">
        <attribute defType="com.stambia.rdbms.column.name" id="_iqx40S43EeuCkMC4uUU-mA" value="REPR_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iqx40i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iqx40y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iqx41C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iqx41S43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iqx41i43EeuCkMC4uUU-mA" name="REPR_0003" position="120">
        <attribute defType="com.stambia.rdbms.column.name" id="_iqx41y43EeuCkMC4uUU-mA" value="REPR_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iqx42C43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iqx42S43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iqx42i43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iqx42y43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iq7CwC43EeuCkMC4uUU-mA" name="ADRTIERS_0001" position="121">
        <attribute defType="com.stambia.rdbms.column.name" id="_iq7CwS43EeuCkMC4uUU-mA" value="ADRTIERS_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iq7Cwi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iq7Cwy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iq7CxC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iq7CxS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_irEzwC43EeuCkMC4uUU-mA" name="ADRTIERS_0002" position="122">
        <attribute defType="com.stambia.rdbms.column.name" id="_irEzwS43EeuCkMC4uUU-mA" value="ADRTIERS_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_irEzwi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_irEzwy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_irEzxC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_irEzxS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_irEzxi43EeuCkMC4uUU-mA" name="ADRTIERS_0003" position="123">
        <attribute defType="com.stambia.rdbms.column.name" id="_irEzxy43EeuCkMC4uUU-mA" value="ADRTIERS_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_irEzyC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_irEzyS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_irEzyi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_irEzyy43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_irN9sC43EeuCkMC4uUU-mA" name="ADRTIERS_0004" position="124">
        <attribute defType="com.stambia.rdbms.column.name" id="_irN9sS43EeuCkMC4uUU-mA" value="ADRTIERS_0004"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_irN9si43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_irN9sy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_irN9tC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_irN9tS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_irXusC43EeuCkMC4uUU-mA" name="ADRTIERS_0005" position="125">
        <attribute defType="com.stambia.rdbms.column.name" id="_irXusS43EeuCkMC4uUU-mA" value="ADRTIERS_0005"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_irXusi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_irXusy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_irXutC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_irXutS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_irXuti43EeuCkMC4uUU-mA" name="ADRCOD_0001" position="126">
        <attribute defType="com.stambia.rdbms.column.name" id="_irXuty43EeuCkMC4uUU-mA" value="ADRCOD_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_irXuuC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_irXuuS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_irXuui43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_irXuuy43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_irlKEC43EeuCkMC4uUU-mA" name="ADRCOD_0002" position="127">
        <attribute defType="com.stambia.rdbms.column.name" id="_irlKES43EeuCkMC4uUU-mA" value="ADRCOD_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_irlKEi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_irlKEy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_irlKFC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_irlKFS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_irlKFi43EeuCkMC4uUU-mA" name="ADRCOD_0003" position="128">
        <attribute defType="com.stambia.rdbms.column.name" id="_irlKFy43EeuCkMC4uUU-mA" value="ADRCOD_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_irlKGC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_irlKGS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_irlKGi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_irlKGy43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iru7EC43EeuCkMC4uUU-mA" name="ADRCOD_0004" position="129">
        <attribute defType="com.stambia.rdbms.column.name" id="_iru7ES43EeuCkMC4uUU-mA" value="ADRCOD_0004"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iru7Ei43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iru7Ey43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iru7FC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iru7FS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ir4sEC43EeuCkMC4uUU-mA" name="ADRCOD_0005" position="130">
        <attribute defType="com.stambia.rdbms.column.name" id="_ir4sES43EeuCkMC4uUU-mA" value="ADRCOD_0005"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ir4sEi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ir4sEy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ir4sFC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ir4sFS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_isB2AC43EeuCkMC4uUU-mA" name="TIERSSTAT" position="131">
        <attribute defType="com.stambia.rdbms.column.name" id="_isB2AS43EeuCkMC4uUU-mA" value="TIERSSTAT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_isB2Ai43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_isB2Ay43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_isB2BC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_isB2BS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_isLnAC43EeuCkMC4uUU-mA" name="TIERSR3" position="132">
        <attribute defType="com.stambia.rdbms.column.name" id="_isLnAS43EeuCkMC4uUU-mA" value="TIERSR3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_isLnAi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_isLnAy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_isLnBC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_isLnBS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_isUw8C43EeuCkMC4uUU-mA" name="RELCOD_0001" position="133">
        <attribute defType="com.stambia.rdbms.column.name" id="_isUw8S43EeuCkMC4uUU-mA" value="RELCOD_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_isUw8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_isUw8y43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_isUw9C43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_isUw9S43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_isUw9i43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_isUw9y43EeuCkMC4uUU-mA" name="RELCOD_0002" position="134">
        <attribute defType="com.stambia.rdbms.column.name" id="_isUw-C43EeuCkMC4uUU-mA" value="RELCOD_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_isUw-S43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_isUw-i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_isUw-y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_isUw_C43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_isUw_S43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_isiMUC43EeuCkMC4uUU-mA" name="RELCOD_0003" position="135">
        <attribute defType="com.stambia.rdbms.column.name" id="_isiMUS43EeuCkMC4uUU-mA" value="RELCOD_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_isiMUi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_isiMUy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_isiMVC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_isiMVS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_isiMVi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_isr9UC43EeuCkMC4uUU-mA" name="BLMOD" position="136">
        <attribute defType="com.stambia.rdbms.column.name" id="_isr9US43EeuCkMC4uUU-mA" value="BLMOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_isr9Ui43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_isr9Uy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_isr9VC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_isr9VS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_isr9Vi43EeuCkMC4uUU-mA" name="TVANO" position="137">
        <attribute defType="com.stambia.rdbms.column.name" id="_isr9Vy43EeuCkMC4uUU-mA" value="TVANO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_isr9WC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_isr9WS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_isr9Wi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_isr9Wy43EeuCkMC4uUU-mA" value="14"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_is-4QC43EeuCkMC4uUU-mA" name="TVAPAY" position="138">
        <attribute defType="com.stambia.rdbms.column.name" id="_is-4QS43EeuCkMC4uUU-mA" value="TVAPAY"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_is-4Qi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_is-4Qy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_is-4RC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_is-4RS43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_itIpQC43EeuCkMC4uUU-mA" name="TVABLCOE" position="139">
        <attribute defType="com.stambia.rdbms.column.name" id="_itIpQS43EeuCkMC4uUU-mA" value="TVABLCOE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_itIpQi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_itIpQy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_itIpRC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_itIpRS43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_itSaQC43EeuCkMC4uUU-mA" name="TVABLCD3" position="140">
        <attribute defType="com.stambia.rdbms.column.name" id="_itSaQS43EeuCkMC4uUU-mA" value="TVABLCD3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_itSaQi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_itSaQy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_itSaRC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_itSaRS43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_itSaRi43EeuCkMC4uUU-mA" name="TVAMAXMT" position="141">
        <attribute defType="com.stambia.rdbms.column.name" id="_itSaRy43EeuCkMC4uUU-mA" value="TVAMAXMT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_itSaSC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_itSaSS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_itSaSi43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_itSaSy43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_itSaTC43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_itf1oC43EeuCkMC4uUU-mA" name="TRANSJRNB" position="142">
        <attribute defType="com.stambia.rdbms.column.name" id="_itf1oS43EeuCkMC4uUU-mA" value="TRANSJRNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_itf1oi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_itf1oy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_itf1pC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_itf1pS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_itf1pi43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_itf1py43EeuCkMC4uUU-mA" name="RFCCTRCOD" position="143">
        <attribute defType="com.stambia.rdbms.column.name" id="_itf1qC43EeuCkMC4uUU-mA" value="RFCCTRCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_itf1qS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_itf1qi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_itf1qy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_itf1rC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_itf1rS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_itpmoC43EeuCkMC4uUU-mA" name="PROTOCOL" position="144">
        <attribute defType="com.stambia.rdbms.column.name" id="_itpmoS43EeuCkMC4uUU-mA" value="PROTOCOL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_itpmoi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_itpmoy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_itpmpC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_itpmpS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_itywkC43EeuCkMC4uUU-mA" name="TIERSEXTERNE" position="145">
        <attribute defType="com.stambia.rdbms.column.name" id="_itywkS43EeuCkMC4uUU-mA" value="TIERSEXTERNE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_itywki43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_itywky43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_itywlC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_itywlS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_it8hkC43EeuCkMC4uUU-mA" name="FEU" position="146">
        <attribute defType="com.stambia.rdbms.column.name" id="_it8hkS43EeuCkMC4uUU-mA" value="FEU"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_it8hki43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_it8hky43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_it8hlC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_it8hlS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_it8hli43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_it8hly43EeuCkMC4uUU-mA" name="RELLIGCOD" position="147">
        <attribute defType="com.stambia.rdbms.column.name" id="_it8hmC43EeuCkMC4uUU-mA" value="RELLIGCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_it8hmS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_it8hmi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_it8hmy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_it8hnC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_it8hnS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iuFrgC43EeuCkMC4uUU-mA" name="VALLIGCOD" position="148">
        <attribute defType="com.stambia.rdbms.column.name" id="_iuFrgS43EeuCkMC4uUU-mA" value="VALLIGCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iuFrgi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iuFrgy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iuFrhC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iuFrhS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iuFrhi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iuPcgC43EeuCkMC4uUU-mA" name="DEMATCOD" position="149">
        <attribute defType="com.stambia.rdbms.column.name" id="_iuPcgS43EeuCkMC4uUU-mA" value="DEMATCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iuPcgi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iuPcgy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iuPchC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iuPchS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iuPchi43EeuCkMC4uUU-mA" name="ZONAPC" position="150">
        <attribute defType="com.stambia.rdbms.column.name" id="_iuPchy43EeuCkMC4uUU-mA" value="ZONAPC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iuPciC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iuPciS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iuPcii43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iuPciy43EeuCkMC4uUU-mA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iuc34C43EeuCkMC4uUU-mA" name="RELFAM" position="151">
        <attribute defType="com.stambia.rdbms.column.name" id="_iuc34S43EeuCkMC4uUU-mA" value="RELFAM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iuc34i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iuc34y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iuc35C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iuc35S43EeuCkMC4uUU-mA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iumo4C43EeuCkMC4uUU-mA" name="RELTIETYP" position="152">
        <attribute defType="com.stambia.rdbms.column.name" id="_iumo4S43EeuCkMC4uUU-mA" value="RELTIETYP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iumo4i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iumo4y43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iumo5C43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iumo5S43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iumo5i43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iumo5y43EeuCkMC4uUU-mA" name="FAMOD" position="153">
        <attribute defType="com.stambia.rdbms.column.name" id="_iumo6C43EeuCkMC4uUU-mA" value="FAMOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iumo6S43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iumo6i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iumo6y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iumo7C43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iumo7S43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iuwZ4C43EeuCkMC4uUU-mA" name="PERIOD" position="154">
        <attribute defType="com.stambia.rdbms.column.name" id="_iuwZ4S43EeuCkMC4uUU-mA" value="PERIOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iuwZ4i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iuwZ4y43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iuwZ5C43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iuwZ5S43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iuwZ5i43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iu5j0C43EeuCkMC4uUU-mA" name="TOUR" position="155">
        <attribute defType="com.stambia.rdbms.column.name" id="_iu5j0S43EeuCkMC4uUU-mA" value="TOUR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iu5j0i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iu5j0y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iu5j1C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iu5j1S43EeuCkMC4uUU-mA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iu5j1i43EeuCkMC4uUU-mA" name="TOURRG" position="156">
        <attribute defType="com.stambia.rdbms.column.name" id="_iu5j1y43EeuCkMC4uUU-mA" value="TOURRG"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iu5j2C43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iu5j2S43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iu5j2i43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iu5j2y43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iu5j3C43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ivDU0C43EeuCkMC4uUU-mA" name="BLJR" position="157">
        <attribute defType="com.stambia.rdbms.column.name" id="_ivDU0S43EeuCkMC4uUU-mA" value="BLJR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ivDU0i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ivDU0y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ivDU1C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ivDU1S43EeuCkMC4uUU-mA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ivNF0C43EeuCkMC4uUU-mA" name="ADVCOD" position="158">
        <attribute defType="com.stambia.rdbms.column.name" id="_ivNF0S43EeuCkMC4uUU-mA" value="ADVCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ivNF0i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ivNF0y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ivNF1C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ivNF1S43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ivNF1i43EeuCkMC4uUU-mA" name="PRIOCOD" position="159">
        <attribute defType="com.stambia.rdbms.column.name" id="_ivNF1y43EeuCkMC4uUU-mA" value="PRIOCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ivNF2C43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ivNF2S43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ivNF2i43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ivNF2y43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ivNF3C43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ivahMC43EeuCkMC4uUU-mA" name="WEBCDECOD" position="160">
        <attribute defType="com.stambia.rdbms.column.name" id="_ivahMS43EeuCkMC4uUU-mA" value="WEBCDECOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ivahMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ivahMy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ivahNC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ivahNS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ivahNi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ivahNy43EeuCkMC4uUU-mA" name="BPBASCOD" position="161">
        <attribute defType="com.stambia.rdbms.column.name" id="_ivahOC43EeuCkMC4uUU-mA" value="BPBASCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ivahOS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ivahOi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ivahOy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ivahPC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ivahPS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ivjrIC43EeuCkMC4uUU-mA" name="BPRUPTCOD" position="162">
        <attribute defType="com.stambia.rdbms.column.name" id="_ivjrIS43EeuCkMC4uUU-mA" value="BPRUPTCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ivjrIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ivjrIy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ivjrJC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ivjrJS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ivjrJi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ivtcIC43EeuCkMC4uUU-mA" name="BLGENCOD" position="163">
        <attribute defType="com.stambia.rdbms.column.name" id="_ivtcIS43EeuCkMC4uUU-mA" value="BLGENCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ivtcIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ivtcIy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ivtcJC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ivtcJS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ivtcJi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iv3NIC43EeuCkMC4uUU-mA" name="BPRELCOD" position="164">
        <attribute defType="com.stambia.rdbms.column.name" id="_iv3NIS43EeuCkMC4uUU-mA" value="BPRELCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iv3NIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iv3NIy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iv3NJC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iv3NJS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iv3NJi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iv3NJy43EeuCkMC4uUU-mA" name="PORFRVAL" position="165">
        <attribute defType="com.stambia.rdbms.column.name" id="_iv3NKC43EeuCkMC4uUU-mA" value="PORFRVAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iv3NKS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iv3NKi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iv3NKy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iv3NLC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iv3NLS43EeuCkMC4uUU-mA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iwAXEC43EeuCkMC4uUU-mA" name="PORFRCOD" position="166">
        <attribute defType="com.stambia.rdbms.column.name" id="_iwAXES43EeuCkMC4uUU-mA" value="PORFRCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iwAXEi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iwAXEy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iwAXFC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iwAXFS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iwAXFi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iwKIEC43EeuCkMC4uUU-mA" name="COEFFPOINT" position="167">
        <attribute defType="com.stambia.rdbms.column.name" id="_iwKIES43EeuCkMC4uUU-mA" value="COEFFPOINT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iwKIEi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iwKIEy43EeuCkMC4uUU-mA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iwKIFC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iwKIFS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iwKIFi43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iwKIFy43EeuCkMC4uUU-mA" name="NBPASSAGE" position="168">
        <attribute defType="com.stambia.rdbms.column.name" id="_iwKIGC43EeuCkMC4uUU-mA" value="NBPASSAGE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iwKIGS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iwKIGi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iwKIGy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iwKIHC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iwKIHS43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iwXjcC43EeuCkMC4uUU-mA" name="NBPOINT" position="169">
        <attribute defType="com.stambia.rdbms.column.name" id="_iwXjcS43EeuCkMC4uUU-mA" value="NBPOINT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iwXjci43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iwXjcy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iwXjdC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iwXjdS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iwXjdi43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iwXjdy43EeuCkMC4uUU-mA" name="CDLET" position="170">
        <attribute defType="com.stambia.rdbms.column.name" id="_iwXjeC43EeuCkMC4uUU-mA" value="CDLET"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iwXjeS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iwXjei43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iwXjey43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iwXjfC43EeuCkMC4uUU-mA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iwhUcC43EeuCkMC4uUU-mA" name="NUMCARTE" position="171">
        <attribute defType="com.stambia.rdbms.column.name" id="_iwhUcS43EeuCkMC4uUU-mA" value="NUMCARTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iwhUci43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iwhUcy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iwhUdC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iwhUdS43EeuCkMC4uUU-mA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iwqeYC43EeuCkMC4uUU-mA" name="PANNO" position="172">
        <attribute defType="com.stambia.rdbms.column.name" id="_iwqeYS43EeuCkMC4uUU-mA" value="PANNO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iwqeYi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iwqeYy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iwqeZC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iwqeZS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iwqeZi43EeuCkMC4uUU-mA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iwqeZy43EeuCkMC4uUU-mA" name="BQCV" position="173">
        <attribute defType="com.stambia.rdbms.column.name" id="_iwqeaC43EeuCkMC4uUU-mA" value="BQCV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iwqeaS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iwqeai43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iwqeay43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iwqebC43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iw0PYC43EeuCkMC4uUU-mA" name="WMPREPMAX" position="174">
        <attribute defType="com.stambia.rdbms.column.name" id="_iw0PYS43EeuCkMC4uUU-mA" value="WMPREPMAX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iw0PYi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iw0PYy43EeuCkMC4uUU-mA" value="2"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iw0PZC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iw0PZS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iw0PZi43EeuCkMC4uUU-mA" value="12"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iw0PZy43EeuCkMC4uUU-mA" name="RETNAT" position="175">
        <attribute defType="com.stambia.rdbms.column.name" id="_iw0PaC43EeuCkMC4uUU-mA" value="RETNAT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iw0PaS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iw0Pai43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iw0Pay43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iw0PbC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iw0PbS43EeuCkMC4uUU-mA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iw-AYC43EeuCkMC4uUU-mA" name="IDCLIENT" position="176">
        <attribute defType="com.stambia.rdbms.column.name" id="_iw-AYS43EeuCkMC4uUU-mA" value="IDCLIENT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iw-AYi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iw-AYy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iw-AZC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iw-AZS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ixHKUC43EeuCkMC4uUU-mA" name="CARTENO" position="177">
        <attribute defType="com.stambia.rdbms.column.name" id="_ixHKUS43EeuCkMC4uUU-mA" value="CARTENO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ixHKUi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ixHKUy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ixHKVC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ixHKVS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ixHKVi43EeuCkMC4uUU-mA" name="TRANSICOD" position="178">
        <attribute defType="com.stambia.rdbms.column.name" id="_ixHKVy43EeuCkMC4uUU-mA" value="TRANSICOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ixHKWC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ixHKWS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ixHKWi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ixHKWy43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ixUlsC43EeuCkMC4uUU-mA" name="LIEUINCT" position="179">
        <attribute defType="com.stambia.rdbms.column.name" id="_ixUlsS43EeuCkMC4uUU-mA" value="LIEUINCT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ixUlsi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ixUlsy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ixUltC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ixUltS43EeuCkMC4uUU-mA" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ixUlti43EeuCkMC4uUU-mA" name="SOURCE" position="180">
        <attribute defType="com.stambia.rdbms.column.name" id="_ixUlty43EeuCkMC4uUU-mA" value="SOURCE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ixUluC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ixUluS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ixUlui43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ixUluy43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ixeWsC43EeuCkMC4uUU-mA" name="SYSTEME" position="181">
        <attribute defType="com.stambia.rdbms.column.name" id="_ixeWsS43EeuCkMC4uUU-mA" value="SYSTEME"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ixeWsi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ixeWsy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ixeWtC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ixeWtS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ixeWti43EeuCkMC4uUU-mA" name="DOSEXTERNE" position="182">
        <attribute defType="com.stambia.rdbms.column.name" id="_ixeWty43EeuCkMC4uUU-mA" value="DOSEXTERNE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ixeWuC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ixeWuS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ixeWui43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ixeWuy43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ixoHsC43EeuCkMC4uUU-mA" name="ETBEXTERNE" position="183">
        <attribute defType="com.stambia.rdbms.column.name" id="_ixoHsS43EeuCkMC4uUU-mA" value="ETBEXTERNE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ixoHsi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ixoHsy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ixoHtC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ixoHtS43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ixxRoC43EeuCkMC4uUU-mA" name="CATCLICOD" position="184">
        <attribute defType="com.stambia.rdbms.column.name" id="_ixxRoS43EeuCkMC4uUU-mA" value="CATCLICOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ixxRoi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ixxRoy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ixxRpC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ixxRpS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ixxRpi43EeuCkMC4uUU-mA" name="CATPICOD" position="185">
        <attribute defType="com.stambia.rdbms.column.name" id="_ixxRpy43EeuCkMC4uUU-mA" value="CATPICOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ixxRqC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ixxRqS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ixxRqi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ixxRqy43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_ix7CoC43EeuCkMC4uUU-mA" name="CIRCUITVALIDATIONBLFL" position="186">
        <attribute defType="com.stambia.rdbms.column.name" id="_ix7CoS43EeuCkMC4uUU-mA" value="CIRCUITVALIDATIONBLFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_ix7Coi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_ix7Coy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_ix7CpC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_ix7CpS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_ix7Cpi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iyEMkC43EeuCkMC4uUU-mA" name="CIRCUITVALIDATIONFCTFL" position="187">
        <attribute defType="com.stambia.rdbms.column.name" id="_iyEMkS43EeuCkMC4uUU-mA" value="CIRCUITVALIDATIONFCTFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iyEMki43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iyEMky43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iyEMlC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iyEMlS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iyEMli43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iyEMly43EeuCkMC4uUU-mA" name="CONDEXP" position="188">
        <attribute defType="com.stambia.rdbms.column.name" id="_iyEMmC43EeuCkMC4uUU-mA" value="CONDEXP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iyEMmS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iyEMmi43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iyEMmy43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iyEMnC43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iyN9kC43EeuCkMC4uUU-mA" name="CONTACTTIERS" position="189">
        <attribute defType="com.stambia.rdbms.column.name" id="_iyN9kS43EeuCkMC4uUU-mA" value="CONTACTTIERS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iyN9ki43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iyN9ky43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iyN9lC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iyN9lS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iyRn8C43EeuCkMC4uUU-mA" name="IMPLSTCOL" position="190">
        <attribute defType="com.stambia.rdbms.column.name" id="_iyRn8S43EeuCkMC4uUU-mA" value="IMPLSTCOL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iyRn8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iyRn8y43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iyRn9C43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iyRn9S43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iyRn9i43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iybY8C43EeuCkMC4uUU-mA" name="MODEEXP" position="191">
        <attribute defType="com.stambia.rdbms.column.name" id="_iybY8S43EeuCkMC4uUU-mA" value="MODEEXP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iybY8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iybY8y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iybY9C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iybY9S43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iybY9i43EeuCkMC4uUU-mA" name="TAGID" position="192">
        <attribute defType="com.stambia.rdbms.column.name" id="_iybY9y43EeuCkMC4uUU-mA" value="TAGID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iybY-C43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_iybY-S43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iybY-i43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iybY-y43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iybY_C43EeuCkMC4uUU-mA" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iylJ8C43EeuCkMC4uUU-mA" name="TIERSFOU" position="193">
        <attribute defType="com.stambia.rdbms.column.name" id="_iylJ8S43EeuCkMC4uUU-mA" value="TIERSFOU"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iylJ8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iylJ8y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iylJ9C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iylJ9S43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iyuT4C43EeuCkMC4uUU-mA" name="UNTYP" position="194">
        <attribute defType="com.stambia.rdbms.column.name" id="_iyuT4S43EeuCkMC4uUU-mA" value="UNTYP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iyuT4i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iyuT4y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iyuT5C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iyuT5S43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iyuT5i43EeuCkMC4uUU-mA" name="PG_CLICHOMETTE" position="195">
        <attribute defType="com.stambia.rdbms.column.name" id="_iyuT5y43EeuCkMC4uUU-mA" value="PG_CLICHOMETTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iyuT6C43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iyuT6S43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iyuT6i43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iyuT6y43EeuCkMC4uUU-mA" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iy4E4C43EeuCkMC4uUU-mA" name="PG_CLICOBAL" position="196">
        <attribute defType="com.stambia.rdbms.column.name" id="_iy4E4S43EeuCkMC4uUU-mA" value="PG_CLICOBAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iy4E4i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iy4E4y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iy4E5C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iy4E5S43EeuCkMC4uUU-mA" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_iy4E5i43EeuCkMC4uUU-mA" name="PG_CLILANTIN" position="197">
        <attribute defType="com.stambia.rdbms.column.name" id="_iy4E5y43EeuCkMC4uUU-mA" value="PG_CLILANTIN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_iy4E6C43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_iy4E6S43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_iy4E6i43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_iy4E6y43EeuCkMC4uUU-mA" value="15"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_izBO0C43EeuCkMC4uUU-mA" name="PG_EMPLACEMENTNB" position="198">
        <attribute defType="com.stambia.rdbms.column.name" id="_izBO0S43EeuCkMC4uUU-mA" value="PG_EMPLACEMENTNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_izBO0i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_izBO0y43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_izBO1C43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_izBO1S43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_izBO1i43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_izK_0C43EeuCkMC4uUU-mA" name="PG_ETOILENB" position="199">
        <attribute defType="com.stambia.rdbms.column.name" id="_izK_0S43EeuCkMC4uUU-mA" value="PG_ETOILENB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_izK_0i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_izK_0y43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_izK_1C43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_izK_1S43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_izK_1i43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_izOqMC43EeuCkMC4uUU-mA" name="PG_LOCATIONNB" position="200">
        <attribute defType="com.stambia.rdbms.column.name" id="_izOqMS43EeuCkMC4uUU-mA" value="PG_LOCATIONNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_izOqMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_izOqMy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_izOqNC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_izOqNS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_izOqNi43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_izYbMC43EeuCkMC4uUU-mA" name="PG_RECAPJOUR" position="201">
        <attribute defType="com.stambia.rdbms.column.name" id="_izYbMS43EeuCkMC4uUU-mA" value="PG_RECAPJOUR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_izYbMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_izYbMy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_izYbNC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_izYbNS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_izYbNi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_izYbNy43EeuCkMC4uUU-mA" name="PG_TEL2" position="202">
        <attribute defType="com.stambia.rdbms.column.name" id="_izYbOC43EeuCkMC4uUU-mA" value="PG_TEL2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_izYbOS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_izYbOi43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_izYbOy43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_izYbPC43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_i0oYYC43EeuCkMC4uUU-mA" name="PK__CLI__D0BA12004A00F247">
        <node defType="com.stambia.rdbms.colref" id="_i0oYYS43EeuCkMC4uUU-mA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_i0oYYi43EeuCkMC4uUU-mA" ref="#_idjVIC43EeuCkMC4uUU-mA?fileId=_N_b74AcLEeu--f7ccO6NIw$type=md$name=CLI_ID?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_i7U2QS43EeuCkMC4uUU-mA" name="LART">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_i7U2Qi43EeuCkMC4uUU-mA" value="LART"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_i7U2Qy43EeuCkMC4uUU-mA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_i71zoC43EeuCkMC4uUU-mA" name="LART_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_i71zoS43EeuCkMC4uUU-mA" value="LART_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i71zoi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i71zoy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i71zpC43EeuCkMC4uUU-mA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i71zpS43EeuCkMC4uUU-mA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i71zpi43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i7-9kC43EeuCkMC4uUU-mA" name="CE1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_i7-9kS43EeuCkMC4uUU-mA" value="CE1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i7-9ki43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i7-9ky43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i7-9lC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i7-9lS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i7-9li43EeuCkMC4uUU-mA" name="CE2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_i7-9ly43EeuCkMC4uUU-mA" value="CE2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i7-9mC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i7-9mS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i7-9mi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i7-9my43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i8IukC43EeuCkMC4uUU-mA" name="CE3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_i8IukS43EeuCkMC4uUU-mA" value="CE3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i8Iuki43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i8Iuky43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i8IulC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i8IulS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i8SfkC43EeuCkMC4uUU-mA" name="CE4" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_i8SfkS43EeuCkMC4uUU-mA" value="CE4"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i8Sfki43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i8Sfky43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i8SflC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i8SflS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i8bpgC43EeuCkMC4uUU-mA" name="CE5" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_i8bpgS43EeuCkMC4uUU-mA" value="CE5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i8bpgi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i8bpgy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i8bphC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i8bphS43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i8y14C43EeuCkMC4uUU-mA" name="CE6" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_i8y14S43EeuCkMC4uUU-mA" value="CE6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i8y14i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i8y14y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i8y15C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i8y15S43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i8y15i43EeuCkMC4uUU-mA" name="CE7" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_i8y15y43EeuCkMC4uUU-mA" value="CE7"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i8y16C43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i8y16S43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i8y16i43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i8y16y43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i88m4C43EeuCkMC4uUU-mA" name="CE8" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_i88m4S43EeuCkMC4uUU-mA" value="CE8"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i88m4i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i88m4y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i88m5C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i88m5S43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i9Fw0C43EeuCkMC4uUU-mA" name="CE9" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_i9Fw0S43EeuCkMC4uUU-mA" value="CE9"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i9Fw0i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i9Fw0y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i9Fw1C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i9Fw1S43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i9Fw1i43EeuCkMC4uUU-mA" name="CEA" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_i9Fw1y43EeuCkMC4uUU-mA" value="CEA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i9Fw2C43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i9Fw2S43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i9Fw2i43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i9Fw2y43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i9Ph0C43EeuCkMC4uUU-mA" name="DOS" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_i9Ph0S43EeuCkMC4uUU-mA" value="DOS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i9Ph0i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i9Ph0y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i9Ph1C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i9Ph1S43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i9YrwC43EeuCkMC4uUU-mA" name="REF" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_i9YrwS43EeuCkMC4uUU-mA" value="REF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i9Yrwi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i9Yrwy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i9YrxC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i9YrxS43EeuCkMC4uUU-mA" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i9Yrxi43EeuCkMC4uUU-mA" name="SREF1" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_i9Yrxy43EeuCkMC4uUU-mA" value="SREF1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i9YryC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i9YryS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i9Yryi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i9Yryy43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i9icwC43EeuCkMC4uUU-mA" name="SREF2" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_i9icwS43EeuCkMC4uUU-mA" value="SREF2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i9icwi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i9icwy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i9icxC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i9icxS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i9sNwC43EeuCkMC4uUU-mA" name="USERCR" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_i9sNwS43EeuCkMC4uUU-mA" value="USERCR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i9sNwi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i9sNwy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i9sNxC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i9sNxS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i9v4IC43EeuCkMC4uUU-mA" name="USERMO" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_i9v4IS43EeuCkMC4uUU-mA" value="USERMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i9v4Ii43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i9v4Iy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i9v4JC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i9v4JS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i95pIC43EeuCkMC4uUU-mA" name="CONF" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_i95pIS43EeuCkMC4uUU-mA" value="CONF"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i95pIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i95pIy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i95pJC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i95pJS43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i95pJi43EeuCkMC4uUU-mA" name="LANG" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_i95pJy43EeuCkMC4uUU-mA" value="LANG"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i95pKC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i95pKS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i95pKi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i95pKy43EeuCkMC4uUU-mA" value="2"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i-CzEC43EeuCkMC4uUU-mA" name="EDCOD_0001" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_i-CzES43EeuCkMC4uUU-mA" value="EDCOD_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i-CzEi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i-CzEy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i-CzFC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i-CzFS43EeuCkMC4uUU-mA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i-MkEC43EeuCkMC4uUU-mA" name="EDCOD_0002" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_i-MkES43EeuCkMC4uUU-mA" value="EDCOD_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i-MkEi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i-MkEy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i-MkFC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i-MkFS43EeuCkMC4uUU-mA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i-MkFi43EeuCkMC4uUU-mA" name="EDCOD_0003" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_i-MkFy43EeuCkMC4uUU-mA" value="EDCOD_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i-MkGC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i-MkGS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i-MkGi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i-MkGy43EeuCkMC4uUU-mA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i-ffAC43EeuCkMC4uUU-mA" name="EDCOD_0004" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_i-ffAS43EeuCkMC4uUU-mA" value="EDCOD_0004"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i-ffAi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i-ffAy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i-ffBC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i-ffBS43EeuCkMC4uUU-mA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i-pQAC43EeuCkMC4uUU-mA" name="EDCOD_0005" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_i-pQAS43EeuCkMC4uUU-mA" value="EDCOD_0005"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i-pQAi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i-pQAy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i-pQBC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i-pQBS43EeuCkMC4uUU-mA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i-s6YC43EeuCkMC4uUU-mA" name="EDCOD_0006" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_i-s6YS43EeuCkMC4uUU-mA" value="EDCOD_0006"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i-s6Yi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i-s6Yy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i-s6ZC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i-s6ZS43EeuCkMC4uUU-mA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i-s6Zi43EeuCkMC4uUU-mA" name="EDCOD_0007" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_i-s6Zy43EeuCkMC4uUU-mA" value="EDCOD_0007"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i-s6aC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i-s6aS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i-s6ai43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i-s6ay43EeuCkMC4uUU-mA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i-2rYC43EeuCkMC4uUU-mA" name="EDCOD_0008" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_i-2rYS43EeuCkMC4uUU-mA" value="EDCOD_0008"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i-2rYi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i-2rYy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i-2rZC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i-2rZS43EeuCkMC4uUU-mA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i-_1UC43EeuCkMC4uUU-mA" name="EDCOD_0009" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_i-_1US43EeuCkMC4uUU-mA" value="EDCOD_0009"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i-_1Ui43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i-_1Uy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i-_1VC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i-_1VS43EeuCkMC4uUU-mA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i-_1Vi43EeuCkMC4uUU-mA" name="EDCOD_0010" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_i-_1Vy43EeuCkMC4uUU-mA" value="EDCOD_0010"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i-_1WC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i-_1WS43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i-_1Wi43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i-_1Wy43EeuCkMC4uUU-mA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i_JmUC43EeuCkMC4uUU-mA" name="USERCRDH" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_i_JmUS43EeuCkMC4uUU-mA" value="USERCRDH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i_JmUi43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i_JmUy43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i_JmVC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i_JmVS43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i_JmVi43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i_TXUC43EeuCkMC4uUU-mA" name="USERMODH" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_i_TXUS43EeuCkMC4uUU-mA" value="USERMODH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i_TXUi43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i_TXUy43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i_TXVC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i_TXVS43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i_TXVi43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i_TXVy43EeuCkMC4uUU-mA" name="NOTE_0001" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_i_TXWC43EeuCkMC4uUU-mA" value="NOTE_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i_TXWS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i_TXWi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i_TXWy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i_TXXC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i_TXXS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i_chQC43EeuCkMC4uUU-mA" name="NOTE_0002" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_i_chQS43EeuCkMC4uUU-mA" value="NOTE_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i_chQi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i_chQy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i_chRC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i_chRS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i_chRi43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i_chRy43EeuCkMC4uUU-mA" name="NOTE_0003" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_i_chSC43EeuCkMC4uUU-mA" value="NOTE_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i_chSS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i_chSi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i_chSy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i_chTC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i_chTS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i_p8oC43EeuCkMC4uUU-mA" name="NOTE_0004" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_i_p8oS43EeuCkMC4uUU-mA" value="NOTE_0004"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i_p8oi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i_p8oy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i_p8pC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i_p8pS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i_p8pi43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i_ztoC43EeuCkMC4uUU-mA" name="NOTE_0005" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_i_ztoS43EeuCkMC4uUU-mA" value="NOTE_0005"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i_ztoi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i_ztoy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i_ztpC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i_ztpS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i_ztpi43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i_ztpy43EeuCkMC4uUU-mA" name="NOTE_0006" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_i_ztqC43EeuCkMC4uUU-mA" value="NOTE_0006"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i_ztqS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i_ztqi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i_ztqy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i_ztrC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i_ztrS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i_83kC43EeuCkMC4uUU-mA" name="NOTE_0007" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_i_83kS43EeuCkMC4uUU-mA" value="NOTE_0007"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_i_83ki43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_i_83ky43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_i_83lC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_i_83lS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_i_83li43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_i_83ly43EeuCkMC4uUU-mA" name="NOTE_0008" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_jAGokC43EeuCkMC4uUU-mA" value="NOTE_0008"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jAGokS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jAGoki43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jAGoky43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jAGolC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jAGolS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jAGoli43EeuCkMC4uUU-mA" name="NOTE_0009" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_jAGoly43EeuCkMC4uUU-mA" value="NOTE_0009"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jAGomC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jAGomS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jAGomi43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jAGomy43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jAGonC43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jAQZkC43EeuCkMC4uUU-mA" name="NOTE_0010" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_jAQZkS43EeuCkMC4uUU-mA" value="NOTE_0010"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jAQZki43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jAQZky43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jAQZlC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jAQZlS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jAQZli43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jAZjgC43EeuCkMC4uUU-mA" name="ICPFL" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_jAZjgS43EeuCkMC4uUU-mA" value="ICPFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jAZjgi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jAZjgy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jAZjhC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jAZjhS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jAZjhi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_jAwv4C43EeuCkMC4uUU-mA" name="PK__LART__2ECB2304564CF689">
        <node defType="com.stambia.rdbms.colref" id="_jAwv4S43EeuCkMC4uUU-mA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_jAwv4i43EeuCkMC4uUU-mA" ref="#_i71zoC43EeuCkMC4uUU-mA?fileId=_N_b74AcLEeu--f7ccO6NIw$type=md$name=LART_ID?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_jG2JwS43EeuCkMC4uUU-mA" name="T100">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_jG2Jwi43EeuCkMC4uUU-mA" value="T100"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_jG2Jwy43EeuCkMC4uUU-mA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_jHJEsC43EeuCkMC4uUU-mA" name="T100_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_jHJEsS43EeuCkMC4uUU-mA" value="T100_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jHJEsi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jHJEsy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jHJEtC43EeuCkMC4uUU-mA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jHJEtS43EeuCkMC4uUU-mA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jHJEti43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jHTcwC43EeuCkMC4uUU-mA" name="CEBIN" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_jHTcwS43EeuCkMC4uUU-mA" value="CEBIN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jHTcwi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jHTcwy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jHTcxC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jHTcxS43EeuCkMC4uUU-mA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jHTcxi43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jHdNwC43EeuCkMC4uUU-mA" name="DOS" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_jHdNwS43EeuCkMC4uUU-mA" value="DOS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jHdNwi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jHdNwy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jHdNxC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jHdNxS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jHdNxi43EeuCkMC4uUU-mA" name="TABNO" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_jHdNxy43EeuCkMC4uUU-mA" value="TABNO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jHdNyC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jHdNyS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jHdNyi43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jHdNyy43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jHdNzC43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jHwIsC43EeuCkMC4uUU-mA" name="LIB" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_jHwIsS43EeuCkMC4uUU-mA" value="LIB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jHwIsi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jHwIsy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jHwItC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jHwItS43EeuCkMC4uUU-mA" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jH55sC43EeuCkMC4uUU-mA" name="USERCR" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_jH55sS43EeuCkMC4uUU-mA" value="USERCR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jH55si43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jH55sy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jH55tC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jH55tS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jIDDoC43EeuCkMC4uUU-mA" name="USERMO" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_jIDDoS43EeuCkMC4uUU-mA" value="USERMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jIDDoi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jIDDoy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jIDDpC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jIDDpS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jIDDpi43EeuCkMC4uUU-mA" name="USERCRDH" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_jIDDpy43EeuCkMC4uUU-mA" value="USERCRDH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jIDDqC43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jIDDqS43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jIDDqi43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jIDDqy43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jIDDrC43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jIQfAC43EeuCkMC4uUU-mA" name="USERMODH" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_jIQfAS43EeuCkMC4uUU-mA" value="USERMODH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jIQfAi43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jIQfAy43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jIQfBC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jIQfBS43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jIQfBi43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jIaQAC43EeuCkMC4uUU-mA" name="CENOTE" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_jIaQAS43EeuCkMC4uUU-mA" value="CENOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jIaQAi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jIaQAy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jIaQBC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jIaQBS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jIaQBi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jIkBAC43EeuCkMC4uUU-mA" name="NOTE" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_jIkBAS43EeuCkMC4uUU-mA" value="NOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jIkBAi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jIkBAy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jIkBBC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jIkBBS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jIkBBi43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jIkBBy43EeuCkMC4uUU-mA" name="DEEECPTA" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_jIkBCC43EeuCkMC4uUU-mA" value="DEEECPTA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jIkBCS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jIkBCi43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jIkBCy43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jIkBDC43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jItK8C43EeuCkMC4uUU-mA" name="DEEECPTV" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_jItK8S43EeuCkMC4uUU-mA" value="DEEECPTV"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jItK8i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jItK8y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jItK9C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jItK9S43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jI278C43EeuCkMC4uUU-mA" name="DEEEPU" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_jI278S43EeuCkMC4uUU-mA" value="DEEEPU"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jI278i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jI278y43EeuCkMC4uUU-mA" value="4"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jI279C43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jI279S43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jI279i43EeuCkMC4uUU-mA" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jI279y43EeuCkMC4uUU-mA" name="DEEEUN" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_jI27-C43EeuCkMC4uUU-mA" value="DEEEUN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jI27-S43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jI27-i43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jI27-y43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jI27_C43EeuCkMC4uUU-mA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jJAF4C43EeuCkMC4uUU-mA" name="EFFETDT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_jJAF4S43EeuCkMC4uUU-mA" value="EFFETDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jJAF4i43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jJAF4y43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jJAF5C43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jJAF5S43EeuCkMC4uUU-mA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jJAF5i43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jJJ24C43EeuCkMC4uUU-mA" name="HTCOD" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_jJJ24S43EeuCkMC4uUU-mA" value="HTCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jJJ24i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jJJ24y43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jJJ25C43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jJJ25S43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jJJ25i43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jJNhQC43EeuCkMC4uUU-mA" name="PAY" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_jJNhQS43EeuCkMC4uUU-mA" value="PAY"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jJNhQi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jJNhQy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jJNhRC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jJNhRS43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jJXSQC43EeuCkMC4uUU-mA" name="TAXCOD" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_jJXSQS43EeuCkMC4uUU-mA" value="TAXCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jJXSQi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jJXSQy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jJXSRC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jJXSRS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_jJqNMC43EeuCkMC4uUU-mA" name="PK__T100__E269796B6450FD1D">
        <node defType="com.stambia.rdbms.colref" id="_jJqNMS43EeuCkMC4uUU-mA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_jJqNMi43EeuCkMC4uUU-mA" ref="#_jHJEsC43EeuCkMC4uUU-mA?fileId=_N_b74AcLEeu--f7ccO6NIw$type=md$name=T100_ID?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_jEXdgS43EeuCkMC4uUU-mA" name="T012">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_jEXdgi43EeuCkMC4uUU-mA" value="T012"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_jEXdgy43EeuCkMC4uUU-mA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_jEup4C43EeuCkMC4uUU-mA" name="T012_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_jEup4S43EeuCkMC4uUU-mA" value="T012_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jEup4i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jEup4y43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jEup5C43EeuCkMC4uUU-mA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jEup5S43EeuCkMC4uUU-mA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jEup5i43EeuCkMC4uUU-mA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jEup5y43EeuCkMC4uUU-mA" name="CEBIN" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_jEup6C43EeuCkMC4uUU-mA" value="CEBIN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jEup6S43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jEup6i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jEup6y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jEup7C43EeuCkMC4uUU-mA" value="tinyint"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jEup7S43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jE4a4C43EeuCkMC4uUU-mA" name="DOS" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_jE4a4S43EeuCkMC4uUU-mA" value="DOS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jE4a4i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jE4a4y43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jE4a5C43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jE4a5S43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jFBk0C43EeuCkMC4uUU-mA" name="TABNO" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_jFBk0S43EeuCkMC4uUU-mA" value="TABNO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jFBk0i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jFBk0y43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jFBk1C43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jFBk1S43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jFBk1i43EeuCkMC4uUU-mA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jFLV0C43EeuCkMC4uUU-mA" name="FAMNO" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_jFLV0S43EeuCkMC4uUU-mA" value="FAMNO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jFLV0i43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jFLV0y43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jFLV1C43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jFLV1S43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jFLV1i43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jFLV1y43EeuCkMC4uUU-mA" name="FAM" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_jFLV2C43EeuCkMC4uUU-mA" value="FAM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jFLV2S43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jFLV2i43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jFLV2y43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jFLV3C43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jFYxMC43EeuCkMC4uUU-mA" name="USERCR" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_jFYxMS43EeuCkMC4uUU-mA" value="USERCR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jFYxMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jFYxMy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jFYxNC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jFYxNS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jFiiMC43EeuCkMC4uUU-mA" name="USERMO" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_jFiiMS43EeuCkMC4uUU-mA" value="USERMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jFiiMi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jFiiMy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jFiiNC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jFiiNS43EeuCkMC4uUU-mA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jFiiNi43EeuCkMC4uUU-mA" name="USERCRDH" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_jFiiNy43EeuCkMC4uUU-mA" value="USERCRDH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jFiiOC43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jFiiOS43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jFiiOi43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jFiiOy43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jFiiPC43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jFsTMC43EeuCkMC4uUU-mA" name="USERMODH" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_jFsTMS43EeuCkMC4uUU-mA" value="USERMODH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jFsTMi43EeuCkMC4uUU-mA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jFsTMy43EeuCkMC4uUU-mA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jFsTNC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jFsTNS43EeuCkMC4uUU-mA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jFsTNi43EeuCkMC4uUU-mA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jF1dIC43EeuCkMC4uUU-mA" name="CENOTE" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_jF1dIS43EeuCkMC4uUU-mA" value="CENOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jF1dIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jF1dIy43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jF1dJC43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jF1dJS43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jF1dJi43EeuCkMC4uUU-mA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jF1dJy43EeuCkMC4uUU-mA" name="NOTE" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_jF1dKC43EeuCkMC4uUU-mA" value="NOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jF1dKS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jF1dKi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jF1dKy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jF1dLC43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jF1dLS43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jF_OIC43EeuCkMC4uUU-mA" name="LIB" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_jF_OIS43EeuCkMC4uUU-mA" value="LIB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jF_OIi43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jF_OIy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jF_OJC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jF_OJS43EeuCkMC4uUU-mA" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jGI_IC43EeuCkMC4uUU-mA" name="QUESTION" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_jGI_IS43EeuCkMC4uUU-mA" value="QUESTION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jGI_Ii43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jGI_Iy43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jGI_JC43EeuCkMC4uUU-mA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jGI_JS43EeuCkMC4uUU-mA" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_jGI_Ji43EeuCkMC4uUU-mA" name="PG_ORDRE" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_jGI_Jy43EeuCkMC4uUU-mA" value="PG_ORDRE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_jGI_KC43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_jGI_KS43EeuCkMC4uUU-mA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_jGI_Ki43EeuCkMC4uUU-mA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_jGI_Ky43EeuCkMC4uUU-mA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_jGI_LC43EeuCkMC4uUU-mA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_jGp8gC43EeuCkMC4uUU-mA" name="PK__T012__E442B46F77A39307">
        <node defType="com.stambia.rdbms.colref" id="_jGp8gS43EeuCkMC4uUU-mA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_jGp8gi43EeuCkMC4uUU-mA" ref="#_jEup4C43EeuCkMC4uUU-mA?fileId=_N_b74AcLEeu--f7ccO6NIw$type=md$name=T012_ID?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_YV_KAS8NEeuJGap-USsnqg" name="T2">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_YV_KAi8NEeuJGap-USsnqg" value="T2"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_YV_KAy8NEeuJGap-USsnqg" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_YXF9QC8NEeuJGap-USsnqg" name="T2_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_YXF9QS8NEeuJGap-USsnqg" value="T2_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YXF9Qi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YXF9Qy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YXF9RC8NEeuJGap-USsnqg" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YXF9RS8NEeuJGap-USsnqg" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YXF9Ri8NEeuJGap-USsnqg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YXdJoC8NEeuJGap-USsnqg" name="CE1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_YXdJoS8NEeuJGap-USsnqg" value="CE1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YXdJoi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YXdJoy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YXdJpC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YXdJpS8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YXm6oC8NEeuJGap-USsnqg" name="CE2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_YXm6oS8NEeuJGap-USsnqg" value="CE2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YXm6oi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YXm6oy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YXm6pC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YXm6pS8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YX51kC8NEeuJGap-USsnqg" name="CE3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_YX51kS8NEeuJGap-USsnqg" value="CE3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YX51ki8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YX51ky8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YX51lC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YX51lS8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YYMwgC8NEeuJGap-USsnqg" name="CE4" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_YYMwgS8NEeuJGap-USsnqg" value="CE4"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YYMwgi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YYMwgy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YYMwhC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YYMwhS8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YYaL4C8NEeuJGap-USsnqg" name="CE5" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_YYaL4S8NEeuJGap-USsnqg" value="CE5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YYaL4i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YYaL4y8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YYaL5C8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YYaL5S8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YYj84C8NEeuJGap-USsnqg" name="CE6" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_YYj84S8NEeuJGap-USsnqg" value="CE6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YYj84i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YYj84y8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YYj85C8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YYj85S8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YYtt4C8NEeuJGap-USsnqg" name="CE7" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_YYtt4S8NEeuJGap-USsnqg" value="CE7"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YYtt4i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YYtt4y8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YYtt5C8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YYtt5S8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YY230C8NEeuJGap-USsnqg" name="CE8" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_YY230S8NEeuJGap-USsnqg" value="CE8"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YY230i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YY230y8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YY231C8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YY231S8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YZAo0C8NEeuJGap-USsnqg" name="CE9" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_YZAo0S8NEeuJGap-USsnqg" value="CE9"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YZAo0i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YZAo0y8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YZAo1C8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YZAo1S8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YZTjwC8NEeuJGap-USsnqg" name="CEA" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_YZTjwS8NEeuJGap-USsnqg" value="CEA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YZTjwi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YZTjwy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YZTjxC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YZTjxS8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YZXOIC8NEeuJGap-USsnqg" name="DOS" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_YZXOIS8NEeuJGap-USsnqg" value="DOS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YZXOIi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YZXOIy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YZXOJC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YZXOJS8NEeuJGap-USsnqg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YZqwIC8NEeuJGap-USsnqg" name="TIERS" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_YZqwIS8NEeuJGap-USsnqg" value="TIERS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YZqwIi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YZqwIy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YZqwJC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YZqwJS8NEeuJGap-USsnqg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YZqwJi8NEeuJGap-USsnqg" name="USERCR" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_YZqwJy8NEeuJGap-USsnqg" value="USERCR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YZqwKC8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YZqwKS8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YZqwKi8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YZqwKy8NEeuJGap-USsnqg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YZ9rEC8NEeuJGap-USsnqg" name="USERMO" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_YZ9rES8NEeuJGap-USsnqg" value="USERMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YZ9rEi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YZ9rEy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YZ9rFC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YZ9rFS8NEeuJGap-USsnqg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YaG1AC8NEeuJGap-USsnqg" name="CONTACT" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_YaG1AS8NEeuJGap-USsnqg" value="CONTACT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YaG1Ai8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YaG1Ay8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YaG1BC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YaG1BS8NEeuJGap-USsnqg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YaU3cC8NEeuJGap-USsnqg" name="NOM" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_YaU3cS8NEeuJGap-USsnqg" value="NOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YaU3ci8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YaU3cy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YaU3dC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YaU3dS8NEeuJGap-USsnqg" value="80"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YaxjYC8NEeuJGap-USsnqg" name="LIB" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_YaxjYS8NEeuJGap-USsnqg" value="LIB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YaxjYi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YaxjYy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YaxjZC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YaxjZS8NEeuJGap-USsnqg" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YbEeUC8NEeuJGap-USsnqg" name="TEL" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_YbEeUS8NEeuJGap-USsnqg" value="TEL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YbEeUi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YbEeUy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YbEeVC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YbEeVS8NEeuJGap-USsnqg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YbR5sC8NEeuJGap-USsnqg" name="TELGSM" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_YbR5sS8NEeuJGap-USsnqg" value="TELGSM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YbR5si8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YbR5sy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YbR5tC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YbR5tS8NEeuJGap-USsnqg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YbbqsC8NEeuJGap-USsnqg" name="FAX" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_YbbqsS8NEeuJGap-USsnqg" value="FAX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ybbqsi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ybbqsy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YbbqtC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YbbqtS8NEeuJGap-USsnqg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ybk0oC8NEeuJGap-USsnqg" name="EMAIL" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ybk0oS8NEeuJGap-USsnqg" value="EMAIL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ybk0oi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ybk0oy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ybk0pC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ybk0pS8NEeuJGap-USsnqg" value="80"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YbuloC8NEeuJGap-USsnqg" name="TIT" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_YbuloS8NEeuJGap-USsnqg" value="TIT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ybuloi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ybuloy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YbulpC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YbulpS8NEeuJGap-USsnqg" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ybulpi8NEeuJGap-USsnqg" name="SERVCOD" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ybulpy8NEeuJGap-USsnqg" value="SERVCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YbulqC8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YbulqS8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ybulqi8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ybulqy8NEeuJGap-USsnqg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Yb4WoC8NEeuJGap-USsnqg" name="ADRCOD" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_Yb4WoS8NEeuJGap-USsnqg" value="ADRCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Yb4Woi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Yb4Woy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Yb4WpC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Yb4WpS8NEeuJGap-USsnqg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YcBgkC8NEeuJGap-USsnqg" name="PRENOM" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_YcBgkS8NEeuJGap-USsnqg" value="PRENOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YcBgki8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YcBgky8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YcBglC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YcBglS8NEeuJGap-USsnqg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YcO78C8NEeuJGap-USsnqg" name="FCTCOD_0001" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_YcO78S8NEeuJGap-USsnqg" value="FCTCOD_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YcO78i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YcO78y8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YcO79C8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YcO79S8NEeuJGap-USsnqg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YcYs8C8NEeuJGap-USsnqg" name="FCTCOD_0002" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_YcYs8S8NEeuJGap-USsnqg" value="FCTCOD_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YcYs8i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YcYs8y8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YcYs9C8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YcYs9S8NEeuJGap-USsnqg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YcYs9i8NEeuJGap-USsnqg" name="FCTCOD_0003" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_YcYs9y8NEeuJGap-USsnqg" value="FCTCOD_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YcYs-C8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YcYs-S8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YcYs-i8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YcYs-y8NEeuJGap-USsnqg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ycrn4C8NEeuJGap-USsnqg" name="FCTCOD_0004" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ycrn4S8NEeuJGap-USsnqg" value="FCTCOD_0004"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ycrn4i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ycrn4y8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ycrn5C8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ycrn5S8NEeuJGap-USsnqg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Yc1Y4C8NEeuJGap-USsnqg" name="FCTCOD_0005" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_Yc1Y4S8NEeuJGap-USsnqg" value="FCTCOD_0005"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Yc1Y4i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Yc1Y4y8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Yc1Y5C8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Yc1Y5S8NEeuJGap-USsnqg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Yc-i0C8NEeuJGap-USsnqg" name="FCTCOD_0006" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_Yc-i0S8NEeuJGap-USsnqg" value="FCTCOD_0006"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Yc-i0i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Yc-i0y8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Yc-i1C8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Yc-i1S8NEeuJGap-USsnqg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YdL-MC8NEeuJGap-USsnqg" name="FCTCOD_0007" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_YdL-MS8NEeuJGap-USsnqg" value="FCTCOD_0007"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YdL-Mi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YdL-My8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YdL-NC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YdL-NS8NEeuJGap-USsnqg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YdVvMC8NEeuJGap-USsnqg" name="FCTCOD_0008" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_YdVvMS8NEeuJGap-USsnqg" value="FCTCOD_0008"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YdVvMi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YdVvMy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YdVvNC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YdVvNS8NEeuJGap-USsnqg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YdfgMC8NEeuJGap-USsnqg" name="FCTCOD_0009" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_YdfgMS8NEeuJGap-USsnqg" value="FCTCOD_0009"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YdfgMi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YdfgMy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YdfgNC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YdfgNS8NEeuJGap-USsnqg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YdybIC8NEeuJGap-USsnqg" name="FCTCOD_0010" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_YdybIS8NEeuJGap-USsnqg" value="FCTCOD_0010"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YdybIi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YdybIy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YdybJC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YdybJS8NEeuJGap-USsnqg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Yd8MIC8NEeuJGap-USsnqg" name="QUESTION" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_Yd8MIS8NEeuJGap-USsnqg" value="QUESTION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Yd8MIi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Yd8MIy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Yd8MJC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Yd8MJS8NEeuJGap-USsnqg" value="32"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YeSxcC8NEeuJGap-USsnqg" name="NOMABR" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_YeSxcS8NEeuJGap-USsnqg" value="NOMABR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YeSxci8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YeSxcy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YeSxdC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YeSxdS8NEeuJGap-USsnqg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YecicC8NEeuJGap-USsnqg" name="LOGIN" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_YecicS8NEeuJGap-USsnqg" value="LOGIN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Yecici8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Yecicy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YecidC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YecidS8NEeuJGap-USsnqg" value="25"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YemTcC8NEeuJGap-USsnqg" name="WEBPASS" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_YemTcS8NEeuJGap-USsnqg" value="WEBPASS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YemTci8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YemTcy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YemTdC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YemTdS8NEeuJGap-USsnqg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YevdYC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0001" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_YevdYS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YevdYi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YevdYy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YevdZC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YevdZS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YevdZi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YfIe8C8NEeuJGap-USsnqg" name="WEBAUTHCOD_0002" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_YfIe8S8NEeuJGap-USsnqg" value="WEBAUTHCOD_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YfIe8i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YfIe8y8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YfIe9C8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YfIe9S8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YfIe9i8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YfSP8C8NEeuJGap-USsnqg" name="WEBAUTHCOD_0003" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_YfSP8S8NEeuJGap-USsnqg" value="WEBAUTHCOD_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YfSP8i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YfSP8y8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YfSP9C8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YfSP9S8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YfSP9i8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YfbZ4C8NEeuJGap-USsnqg" name="WEBAUTHCOD_0004" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_YfbZ4S8NEeuJGap-USsnqg" value="WEBAUTHCOD_0004"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YfbZ4i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YfbZ4y8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YfbZ5C8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YfbZ5S8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YfbZ5i8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YflK4C8NEeuJGap-USsnqg" name="WEBAUTHCOD_0005" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_YflK4S8NEeuJGap-USsnqg" value="WEBAUTHCOD_0005"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YflK4i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YflK4y8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YflK5C8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YflK5S8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YflK5i8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YfuU0C8NEeuJGap-USsnqg" name="WEBAUTHCOD_0006" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_YfuU0S8NEeuJGap-USsnqg" value="WEBAUTHCOD_0006"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YfuU0i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YfuU0y8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YfuU1C8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YfuU1S8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YfuU1i8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Yf4F0C8NEeuJGap-USsnqg" name="WEBAUTHCOD_0007" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_Yf4F0S8NEeuJGap-USsnqg" value="WEBAUTHCOD_0007"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Yf4F0i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Yf4F0y8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Yf4F1C8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Yf4F1S8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Yf4F1i8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YgFhMC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0008" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_YgFhMS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0008"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YgFhMi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YgFhMy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YgFhNC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YgFhNS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YgFhNi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YgPSMC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0009" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_YgPSMS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0009"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YgPSMi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YgPSMy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YgPSNC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YgPSNS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YgPSNi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YgYcIC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0010" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_YgYcIS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0010"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YgYcIi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YgYcIy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YgYcJC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YgYcJS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YgYcJi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ygr-IC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0011" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ygr-IS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0011"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ygr-Ii8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ygr-Iy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ygr-JC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ygr-JS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ygr-Ji8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ygr-Jy8NEeuJGap-USsnqg" name="WEBAUTHCOD_0012" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ygr-KC8NEeuJGap-USsnqg" value="WEBAUTHCOD_0012"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ygr-KS8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ygr-Ki8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ygr-Ky8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ygr-LC8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ygr-LS8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Yg1IEC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0013" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_Yg1IES8NEeuJGap-USsnqg" value="WEBAUTHCOD_0013"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Yg1IEi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Yg1IEy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Yg1IFC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Yg1IFS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Yg1IFi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YhCjcC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0014" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_YhCjcS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0014"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YhCjci8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YhCjcy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YhCjdC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YhCjdS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YhCjdi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YhMUcC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0015" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_YhMUcS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0015"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YhMUci8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YhMUcy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YhMUdC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YhMUdS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YhMUdi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YhWFcC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0016" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_YhWFcS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0016"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YhWFci8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YhWFcy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YhWFdC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YhWFdS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YhWFdi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YhfPYC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0017" position="57">
        <attribute defType="com.stambia.rdbms.column.name" id="_YhfPYS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0017"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YhfPYi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YhfPYy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YhfPZC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YhfPZS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YhfPZi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YhfPZy8NEeuJGap-USsnqg" name="WEBAUTHCOD_0018" position="58">
        <attribute defType="com.stambia.rdbms.column.name" id="_YhfPaC8NEeuJGap-USsnqg" value="WEBAUTHCOD_0018"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YhfPaS8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YhfPai8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YhfPay8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YhfPbC8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YhfPbS8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YhyxYC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0019" position="59">
        <attribute defType="com.stambia.rdbms.column.name" id="_YhyxYS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0019"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YhyxYi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YhyxYy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YhyxZC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YhyxZS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YhyxZi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Yh-XkC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0020" position="60">
        <attribute defType="com.stambia.rdbms.column.name" id="_Yh-XkS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0020"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Yh-Xki8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Yh-Xky8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Yh-XlC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Yh-XlS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Yh-Xli8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YiAz0C8NEeuJGap-USsnqg" name="WEBAUTHCOD_0021" position="61">
        <attribute defType="com.stambia.rdbms.column.name" id="_YiAz0S8NEeuJGap-USsnqg" value="WEBAUTHCOD_0021"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YiAz0i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YiAz0y8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YiAz1C8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YiAz1S8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YiAz1i8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YiKk0C8NEeuJGap-USsnqg" name="WEBAUTHCOD_0022" position="62">
        <attribute defType="com.stambia.rdbms.column.name" id="_YiKk0S8NEeuJGap-USsnqg" value="WEBAUTHCOD_0022"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YiKk0i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YiKk0y8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YiKk1C8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YiKk1S8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YiKk1i8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YiUV0C8NEeuJGap-USsnqg" name="WEBAUTHCOD_0023" position="63">
        <attribute defType="com.stambia.rdbms.column.name" id="_YiUV0S8NEeuJGap-USsnqg" value="WEBAUTHCOD_0023"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YiUV0i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YiUV0y8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YiUV1C8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YiUV1S8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YiUV1i8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YidfwC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0024" position="64">
        <attribute defType="com.stambia.rdbms.column.name" id="_YidfwS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0024"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Yidfwi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Yidfwy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YidfxC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YidfxS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Yidfxi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YinQwC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0025" position="65">
        <attribute defType="com.stambia.rdbms.column.name" id="_YinQwS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0025"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YinQwi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YinQwy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YinQxC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YinQxS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YinQxi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YixBwC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0026" position="66">
        <attribute defType="com.stambia.rdbms.column.name" id="_YixBwS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0026"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YixBwi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YixBwy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YixBxC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YixBxS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YixBxi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Yi-dIC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0027" position="67">
        <attribute defType="com.stambia.rdbms.column.name" id="_Yi-dIS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0027"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Yi-dIi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Yi-dIy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Yi-dJC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Yi-dJS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Yi-dJi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YjIOIC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0028" position="68">
        <attribute defType="com.stambia.rdbms.column.name" id="_YjIOIS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0028"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YjIOIi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YjIOIy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YjIOJC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YjIOJS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YjIOJi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YjRYEC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0029" position="69">
        <attribute defType="com.stambia.rdbms.column.name" id="_YjRYES8NEeuJGap-USsnqg" value="WEBAUTHCOD_0029"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YjRYEi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YjRYEy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YjRYFC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YjRYFS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YjRYFi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YjbJEC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0030" position="70">
        <attribute defType="com.stambia.rdbms.column.name" id="_YjbJES8NEeuJGap-USsnqg" value="WEBAUTHCOD_0030"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YjbJEi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YjbJEy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YjbJFC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YjbJFS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YjbJFi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YjkTAC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0031" position="71">
        <attribute defType="com.stambia.rdbms.column.name" id="_YjkTAS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0031"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YjkTAi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YjkTAy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YjkTBC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YjkTBS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YjkTBi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YjuEAC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0032" position="72">
        <attribute defType="com.stambia.rdbms.column.name" id="_YjuEAS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0032"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YjuEAi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YjuEAy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YjuEBC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YjuEBS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YjuEBi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YjuEBy8NEeuJGap-USsnqg" name="WEBAUTHCOD_0033" position="73">
        <attribute defType="com.stambia.rdbms.column.name" id="_YjuECC8NEeuJGap-USsnqg" value="WEBAUTHCOD_0033"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YjuECS8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YjuECi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YjuECy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YjuEDC8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YjuEDS8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YkFQYC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0034" position="74">
        <attribute defType="com.stambia.rdbms.column.name" id="_YkFQYS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0034"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YkFQYi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YkFQYy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YkFQZC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YkFQZS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YkFQZi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YkOaUC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0035" position="75">
        <attribute defType="com.stambia.rdbms.column.name" id="_YkOaUS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0035"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YkOaUi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YkOaUy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YkOaVC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YkOaVS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YkOaVi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ykh8UC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0036" position="76">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ykh8US8NEeuJGap-USsnqg" value="WEBAUTHCOD_0036"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ykh8Ui8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ykh8Uy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ykh8VC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ykh8VS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ykh8Vi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YkrGQC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0037" position="77">
        <attribute defType="com.stambia.rdbms.column.name" id="_YkrGQS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0037"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YkrGQi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YkrGQy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YkrGRC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YkrGRS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YkrGRi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Yk03QC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0038" position="78">
        <attribute defType="com.stambia.rdbms.column.name" id="_Yk03QS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0038"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Yk03Qi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Yk03Qy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Yk03RC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Yk03RS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Yk03Ri8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YlBrkC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0039" position="79">
        <attribute defType="com.stambia.rdbms.column.name" id="_YlBrkS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0039"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YlBrki8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YlBrky8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YlBrlC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YlBrlS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YlBrli8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YlLckC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0040" position="80">
        <attribute defType="com.stambia.rdbms.column.name" id="_YlLckS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0040"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YlLcki8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YlLcky8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YlLclC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YlLclS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YlLcli8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YlVNkC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0041" position="81">
        <attribute defType="com.stambia.rdbms.column.name" id="_YlVNkS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0041"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YlVNki8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YlVNky8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YlVNlC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YlVNlS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YlVNli8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YleXgC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0042" position="82">
        <attribute defType="com.stambia.rdbms.column.name" id="_YleXgS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0042"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YleXgi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YleXgy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YleXhC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YleXhS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YleXhi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ylx5gC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0043" position="83">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ylx5gS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0043"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ylx5gi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ylx5gy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ylx5hC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ylx5hS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ylx5hi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Yl1j4C8NEeuJGap-USsnqg" name="WEBAUTHCOD_0044" position="84">
        <attribute defType="com.stambia.rdbms.column.name" id="_Yl1j4S8NEeuJGap-USsnqg" value="WEBAUTHCOD_0044"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Yl1j4i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Yl1j4y8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Yl1j5C8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Yl1j5S8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Yl1j5i8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YmIe0C8NEeuJGap-USsnqg" name="WEBAUTHCOD_0045" position="85">
        <attribute defType="com.stambia.rdbms.column.name" id="_YmIe0S8NEeuJGap-USsnqg" value="WEBAUTHCOD_0045"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YmIe0i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YmIe0y8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YmIe1C8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YmIe1S8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YmIe1i8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YmSP0C8NEeuJGap-USsnqg" name="WEBAUTHCOD_0046" position="86">
        <attribute defType="com.stambia.rdbms.column.name" id="_YmSP0S8NEeuJGap-USsnqg" value="WEBAUTHCOD_0046"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YmSP0i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YmSP0y8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YmSP1C8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YmSP1S8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YmSP1i8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YmbZwC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0047" position="87">
        <attribute defType="com.stambia.rdbms.column.name" id="_YmbZwS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0047"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YmbZwi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YmbZwy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YmbZxC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YmbZxS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YmbZxi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YmymIC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0048" position="88">
        <attribute defType="com.stambia.rdbms.column.name" id="_YmymIS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0048"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YmymIi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YmymIy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YmymJC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YmymJS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YmymJi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Ym8XIC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0049" position="89">
        <attribute defType="com.stambia.rdbms.column.name" id="_Ym8XIS8NEeuJGap-USsnqg" value="WEBAUTHCOD_0049"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Ym8XIi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Ym8XIy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Ym8XJC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Ym8XJS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Ym8XJi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YnFhEC8NEeuJGap-USsnqg" name="WEBAUTHCOD_0050" position="90">
        <attribute defType="com.stambia.rdbms.column.name" id="_YnFhES8NEeuJGap-USsnqg" value="WEBAUTHCOD_0050"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YnFhEi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YnFhEy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YnFhFC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YnFhFS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YnFhFi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YnPSEC8NEeuJGap-USsnqg" name="USERCRDH" position="91">
        <attribute defType="com.stambia.rdbms.column.name" id="_YnPSES8NEeuJGap-USsnqg" value="USERCRDH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YnPSEi8NEeuJGap-USsnqg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YnPSEy8NEeuJGap-USsnqg" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YnPSFC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YnPSFS8NEeuJGap-USsnqg" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YnPSFi8NEeuJGap-USsnqg" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YnYcAC8NEeuJGap-USsnqg" name="USERMODH" position="92">
        <attribute defType="com.stambia.rdbms.column.name" id="_YnYcAS8NEeuJGap-USsnqg" value="USERMODH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YnYcAi8NEeuJGap-USsnqg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YnYcAy8NEeuJGap-USsnqg" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YnYcBC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YnYcBS8NEeuJGap-USsnqg" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YnYcBi8NEeuJGap-USsnqg" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YniNAC8NEeuJGap-USsnqg" name="HSDT" position="93">
        <attribute defType="com.stambia.rdbms.column.name" id="_YniNAS8NEeuJGap-USsnqg" value="HSDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YniNAi8NEeuJGap-USsnqg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YniNAy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YniNBC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YniNBS8NEeuJGap-USsnqg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YniNBi8NEeuJGap-USsnqg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Yn5ZYC8NEeuJGap-USsnqg" name="NOTE" position="94">
        <attribute defType="com.stambia.rdbms.column.name" id="_Yn5ZYS8NEeuJGap-USsnqg" value="NOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Yn5ZYi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Yn5ZYy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Yn5ZZC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Yn5ZZS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Yn5ZZi8NEeuJGap-USsnqg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YqZ60C8NEeuJGap-USsnqg" name="CENOTE" position="95">
        <attribute defType="com.stambia.rdbms.column.name" id="_YqZ60S8NEeuJGap-USsnqg" value="CENOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YqZ60i8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YqZ60y8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YqZ61C8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YqZ61S8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YqZ61i8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YqnWMC8NEeuJGap-USsnqg" name="JOINT" position="96">
        <attribute defType="com.stambia.rdbms.column.name" id="_YqnWMS8NEeuJGap-USsnqg" value="JOINT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YqnWMi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YqnWMy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YqnWNC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YqnWNS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YqnWNi8NEeuJGap-USsnqg" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_Yq6RIC8NEeuJGap-USsnqg" name="CEJOINT" position="97">
        <attribute defType="com.stambia.rdbms.column.name" id="_Yq6RIS8NEeuJGap-USsnqg" value="CEJOINT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_Yq6RIi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_Yq6RIy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_Yq6RJC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_Yq6RJS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_Yq6RJi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YrECIC8NEeuJGap-USsnqg" name="IDCONNECT" position="98">
        <attribute defType="com.stambia.rdbms.column.name" id="_YrECIS8NEeuJGap-USsnqg" value="IDCONNECT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YrECIi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YrECIy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YrECJC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YrECJS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YrECJi8NEeuJGap-USsnqg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YrW9EC8NEeuJGap-USsnqg" name="TELCLE" position="99">
        <attribute defType="com.stambia.rdbms.column.name" id="_YrW9ES8NEeuJGap-USsnqg" value="TELCLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YrW9Ei8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YrW9Ey8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YrW9FC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YrW9FS8NEeuJGap-USsnqg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YrW9Fi8NEeuJGap-USsnqg" name="TELGSMCLE" position="100">
        <attribute defType="com.stambia.rdbms.column.name" id="_YrW9Fy8NEeuJGap-USsnqg" value="TELGSMCLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YrW9GC8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YrW9GS8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YrW9Gi8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YrW9Gy8NEeuJGap-USsnqg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YrkYcC8NEeuJGap-USsnqg" name="ICPFL" position="101">
        <attribute defType="com.stambia.rdbms.column.name" id="_YrkYcS8NEeuJGap-USsnqg" value="ICPFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YrkYci8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YrkYcy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YrkYdC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YrkYdS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YrkYdi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YruJcC8NEeuJGap-USsnqg" name="AFFCLDCOD" position="102">
        <attribute defType="com.stambia.rdbms.column.name" id="_YruJcS8NEeuJGap-USsnqg" value="AFFCLDCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YruJci8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YruJcy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YruJdC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YruJdS8NEeuJGap-USsnqg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YsBEYC8NEeuJGap-USsnqg" name="CARTENO" position="103">
        <attribute defType="com.stambia.rdbms.column.name" id="_YsBEYS8NEeuJGap-USsnqg" value="CARTENO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YsBEYi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YsBEYy8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YsBEZC8NEeuJGap-USsnqg" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YsBEZS8NEeuJGap-USsnqg" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YsK1YC8NEeuJGap-USsnqg" name="CONTACTTYP" position="104">
        <attribute defType="com.stambia.rdbms.column.name" id="_YsK1YS8NEeuJGap-USsnqg" value="CONTACTTYP"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YsK1Yi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YsK1Yy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YsK1ZC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YsK1ZS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YsK1Zi8NEeuJGap-USsnqg" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YsdwUC8NEeuJGap-USsnqg" name="IDENTITEEXT" position="105">
        <attribute defType="com.stambia.rdbms.column.name" id="_YsdwUS8NEeuJGap-USsnqg" value="IDENTITEEXT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YsdwUi8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YsdwUy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YsdwVC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YsdwVS8NEeuJGap-USsnqg" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YsdwVi8NEeuJGap-USsnqg" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_YsrLsC8NEeuJGap-USsnqg" name="NAISDT" position="106">
        <attribute defType="com.stambia.rdbms.column.name" id="_YsrLsS8NEeuJGap-USsnqg" value="NAISDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_YsrLsi8NEeuJGap-USsnqg" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_YsrLsy8NEeuJGap-USsnqg" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_YsrLtC8NEeuJGap-USsnqg" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_YsrLtS8NEeuJGap-USsnqg" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_YsrLti8NEeuJGap-USsnqg" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_YtH3oC8NEeuJGap-USsnqg" name="PK__T2__2848FEF57B1466E4">
        <node defType="com.stambia.rdbms.colref" id="_YtH3oS8NEeuJGap-USsnqg" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_YtH3oi8NEeuJGap-USsnqg" ref="#_YXF9QC8NEeuJGap-USsnqg?fileId=_N_b74AcLEeu--f7ccO6NIw$type=md$name=T2_ID?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_W0yokDPlEeuiuMhjAPcU2A" name="ECF_COBAL_MERCURIALE">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_W0yokTPlEeuiuMhjAPcU2A" value="ECF_COBAL_MERCURIALE"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_W0yokjPlEeuiuMhjAPcU2A" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_W2bnUDPlEeuiuMhjAPcU2A" name="NOM_MERCURIALE" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_W2bnUTPlEeuiuMhjAPcU2A" value="NOM_MERCURIALE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_W2bnUjPlEeuiuMhjAPcU2A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_W2bnUzPlEeuiuMhjAPcU2A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_W2bnVDPlEeuiuMhjAPcU2A" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_W2bnVTPlEeuiuMhjAPcU2A" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_W2oboDPlEeuiuMhjAPcU2A" name="REF_ARTICLE" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_W2oboTPlEeuiuMhjAPcU2A" value="REF_ARTICLE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_W2obojPlEeuiuMhjAPcU2A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_W2obozPlEeuiuMhjAPcU2A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_W2obpDPlEeuiuMhjAPcU2A" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_W2obpTPlEeuiuMhjAPcU2A" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_W6j5YDPlEeuiuMhjAPcU2A" name="TOP_ETL" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_W6j5YTPlEeuiuMhjAPcU2A" value="TOP_ETL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_W6j5YjPlEeuiuMhjAPcU2A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_W6j5YzPlEeuiuMhjAPcU2A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_W6j5ZDPlEeuiuMhjAPcU2A" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_W6j5ZTPlEeuiuMhjAPcU2A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_W6tqYDPlEeuiuMhjAPcU2A" name="TOP_MAJ" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_W6tqYTPlEeuiuMhjAPcU2A" value="TOP_MAJ"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_W6tqYjPlEeuiuMhjAPcU2A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_W6tqYzPlEeuiuMhjAPcU2A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_W6tqZDPlEeuiuMhjAPcU2A" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_W6tqZTPlEeuiuMhjAPcU2A" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_W63bYDPlEeuiuMhjAPcU2A" name="DATE_CREATION" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_W63bYTPlEeuiuMhjAPcU2A" value="DATE_CREATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_W63bYjPlEeuiuMhjAPcU2A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_W63bYzPlEeuiuMhjAPcU2A" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_W63bZDPlEeuiuMhjAPcU2A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_W63bZTPlEeuiuMhjAPcU2A" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_W63bZjPlEeuiuMhjAPcU2A" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_W63bZzPlEeuiuMhjAPcU2A" name="DATE_MODIFICATION" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_W63baDPlEeuiuMhjAPcU2A" value="DATE_MODIFICATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_W63baTPlEeuiuMhjAPcU2A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_W63bajPlEeuiuMhjAPcU2A" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_W63bazPlEeuiuMhjAPcU2A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_W63bbDPlEeuiuMhjAPcU2A" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_W63bbTPlEeuiuMhjAPcU2A" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_W7E2wDPlEeuiuMhjAPcU2A" name="DATE_SUPPRESSION" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_W7E2wTPlEeuiuMhjAPcU2A" value="DATE_SUPPRESSION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_W7E2wjPlEeuiuMhjAPcU2A" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_W7E2wzPlEeuiuMhjAPcU2A" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_W7E2xDPlEeuiuMhjAPcU2A" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_W7E2xTPlEeuiuMhjAPcU2A" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_W7E2xjPlEeuiuMhjAPcU2A" value="23"/>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_az14gTq-Eeuj-N7gRVfuJA" name="T1">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_az14gjq-Eeuj-N7gRVfuJA" value="T1"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_az14gzq-Eeuj-N7gRVfuJA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_a1sSoDq-Eeuj-N7gRVfuJA" name="T1_ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_a1sSoTq-Eeuj-N7gRVfuJA" value="T1_ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a1sSojq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a1sSozq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a1sSpDq-Eeuj-N7gRVfuJA" value="true"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a1sSpTq-Eeuj-N7gRVfuJA" value="int identity"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a1sSpjq-Eeuj-N7gRVfuJA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a1wkEDq-Eeuj-N7gRVfuJA" name="CE1" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_a1wkETq-Eeuj-N7gRVfuJA" value="CE1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a1wkEjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a1wkEzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a1wkFDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a1wkFTq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a1wkFjq-Eeuj-N7gRVfuJA" name="CE2" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_a1wkFzq-Eeuj-N7gRVfuJA" value="CE2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a1wkGDq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a1wkGTq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a1wkGjq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a1wkGzq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a15uADq-Eeuj-N7gRVfuJA" name="CE3" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_a15uATq-Eeuj-N7gRVfuJA" value="CE3"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a15uAjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a15uAzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a15uBDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a15uBTq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a2DfADq-Eeuj-N7gRVfuJA" name="CE4" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_a2DfATq-Eeuj-N7gRVfuJA" value="CE4"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a2DfAjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a2DfAzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a2DfBDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a2DfBTq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a2DfBjq-Eeuj-N7gRVfuJA" name="CE5" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_a2DfBzq-Eeuj-N7gRVfuJA" value="CE5"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a2DfCDq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a2DfCTq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a2DfCjq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a2DfCzq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a2NQADq-Eeuj-N7gRVfuJA" name="CE6" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_a2NQATq-Eeuj-N7gRVfuJA" value="CE6"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a2NQAjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a2NQAzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a2NQBDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a2NQBTq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a2WZ8Dq-Eeuj-N7gRVfuJA" name="CE7" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_a2WZ8Tq-Eeuj-N7gRVfuJA" value="CE7"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a2WZ8jq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a2WZ8zq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a2WZ9Dq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a2WZ9Tq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a2tmUDq-Eeuj-N7gRVfuJA" name="CE8" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_a2tmUTq-Eeuj-N7gRVfuJA" value="CE8"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a2tmUjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a2tmUzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a2tmVDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a2tmVTq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a23XUDq-Eeuj-N7gRVfuJA" name="CE9" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_a23XUTq-Eeuj-N7gRVfuJA" value="CE9"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a23XUjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a23XUzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a23XVDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a23XVTq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a3AhQDq-Eeuj-N7gRVfuJA" name="CEA" position="11">
        <attribute defType="com.stambia.rdbms.column.name" id="_a3AhQTq-Eeuj-N7gRVfuJA" value="CEA"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a3AhQjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a3AhQzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a3AhRDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a3AhRTq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a3KSQDq-Eeuj-N7gRVfuJA" name="DOS" position="12">
        <attribute defType="com.stambia.rdbms.column.name" id="_a3KSQTq-Eeuj-N7gRVfuJA" value="DOS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a3KSQjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a3KSQzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a3KSRDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a3KSRTq-Eeuj-N7gRVfuJA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a3UDQDq-Eeuj-N7gRVfuJA" name="TIERS" position="13">
        <attribute defType="com.stambia.rdbms.column.name" id="_a3UDQTq-Eeuj-N7gRVfuJA" value="TIERS"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a3UDQjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a3UDQzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a3UDRDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a3UDRTq-Eeuj-N7gRVfuJA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a3UDRjq-Eeuj-N7gRVfuJA" name="USERCR" position="14">
        <attribute defType="com.stambia.rdbms.column.name" id="_a3UDRzq-Eeuj-N7gRVfuJA" value="USERCR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a3UDSDq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a3UDSTq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a3UDSjq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a3UDSzq-Eeuj-N7gRVfuJA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a3dNMDq-Eeuj-N7gRVfuJA" name="USERMO" position="15">
        <attribute defType="com.stambia.rdbms.column.name" id="_a3dNMTq-Eeuj-N7gRVfuJA" value="USERMO"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a3dNMjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a3dNMzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a3dNNDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a3dNNTq-Eeuj-N7gRVfuJA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a3m-MDq-Eeuj-N7gRVfuJA" name="ADRCOD" position="16">
        <attribute defType="com.stambia.rdbms.column.name" id="_a3m-MTq-Eeuj-N7gRVfuJA" value="ADRCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a3m-Mjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a3m-Mzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a3m-NDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a3m-NTq-Eeuj-N7gRVfuJA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a3qokDq-Eeuj-N7gRVfuJA" name="NOM" position="17">
        <attribute defType="com.stambia.rdbms.column.name" id="_a3qokTq-Eeuj-N7gRVfuJA" value="NOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a3qokjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a3qokzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a3qolDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a3qolTq-Eeuj-N7gRVfuJA" value="80"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a30ZkDq-Eeuj-N7gRVfuJA" name="ADRCPL1" position="18">
        <attribute defType="com.stambia.rdbms.column.name" id="_a30ZkTq-Eeuj-N7gRVfuJA" value="ADRCPL1"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a30Zkjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a30Zkzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a30ZlDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a30ZlTq-Eeuj-N7gRVfuJA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a3210Dq-Eeuj-N7gRVfuJA" name="ADRCPL2" position="19">
        <attribute defType="com.stambia.rdbms.column.name" id="_a3210Tq-Eeuj-N7gRVfuJA" value="ADRCPL2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a3210jq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a3210zq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a3211Dq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a3211Tq-Eeuj-N7gRVfuJA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a4Am0Dq-Eeuj-N7gRVfuJA" name="RUE" position="20">
        <attribute defType="com.stambia.rdbms.column.name" id="_a4Am0Tq-Eeuj-N7gRVfuJA" value="RUE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a4Am0jq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a4Am0zq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a4Am1Dq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a4Am1Tq-Eeuj-N7gRVfuJA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a4KX0Dq-Eeuj-N7gRVfuJA" name="LOC" position="21">
        <attribute defType="com.stambia.rdbms.column.name" id="_a4KX0Tq-Eeuj-N7gRVfuJA" value="LOC"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a4KX0jq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a4KX0zq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a4KX1Dq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a4KX1Tq-Eeuj-N7gRVfuJA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a4ThwDq-Eeuj-N7gRVfuJA" name="VIL" position="22">
        <attribute defType="com.stambia.rdbms.column.name" id="_a4ThwTq-Eeuj-N7gRVfuJA" value="VIL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a4Thwjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a4Thwzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a4ThxDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a4ThxTq-Eeuj-N7gRVfuJA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a4dSwDq-Eeuj-N7gRVfuJA" name="PAY" position="23">
        <attribute defType="com.stambia.rdbms.column.name" id="_a4dSwTq-Eeuj-N7gRVfuJA" value="PAY"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a4dSwjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a4dSwzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a4dSxDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a4dSxTq-Eeuj-N7gRVfuJA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a4mcsDq-Eeuj-N7gRVfuJA" name="CPOSTAL" position="24">
        <attribute defType="com.stambia.rdbms.column.name" id="_a4mcsTq-Eeuj-N7gRVfuJA" value="CPOSTAL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a4mcsjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a4mcszq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a4mctDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a4mctTq-Eeuj-N7gRVfuJA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a4nq0Dq-Eeuj-N7gRVfuJA" name="ZIPCOD" position="25">
        <attribute defType="com.stambia.rdbms.column.name" id="_a4nq0Tq-Eeuj-N7gRVfuJA" value="ZIPCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a4nq0jq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a4nq0zq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a4nq1Dq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a4nq1Tq-Eeuj-N7gRVfuJA" value="50"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a4xb0Dq-Eeuj-N7gRVfuJA" name="REGIONCOD" position="26">
        <attribute defType="com.stambia.rdbms.column.name" id="_a4xb0Tq-Eeuj-N7gRVfuJA" value="REGIONCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a4xb0jq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a4xb0zq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a4xb1Dq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a4xb1Tq-Eeuj-N7gRVfuJA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a4xb1jq-Eeuj-N7gRVfuJA" name="INSEECOD" position="27">
        <attribute defType="com.stambia.rdbms.column.name" id="_a4xb1zq-Eeuj-N7gRVfuJA" value="INSEECOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a4xb2Dq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a4xb2Tq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a4xb2jq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a4xb2zq-Eeuj-N7gRVfuJA" value="5"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a46lwDq-Eeuj-N7gRVfuJA" name="TEL" position="28">
        <attribute defType="com.stambia.rdbms.column.name" id="_a46lwTq-Eeuj-N7gRVfuJA" value="TEL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a46lwjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a46lwzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a46lxDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a46lxTq-Eeuj-N7gRVfuJA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a5EWwDq-Eeuj-N7gRVfuJA" name="FAX" position="29">
        <attribute defType="com.stambia.rdbms.column.name" id="_a5EWwTq-Eeuj-N7gRVfuJA" value="FAX"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a5EWwjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a5EWwzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a5EWxDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a5EWxTq-Eeuj-N7gRVfuJA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a5EWxjq-Eeuj-N7gRVfuJA" name="EMAIL" position="30">
        <attribute defType="com.stambia.rdbms.column.name" id="_a5EWxzq-Eeuj-N7gRVfuJA" value="EMAIL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a5EWyDq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a5EWyTq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a5EWyjq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a5EWyzq-Eeuj-N7gRVfuJA" value="80"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a5OHwDq-Eeuj-N7gRVfuJA" name="TOUR" position="31">
        <attribute defType="com.stambia.rdbms.column.name" id="_a5OHwTq-Eeuj-N7gRVfuJA" value="TOUR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a5OHwjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a5OHwzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a5OHxDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a5OHxTq-Eeuj-N7gRVfuJA" value="6"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a5XRsDq-Eeuj-N7gRVfuJA" name="BLJR" position="32">
        <attribute defType="com.stambia.rdbms.column.name" id="_a5XRsTq-Eeuj-N7gRVfuJA" value="BLJR"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a5XRsjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a5XRszq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a5XRtDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a5XRtTq-Eeuj-N7gRVfuJA" value="7"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a5XRtjq-Eeuj-N7gRVfuJA" name="TVABLCOE" position="33">
        <attribute defType="com.stambia.rdbms.column.name" id="_a5XRtzq-Eeuj-N7gRVfuJA" value="TVABLCOE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a5XRuDq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a5XRuTq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a5XRujq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a5XRuzq-Eeuj-N7gRVfuJA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a5hCsDq-Eeuj-N7gRVfuJA" name="USERCRDH" position="34">
        <attribute defType="com.stambia.rdbms.column.name" id="_a5hCsTq-Eeuj-N7gRVfuJA" value="USERCRDH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a5hCsjq-Eeuj-N7gRVfuJA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a5hCszq-Eeuj-N7gRVfuJA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a5hCtDq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a5hCtTq-Eeuj-N7gRVfuJA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a5hCtjq-Eeuj-N7gRVfuJA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a5ktEDq-Eeuj-N7gRVfuJA" name="USERMODH" position="35">
        <attribute defType="com.stambia.rdbms.column.name" id="_a5ktETq-Eeuj-N7gRVfuJA" value="USERMODH"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a5ktEjq-Eeuj-N7gRVfuJA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a5ktEzq-Eeuj-N7gRVfuJA" value="7"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a5ktFDq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a5ktFTq-Eeuj-N7gRVfuJA" value="datetime2"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a5ktFjq-Eeuj-N7gRVfuJA" value="27"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a5ueEDq-Eeuj-N7gRVfuJA" name="HSDT" position="36">
        <attribute defType="com.stambia.rdbms.column.name" id="_a5ueETq-Eeuj-N7gRVfuJA" value="HSDT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a5ueEjq-Eeuj-N7gRVfuJA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a5ueEzq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a5ueFDq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a5ueFTq-Eeuj-N7gRVfuJA" value="date"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a5ueFjq-Eeuj-N7gRVfuJA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a5ueFzq-Eeuj-N7gRVfuJA" name="NOTE" position="37">
        <attribute defType="com.stambia.rdbms.column.name" id="_a5ueGDq-Eeuj-N7gRVfuJA" value="NOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a5ueGTq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a5ueGjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a5ueGzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a5ueHDq-Eeuj-N7gRVfuJA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a5ueHTq-Eeuj-N7gRVfuJA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a53oADq-Eeuj-N7gRVfuJA" name="CENOTE" position="38">
        <attribute defType="com.stambia.rdbms.column.name" id="_a53oATq-Eeuj-N7gRVfuJA" value="CENOTE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a53oAjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a53oAzq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a53oBDq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a53oBTq-Eeuj-N7gRVfuJA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a53oBjq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a53oBzq-Eeuj-N7gRVfuJA" name="TRANSJRNB" position="39">
        <attribute defType="com.stambia.rdbms.column.name" id="_a53oCDq-Eeuj-N7gRVfuJA" value="TRANSJRNB"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a53oCTq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a53oCjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a53oCzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a53oDDq-Eeuj-N7gRVfuJA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a53oDTq-Eeuj-N7gRVfuJA" value="3"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a6BZADq-Eeuj-N7gRVfuJA" name="TOURRG" position="40">
        <attribute defType="com.stambia.rdbms.column.name" id="_a6BZATq-Eeuj-N7gRVfuJA" value="TOURRG"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a6BZAjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a6BZAzq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a6BZBDq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a6BZBTq-Eeuj-N7gRVfuJA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a6BZBjq-Eeuj-N7gRVfuJA" value="4"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a6LKADq-Eeuj-N7gRVfuJA" name="ADRPIECETYP_0001" position="41">
        <attribute defType="com.stambia.rdbms.column.name" id="_a6LKATq-Eeuj-N7gRVfuJA" value="ADRPIECETYP_0001"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a6LKAjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a6LKAzq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a6LKBDq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a6LKBTq-Eeuj-N7gRVfuJA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a6LKBjq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a6LKBzq-Eeuj-N7gRVfuJA" name="ADRPIECETYP_0002" position="42">
        <attribute defType="com.stambia.rdbms.column.name" id="_a6LKCDq-Eeuj-N7gRVfuJA" value="ADRPIECETYP_0002"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a6LKCTq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a6LKCjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a6LKCzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a6LKDDq-Eeuj-N7gRVfuJA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a6LKDTq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a6UT8Dq-Eeuj-N7gRVfuJA" name="ADRPIECETYP_0003" position="43">
        <attribute defType="com.stambia.rdbms.column.name" id="_a6UT8Tq-Eeuj-N7gRVfuJA" value="ADRPIECETYP_0003"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a6UT8jq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a6UT8zq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a6UT9Dq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a6UT9Tq-Eeuj-N7gRVfuJA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a6UT9jq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a6eE8Dq-Eeuj-N7gRVfuJA" name="ADRPIECETYP_0004" position="44">
        <attribute defType="com.stambia.rdbms.column.name" id="_a6eE8Tq-Eeuj-N7gRVfuJA" value="ADRPIECETYP_0004"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a6eE8jq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a6eE8zq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a6eE9Dq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a6eE9Tq-Eeuj-N7gRVfuJA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a6eE9jq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a6hvUDq-Eeuj-N7gRVfuJA" name="ADRPIECETYP_0005" position="45">
        <attribute defType="com.stambia.rdbms.column.name" id="_a6hvUTq-Eeuj-N7gRVfuJA" value="ADRPIECETYP_0005"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a6hvUjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a6hvUzq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a6hvVDq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a6hvVTq-Eeuj-N7gRVfuJA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a6hvVjq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a6rgUDq-Eeuj-N7gRVfuJA" name="BPBASCOD" position="46">
        <attribute defType="com.stambia.rdbms.column.name" id="_a6rgUTq-Eeuj-N7gRVfuJA" value="BPBASCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a6rgUjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a6rgUzq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a6rgVDq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a6rgVTq-Eeuj-N7gRVfuJA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a6rgVjq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a6rgVzq-Eeuj-N7gRVfuJA" name="BPRUPTCOD" position="47">
        <attribute defType="com.stambia.rdbms.column.name" id="_a6rgWDq-Eeuj-N7gRVfuJA" value="BPRUPTCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a6rgWTq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a6rgWjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a6rgWzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a6rgXDq-Eeuj-N7gRVfuJA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a6rgXTq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a60qQDq-Eeuj-N7gRVfuJA" name="BLGENCOD" position="48">
        <attribute defType="com.stambia.rdbms.column.name" id="_a60qQTq-Eeuj-N7gRVfuJA" value="BLGENCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a60qQjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a60qQzq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a60qRDq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a60qRTq-Eeuj-N7gRVfuJA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a60qRjq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a60qRzq-Eeuj-N7gRVfuJA" name="BPRELCOD" position="49">
        <attribute defType="com.stambia.rdbms.column.name" id="_a60qSDq-Eeuj-N7gRVfuJA" value="BPRELCOD"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a60qSTq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a60qSjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a60qSzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a60qTDq-Eeuj-N7gRVfuJA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a60qTTq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a6-bQDq-Eeuj-N7gRVfuJA" name="GLN" position="50">
        <attribute defType="com.stambia.rdbms.column.name" id="_a6-bQTq-Eeuj-N7gRVfuJA" value="GLN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a6-bQjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a6-bQzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a6-bRDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a6-bRTq-Eeuj-N7gRVfuJA" value="13"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a7IMQDq-Eeuj-N7gRVfuJA" name="ICPFL" position="51">
        <attribute defType="com.stambia.rdbms.column.name" id="_a7IMQTq-Eeuj-N7gRVfuJA" value="ICPFL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a7IMQjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a7IMQzq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a7IMRDq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a7IMRTq-Eeuj-N7gRVfuJA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a7IMRjq-Eeuj-N7gRVfuJA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a7IMRzq-Eeuj-N7gRVfuJA" name="IDENTITEEXT" position="52">
        <attribute defType="com.stambia.rdbms.column.name" id="_a7IMSDq-Eeuj-N7gRVfuJA" value="IDENTITEEXT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a7IMSTq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_a7IMSjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a7IMSzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a7IMTDq-Eeuj-N7gRVfuJA" value="numeric"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a7IMTTq-Eeuj-N7gRVfuJA" value="9"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a7RWMDq-Eeuj-N7gRVfuJA" name="TVATIE" position="53">
        <attribute defType="com.stambia.rdbms.column.name" id="_a7RWMTq-Eeuj-N7gRVfuJA" value="TVATIE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a7RWMjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a7RWMzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a7RWNDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a7RWNTq-Eeuj-N7gRVfuJA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a7bHMDq-Eeuj-N7gRVfuJA" name="PG_CONTACT" position="54">
        <attribute defType="com.stambia.rdbms.column.name" id="_a7bHMTq-Eeuj-N7gRVfuJA" value="PG_CONTACT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a7bHMjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a7bHMzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a7bHNDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a7bHNTq-Eeuj-N7gRVfuJA" value="8"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a7exkDq-Eeuj-N7gRVfuJA" name="PG_COMMENTAIRE" position="55">
        <attribute defType="com.stambia.rdbms.column.name" id="_a7exkTq-Eeuj-N7gRVfuJA" value="PG_COMMENTAIRE"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a7exkjq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a7exkzq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a7exlDq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a7exlTq-Eeuj-N7gRVfuJA" value="40"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_a7exljq-Eeuj-N7gRVfuJA" name="PG_TEL2" position="56">
        <attribute defType="com.stambia.rdbms.column.name" id="_a7exlzq-Eeuj-N7gRVfuJA" value="PG_TEL2"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_a7exmDq-Eeuj-N7gRVfuJA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_a7exmTq-Eeuj-N7gRVfuJA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_a7exmjq-Eeuj-N7gRVfuJA" value="char"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_a7exmzq-Eeuj-N7gRVfuJA" value="20"/>
      </node>
      <node defType="com.stambia.rdbms.pk" id="_a8lk0Dq-Eeuj-N7gRVfuJA" name="PK__T1__B4429B647743D600">
        <node defType="com.stambia.rdbms.colref" id="_a8lk0Tq-Eeuj-N7gRVfuJA" position="1">
          <attribute defType="com.stambia.rdbms.colref.ref" id="_a8lk0jq-Eeuj-N7gRVfuJA" ref="#_a1sSoDq-Eeuj-N7gRVfuJA?fileId=_N_b74AcLEeu--f7ccO6NIw$type=md$name=T1_ID?"/>
        </node>
      </node>
    </node>
    <node defType="com.stambia.rdbms.datastore" id="_P4jcIUBPEeufWN0sTrhTiA" name="ECF_COBAL_CLIENT">
      <attribute defType="com.stambia.rdbms.datastore.name" id="_P4jcIkBPEeufWN0sTrhTiA" value="ECF_COBAL_CLIENT"/>
      <attribute defType="com.stambia.rdbms.datastore.type" id="_P4jcI0BPEeufWN0sTrhTiA" value="TABLE"/>
      <node defType="com.stambia.rdbms.column" id="_P46ogEBPEeufWN0sTrhTiA" name="ID" position="1">
        <attribute defType="com.stambia.rdbms.column.name" id="_P46ogUBPEeufWN0sTrhTiA" value="ID"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_P46ogkBPEeufWN0sTrhTiA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_P46og0BPEeufWN0sTrhTiA" value="0"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_P46ohEBPEeufWN0sTrhTiA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_P46ohUBPEeufWN0sTrhTiA" value="int"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_P46ohkBPEeufWN0sTrhTiA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_P5DycEBPEeufWN0sTrhTiA" name="CODE_CLIENT" position="2">
        <attribute defType="com.stambia.rdbms.column.name" id="_P5DycUBPEeufWN0sTrhTiA" value="CODE_CLIENT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_P5DyckBPEeufWN0sTrhTiA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_P5Dyc0BPEeufWN0sTrhTiA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_P5DydEBPEeufWN0sTrhTiA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_P5DydUBPEeufWN0sTrhTiA" value="10"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_P5NjcEBPEeufWN0sTrhTiA" name="EMAIL_CONTACT" position="3">
        <attribute defType="com.stambia.rdbms.column.name" id="_P5NjcUBPEeufWN0sTrhTiA" value="EMAIL_CONTACT"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_P5NjckBPEeufWN0sTrhTiA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_P5Njc0BPEeufWN0sTrhTiA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_P5NjdEBPEeufWN0sTrhTiA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_P5NjdUBPEeufWN0sTrhTiA" value="255"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_P5NjdkBPEeufWN0sTrhTiA" name="TOKEN" position="4">
        <attribute defType="com.stambia.rdbms.column.name" id="_P5Njd0BPEeufWN0sTrhTiA" value="TOKEN"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_P5NjeEBPEeufWN0sTrhTiA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_P5NjeUBPEeufWN0sTrhTiA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_P5NjekBPEeufWN0sTrhTiA" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_P5Nje0BPEeufWN0sTrhTiA" value="100"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_P5WtYEBPEeufWN0sTrhTiA" name="TOP_ETL" position="5">
        <attribute defType="com.stambia.rdbms.column.name" id="_P5WtYUBPEeufWN0sTrhTiA" value="TOP_ETL"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_P5WtYkBPEeufWN0sTrhTiA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_P5WtY0BPEeufWN0sTrhTiA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_P5WtZEBPEeufWN0sTrhTiA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_P5WtZUBPEeufWN0sTrhTiA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_P5geYEBPEeufWN0sTrhTiA" name="TOP_MAJ" position="6">
        <attribute defType="com.stambia.rdbms.column.name" id="_P5geYUBPEeufWN0sTrhTiA" value="TOP_MAJ"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_P5geYkBPEeufWN0sTrhTiA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_P5geY0BPEeufWN0sTrhTiA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_P5geZEBPEeufWN0sTrhTiA" value="bit"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_P5geZUBPEeufWN0sTrhTiA" value="1"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_P5geZkBPEeufWN0sTrhTiA" name="DATE_CREATION" position="7">
        <attribute defType="com.stambia.rdbms.column.name" id="_P5geZ0BPEeufWN0sTrhTiA" value="DATE_CREATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_P5geaEBPEeufWN0sTrhTiA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_P5geaUBPEeufWN0sTrhTiA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_P5geakBPEeufWN0sTrhTiA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_P5gea0BPEeufWN0sTrhTiA" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_P5gebEBPEeufWN0sTrhTiA" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_P5qPYEBPEeufWN0sTrhTiA" name="DATE_MODIFICATION" position="8">
        <attribute defType="com.stambia.rdbms.column.name" id="_P5qPYUBPEeufWN0sTrhTiA" value="DATE_MODIFICATION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_P5qPYkBPEeufWN0sTrhTiA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_P5qPY0BPEeufWN0sTrhTiA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_P5qPZEBPEeufWN0sTrhTiA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_P5qPZUBPEeufWN0sTrhTiA" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_P5qPZkBPEeufWN0sTrhTiA" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_P5t5wEBPEeufWN0sTrhTiA" name="DATE_SUPPRESSION" position="9">
        <attribute defType="com.stambia.rdbms.column.name" id="_P5t5wUBPEeufWN0sTrhTiA" value="DATE_SUPPRESSION"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_P5t5wkBPEeufWN0sTrhTiA" value="1"/>
        <attribute defType="com.stambia.rdbms.column.digits" id="_P5t5w0BPEeufWN0sTrhTiA" value="3"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_P5t5xEBPEeufWN0sTrhTiA" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_P5t5xUBPEeufWN0sTrhTiA" value="datetime"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_P5t5xkBPEeufWN0sTrhTiA" value="23"/>
      </node>
      <node defType="com.stambia.rdbms.column" id="_5CViR0BiEeuSxY8QOGhQIQ" name="PRENOM_NOM" position="10">
        <attribute defType="com.stambia.rdbms.column.name" id="_5CViSEBiEeuSxY8QOGhQIQ" value="PRENOM_NOM"/>
        <attribute defType="com.stambia.rdbms.column.nullable" id="_5CViSUBiEeuSxY8QOGhQIQ" value="1"/>
        <attribute defType="com.stambia.rdbms.column.autoIncrement" id="_5CViSkBiEeuSxY8QOGhQIQ" value="false"/>
        <attribute defType="com.stambia.rdbms.column.type" id="_5CViS0BiEeuSxY8QOGhQIQ" value="varchar"/>
        <attribute defType="com.stambia.rdbms.column.size" id="_5CViTEBiEeuSxY8QOGhQIQ" value="100"/>
      </node>
    </node>
  </node>
</md:node>